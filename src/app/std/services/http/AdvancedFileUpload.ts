import { CollectionType, IRow, IFileRow } from "../../shared/types/CollectionType";
import { IFileFormData, IFileResponse } from "./FileUpload";
import { Attribute } from "../../shared/common/Attribute";
import { FileType } from "../../shared/types/FileType";
import { Utils } from "../../shared/common/Utils";
import { environment } from "src/environments/environment";


/* CollectionType koji je predan kao argument je root transakcije i on mora imat edit-row postavljen. Detalji su u normalnom modu 
   TODO: freez onCollectionChanged cu collectionType-u ---> isto kao AdvancedBatchRequest
*/
export class AdvancedFileUpload{

    private _collectionType: CollectionType;

    private _fileFormData: IFileFormData[];
    private _map:Map<number, IRowFileFormData>;

    private _currentIndex: number;

    constructor(CollectionType: CollectionType) {
        this._collectionType = CollectionType;
    }

    private _processRow(row:IFileRow): void {

        if(row.files != null && row.files.length > 0){
                
            row.files.forEach(f => {

                let type = <FileType>this._collectionType.findAttr(f.name).type;

                this._buildAndAddFormData(row, this._collectionType.findAttr(f.name), [f.value], type.destination);

            });

        }
        this._collectionType.attributes.filter(a => a.type instanceof CollectionType).forEach(c => this._processSubCollection(c, row.data[c.name]));
    }


    protected _processSubCollection(subCollectionTypeAttr:Attribute, rows:IFileRow[]): void{

        if(rows == null) return;

        let ct = <CollectionType>subCollectionTypeAttr.type;

        rows.forEach(r => {
            
            if(r.files != null && r.files.length > 0){
                
                r.files.forEach(f => {

                    let type = <FileType>ct.findAttr(f.name).type;

                    this._buildAndAddFormData(r, ct.findAttr(f.name), [f.value], type.destination);

                });

            }

            ct.attributes.filter(a => a.type instanceof CollectionType).forEach(c => this._processSubCollection(c, r.data[c.name]));   

        });
    }

    private _buildAndAddFormData(row:IFileRow, attr:Attribute, files: File[], destination: string): void{
        
        let ffd = this._buildFormData(files, destination);

        this._fileFormData.push(ffd);

        this._map.set(this._currentIndex, {id: this._currentIndex, attribute: attr, row: row})

    }

    private _buildFormData(files: File[], destination: string): IFileFormData {

        let fnms: string[] = [];
        const fd = new FormData();

        for (let i = 0; i < files.length; i++) {

            fnms.push("file" + [i]);

            fd.append("file" + [i], files[i]);

        }

        fd.append('destination', destination);

        fd.append('fileNames', JSON.stringify(fnms));
        
        this._currentIndex++;

        return { id: this._currentIndex, formData: fd, fileNames: fnms };
    }


    public syncWithResponse(res: any, ffd: IFileFormData): void {

        let ctx = this._map.get(ffd.id);
        
        let fr: IFileResponse = res[ffd.fileNames[0]];

        if (!Utils.emptyValue(fr.error)){

            if(!environment.production)
                console.log(fr.error);

            // ctx.attribute.setError(fr.error);
            ctx.row.files.find(f => f.name == ctx.attribute.name).error = fr.error;            
        
        }else{
            
            ctx.row.data[ctx.attribute.name] = fr.path + '/' + fr.name;
            
            //Remove file from row
            if(ctx.row.files.length == 1)
                ctx.row.files = null;

            else
                ctx.row.files.splice(ctx.row.files.findIndex(f => f.name == ctx.attribute.name), 1);

        }

        this._map.delete(ffd.id);
    }


    public uploadIsFinished():boolean{
        return this._map.size == 0;  
    }


    private _clear(): void {
        this._fileFormData = [];
        this._currentIndex = 0;
        this._map = new Map<number, IRowFileFormData>();
    }

    public getFileFormData(): IFileFormData[] {
        
        this._clear();

        this._processRow(<IFileRow>this._collectionType.getEditRow());

        return this._fileFormData;
    }

}


export interface IRowFileFormData{
    id:number;
    row:IFileRow;
    attribute:Attribute;
}