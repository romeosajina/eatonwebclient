import { CollectionType, IRow, ILink, LINK_TYPE, ROW_STATE } from '../../shared/types/CollectionType';
import { FileCollectionType, IFileRow } from '../../shared/types/FileCollectionType';
import { FileType } from '../../shared/types/FileType';
import { Attribute } from '../../shared/common/Attribute';
import { Utils } from '../../shared/common/Utils';
// import { BatchRequest, RowInCollection } from './BatchRequest';

export class FileUpload {

    private _collectionType: CollectionType;

    private _fileFormData: IFileFormData[];

    private _subCollTypesAttributes: Attribute[];

    // protected _map: Map<string, RowInCollection> = new Map<string, RowInCollection>();

    private _fileFormsCollectionList: Array<IFileFormForColletion> = null;
    private _fileFormsFileTypeList: Array<IFileFormForFileType> = null;

    private _formId: number;

    constructor(CollectionType: CollectionType) {
        this._collectionType = CollectionType;
    }

    // public getFileFormData(): FormData[]{
    public getFileFormData(): IFileFormData[] {
        this._clear();

        this._processCollection(this._collectionType);

        this._fillFileForm();
        return this._fileFormData;
        // return this._getFormData();
    }


    private _processCollection(collectionType: CollectionType = this._collectionType): void {

        collectionType.attributes.forEach((attr) => {

            if (attr.type instanceof FileCollectionType)
                this._proccessFileCollection(attr.type)

            else if (attr.type instanceof CollectionType)
                this._processCollection(attr.type)

            else if (attr.type instanceof FileType && attr.type.file != null)
                this._addFileTypeToList(this._buildFormData([attr.type.file], attr.type.destination), attr, collectionType);
            // this._proccessFileType(attr.type);

        });

    }

//TODO: add row for file to sync it later
    private _proccessFileCollection(fileCollectionType: FileCollectionType): void {

        let files: File[] = [];

        fileCollectionType.getFiles().forEach(fr => files.push(fr.file) );

        this._addFileCollectionToList(this._buildFormData(files, fileCollectionType.destination), fileCollectionType);

    }


    // private _proccessFileType(fileType:FileType): void{
    //     if(fileType.file != null)
    //         this._addFileTypeToList();

    // }

    private _addFileTypeToList(ffd: IFileFormData, attr: Attribute, ct: CollectionType): void {
        this._fileFormsFileTypeList.push({ fileFormData: ffd, attribute: attr, collectionType: ct });
    }

    private _addFileCollectionToList(ffd: IFileFormData, ct: FileCollectionType): void {
        this._fileFormsCollectionList.push({ fileFormData: ffd, collectionType: ct });
    }

    private _getFormData(): FormData[] {

        let fd: FormData[] = [];

        this._fileFormData.forEach(f => fd.push(f.formData));

        return fd;
    }

    private _fillFileForm() {
        this._fileFormsCollectionList.forEach((f) => this._fileFormData.push(f.fileFormData));
        this._fileFormsFileTypeList.forEach((f) => this._fileFormData.push(f.fileFormData));
    }

    private _clear(): void {
        this._fileFormsCollectionList = new Array<IFileFormForColletion>();
        this._fileFormsFileTypeList = new Array<IFileFormForFileType>();
        this._fileFormData = [];
        this._formId = 0;

    }


    private _buildFormData(files: File[], destination: string): IFileFormData {

        let fnms: string[] = [];
        const fd = new FormData();

        for (let i = 0; i < files.length; i++) {

            fnms.push("file" + [i]);

            fd.append("file" + [i], files[i]);

        }

        fd.append('destination', destination);

        fd.append('fileNames', JSON.stringify(fnms));

        return { id: this._formId++, formData: fd, fileNames: fnms };
    }


    public syncWithResponse(res: any, ffd: IFileFormData): void {

        //res["file0"]:FileResponse

        if (this._syncFileTypeIfNeeded(res, ffd)) return;

        if(this._syncFileCollectionIfNeeded(res, ffd)) return;

    }


    private _syncFileTypeIfNeeded(res: any, ffd: IFileFormData): boolean {

        let index:number = this._fileFormsFileTypeList.findIndex((f) => { return f.fileFormData.id == ffd.id });

        if(index < 0) return false;

        let ftype: IFileFormForFileType = this._fileFormsFileTypeList[index];

        let fr: IFileResponse = res[ffd.fileNames[0]];

        if (!Utils.emptyValue(fr.error)){

            ftype.attribute.setError(fr.error);
            
        }else{
            
            ftype.collectionType.getEditRow().data[ftype.attribute.name] = fr.path + '/' + fr.name;
            // ftype.collectionType.getCurrentRow().data[ftype.attribute.name] = fr.path + '/' +  fr.name;
            this._fileFormsFileTypeList.splice(index, 1);

        }

        return true;
    }

    private _syncFileCollectionIfNeeded(res: any, ffd: IFileFormData): boolean {

        let index:number = this._fileFormsCollectionList.findIndex((f) => { return f.fileFormData.id == ffd.id });
       
        if(index < 0) return false;

        let fcoll: IFileFormForColletion = this._fileFormsCollectionList[index];

        
        this._fileFormsCollectionList.splice(index, 1);
        
        // let fr: FileResponse = res[ffd.fileNames[0]];

        // fcoll.collectionType.getEditRow().data[ftype.attribute.name] = fr.path + '/' + fr.name;
        // ftype.collectionType.getCurrentRow().data[ftype.attribute.name] = fr.path + '/' +  fr.name;

        // if (fr.error != null)
            // ftype.attribute.setError(fr.error);

        //TODO splice forcolleciton .... remove processed
        return true;

    }


    public uploadIsFinished(): boolean{
        return this._fileFormsCollectionList.length == 0 && this._fileFormsFileTypeList.length == 0;
    }

}

export interface IFileFormData {
    id: number;
    formData: FormData;
    fileNames: string[];
}


export interface IFileFormForFileType {
    fileFormData: IFileFormData,
    collectionType: CollectionType;
    attribute: Attribute;
}

export interface IFileFormForColletion {
    fileFormData: IFileFormData,
    collectionType: FileCollectionType;
}


export interface IFileResponse {
    name: string;
    path: string;
    error: string;
    // longError: string;
}