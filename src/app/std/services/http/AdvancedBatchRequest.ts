import { CollectionType, ROW_STATE, IRow, ILink } from "../../shared/types/CollectionType";
import { IBatchPart } from "./BatchRequest";
import { Attribute } from "../../shared/common/Attribute";
import { environment } from "src/environments/environment";
import { Utils } from "../../shared/common/Utils";
import { StaticService } from "../StaticService";

/* CollectionType koji je predan kao argument je root transakcije i on mora imat edit-row postavljen. Detalji su u normalnom modu 
   TODO: freez onCollectionChanged cu collectionType-u
*/
export class AdvancedBatchRequest{

    protected _parts: IBatchPart[] = [];

    protected _currentIndex: number = 0;

    protected _collectionType: CollectionType;

    protected _map: Map<string, IRowContext> = new Map<string, IRowContext>();

    private _originalRootPath:string;
    private _interpolatedRootPath:string;
    private _staticService:StaticService;

    constructor(collectionType: CollectionType, staticService:StaticService) {
        this._collectionType = collectionType;
        this._staticService = staticService;
    }


    protected _processRow(row: IRow): void {

        this._originalRootPath = this._interpolatedRootPath = this._collectionType.getUnmodifiedCollectionLink().href;//this._getPath(row, this._collectionType);

        let pCT = this._collectionType.getParentCollectionType();

        //Idi prema najvisem collectionTypu i prikupljaj kljuceve
        while (this._interpolatedRootPath.indexOf(CollectionType.LINK_KEY_NAME) >= 0 && pCT != null){

            let pos = this._interpolatedRootPath.lastIndexOf(CollectionType.LINK_KEY_NAME);

            let tmpRow = pCT.getCurrentRow();
            
            this._interpolatedRootPath = this._interpolatedRootPath.substring(0,pos) + tmpRow.data[pCT.getKey().name] + this._interpolatedRootPath.substring(pos + CollectionType.LINK_KEY_NAME.length);
            
            pCT = pCT.getParentCollectionType();
        }

        this._processData(row, this._collectionType, null);
        
        //TODO: deleted rows ne dela
        // this._collectionType.getDeletedRows().forEach(r => { this._processData(r, this._collectionType, null); });  
        
        // this._collectionType.attributes
        //     .filter(a => a instanceof CollectionType)
        //     .forEach(c => (<CollectionType>c.type).getDeletedRows().forEach(r => this._processSubCollection(c) ));        

        this._collectionType.attributes.filter(a => a.type instanceof CollectionType).forEach(a => this._processSubCollection(a) );

    }

    protected _processSubCollection(subCollectionTypeAttr:Attribute): void{

        (<CollectionType>subCollectionTypeAttr.type).getCollection().forEach(r => {

            // subCollectionType.setCurrentRow(r);
            (<CollectionType>subCollectionTypeAttr.type).setEditRow(r);

            this._processData(r, (<CollectionType>subCollectionTypeAttr.type), subCollectionTypeAttr);
            
            (<CollectionType>subCollectionTypeAttr.type).attributes.filter(a => a.type instanceof CollectionType).forEach(a => this._processSubCollection(a) );

        });
        
        //Unused
        // (<CollectionType>subCollectionTypeAttr.type).getDeletedRows().forEach(r => { this._processData(r, (<CollectionType>subCollectionTypeAttr.type), subCollectionTypeAttr); });        

    }

    protected _processData(row: IRow, collectionType: CollectionType, attr:Attribute): void {

        if(row.state == ROW_STATE.QUERY) return;

        let partId: string = 'part' + this._currentIndex++;

        this._parts.push({
            id: partId,
            operation: this._getMethod(row),
            path: this._getPath(row, collectionType),
            // payload: collectionType.getDataForSend(row.data), //Test
            payload: row.state == ROW_STATE.DELETED ? {} : collectionType.getDataForSend(row.data),
            parentPartId: collectionType.getParentCollectionType()==null?null:this._findPartId(collectionType.getParentCollectionType().getEditRow())
        });
        
        this._map.set(partId, { row: row,
                                collectionType: collectionType, 
                                parentRow:collectionType.getParentCollectionType()==null?null:collectionType.getParentCollectionType().getEditRow(), 
                                parentCollectionAttrName: attr==null?null:attr.name,
                                partId: partId
                              });
    }


    private _findPartId(row:IRow): string{
        let part:IBatchPart = null;

        this._map.forEach((rowCtx, id) => { 
            if(rowCtx.row == row){
                part = this._parts.find(p => p.id == rowCtx.partId);
                return;
            } 
        });

        return part == null? null : part.id;
    }

    protected _getMethod(row: IRow): string {
        switch(row.state){
            case ROW_STATE.NEW: return 'create'; //Add
            case ROW_STATE.CHANGED: return 'update';  //Update
            case ROW_STATE.DELETED: return 'delete';  //Delete
            // default: return 'dummy';
            default: console.log(row); throw new Error("Cannot resolve ROW_STATE for row (it might still be QUERY).");
        }
    }

    protected _getPath(row: IRow, collectionType: CollectionType): string {

        let link: ILink = collectionType.getUnmodifiedCollectionLink();

        let href:string = link.href.replace(this._originalRootPath, this._interpolatedRootPath);

        // if(collectionType == this._collectionType) return href;

        let pCT = collectionType.getParentCollectionType();

        //Ovisi o master redku
        while (href.indexOf(CollectionType.LINK_KEY_NAME) >= 0 && pCT != null && pCT.getEditRow() != null) {

            let pos = href.lastIndexOf(CollectionType.LINK_KEY_NAME);

            //Ako je roditelj root onda uzmi edit-row inace current row
            // let tmpRow = pCT == this._collectionType ? pCT.getEditRow() : pCT.getCurrentRow();
            let tmpRow = pCT.getEditRow();

            href = href.substring(0,pos) + tmpRow.data[pCT.getKey().name] + href.substring(pos + CollectionType.LINK_KEY_NAME.length);
            
            pCT = pCT.getParentCollectionType();
        }
 
        //Ako nije novi onda treba stavi key na kraj
        if(row.state != ROW_STATE.NEW && !collectionType.isSingleMode())
            href += "/" + row.data[collectionType.getKey().name];

        return href;
    }

    private _finalize(): void{
        //Inverse deleted and put it on the end
        let deleted = this._parts.filter(p => p.operation == "delete").sort((a, b) => b.id > a.id? 1 : b.id < a.id? -1 : 0);

        this._parts = this._parts.filter(p => p.operation != "delete").concat(deleted);

    }

    public getPartsForRow(): Object {

        this._clear();

        this._processRow(this._collectionType.getEditRow());

        this._finalize();

        return this._getParts();
    };

    public getPartsForRows(rows: IRow[]): Object {
        
        this._clear();

        rows.forEach((row) => { this._processRow(row); });

        this._finalize();

        return this._getParts();
    }


    public syncRowWithParts(resp: any): boolean {

        let errors:Array<string> = [];

        resp.parts.forEach((part: IBatchPart) => {

            let rowCtx: IRowContext = this._map.get(part.id);
            
            //Skip root row - will be processed later
            if(rowCtx.row == this._collectionType.getEditRow()) return;


            if(part.error != null){
                rowCtx.collectionType.setError(rowCtx.collectionType.error + part.error + "\n\r");
                
                if(!environment.production)
                    console.log(part.error);                    

                errors.push(part.error);
                return;
            }
            

            switch(part.operation){
                case "delete": 
                    //collectionType.remove ne more jer ovisi o current rowu parent-a

                    if(rowCtx.parentRow != null)
                        rowCtx.parentRow.data[rowCtx.parentCollectionAttrName].splice(rowCtx.parentRow.data[rowCtx.parentCollectionAttrName].indexOf(rowCtx.row), 1);            
                break;

                case "update": 
                case "create": 
                    rowCtx.collectionType.attributes.filter(a => !(a.type instanceof CollectionType)).forEach((attr) => rowCtx.row.data[attr.name] = part.payload[attr.name] );                
                break;
                
                default: console.log(part); throw new Error("Unrecognized operation when sync: " + part.operation);
            }

            rowCtx.row.state = ROW_STATE.QUERY;
        });

        
        //Ako postoje greske onda ne mjenja status retka
        //StaticService?
        if(!Utils.emptyValue(errors)){
            this._staticService.error(errors.map(e => e));            
            return false;  
        } 

        
        let r = this._collectionType.getEditRow();
        
        switch(r.state){
            case ROW_STATE.NEW: 
                this._collectionType.addRowToCollection(r);
            break;
            
            case ROW_STATE.CHANGED: 
                this._collectionType.replaceRowWithKeyInCollection(r);
            break;
            
            case ROW_STATE.DELETED:
                this._collectionType.removeEditRowFromCollection();
            break;

            default: console.log(r); throw new Error("Error when saving data with state " + r.state + " for row " + r);
            
        }

        this._collectionType.setCurrentRow(r);
        this._collectionType.clear();

        r.state = ROW_STATE.QUERY;

        return true;
    }

    protected _clear(): void {
        this._currentIndex = 0;
        this._parts = [];
        this._map.clear();
        this._originalRootPath = null;
        this._interpolatedRootPath = null;
    }

    protected _getParts(): Object { return { 'parts': this._parts }; }
}

export interface IRowContext {
    row: IRow;
    collectionType: CollectionType;
    parentCollectionAttrName: string;
    parentRow: IRow;
    partId: string;
    // parentPartId: string;
}