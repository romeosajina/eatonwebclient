import { CollectionType, IRow, ILink, LINK_TYPE, ROW_STATE } from '../../shared/types/CollectionType';
import { Attribute } from '../../shared/common/Attribute';

//http://andrejusb.blogspot.hr/2017/05/batch-requests-support-in-adf-bc-rest.html
export class BatchRequest {

    protected _parts: IBatchPart[] = [];
    protected _currentIndex: number = 0;

    protected _baseUrl: string;
    protected _collectionType: CollectionType;

    protected _subCollTypesAttributes: Attribute[];

    protected _dataInEdit: any = new Object();
    
    protected _map: Map<string, IRowInCollection> = new Map<string, IRowInCollection>();

    constructor(collectionType: CollectionType, baseUrl: string = null) {

        this._collectionType = collectionType;
        this._baseUrl = baseUrl;

    }


    protected _processRow(row: IRow, collectionType: CollectionType = this._collectionType): void {

        // console.log(row);

        collectionType.attributes.forEach((attr) => {

            if (attr.type instanceof CollectionType){
                this._subCollTypesAttributes.push(attr);
                // console.log("BatchRequest:_processRow attr.type instanceof CollectionType");
                // console.log(attr);                                

            }else{
                this._dataInEdit[attr.name] = row.data[attr.name];

            }

        });

        this._processData(row, collectionType);
        this._removeFromSubCollTypesAttributes(collectionType);

        // this._processDeletedRows(collectionType);

        if (collectionType == this._collectionType){
            this._processDeletedRows(collectionType);
            this._processSubCollTypesAttributes(row);            
        }
    }

    protected _processSubCollTypesAttributes(row: IRow): void {

        while (this._subCollTypesAttributes.length > 0) {

            let attr: Attribute = this._subCollTypesAttributes[0];
            let cType:CollectionType = <CollectionType>this._subCollTypesAttributes[0].type;
            //Mora biti u collection-u
            // if (row.data[attr.name].length >= 0)
            //     this._processRows(row.data[attr.name], <CollectionType>attr.type);
            
            if (cType.getCollection().length >= 0)
                this._processRows(cType.getCollection(), cType);
                    

            this._removeFromSubCollTypesAttributes(cType);
                
            this._processDeletedRows(cType);
        }

    }

    protected _processRows(rows: IRow[], collectionType: CollectionType = this._collectionType): void {
        rows.filter(row => row.state != ROW_STATE.QUERY).forEach(row => this._processRow(row, collectionType));
    }

    protected _removeFromSubCollTypesAttributes(collectionType: CollectionType): void {

        let indx: number = this._subCollTypesAttributes.findIndex((attr) => { return attr.type == collectionType; });

        if (indx >= 0)
            this._subCollTypesAttributes.splice(indx, 1);
    }

    protected _processDeletedRows(collectionType: CollectionType): void{
   
        collectionType.getDeletedRows().forEach(row => {
            this._processData(row, collectionType);
        });

        // collectionType.attributes.filter(attr => attr.type instanceof CollectionType).forEach(attr => this._processDeletedRows(<CollectionType>attr.type));
    }

    protected _processData(row: IRow, collectionType: CollectionType = this._collectionType): void {

        let partId: string = 'part' + this._currentIndex++;

        // this._map.set(partId, row);
        this._map.set(partId, { row: row, collectionType: collectionType });

        // if (row.state != ROW_STATE.QUERY) {

            this._parts.push({
                id: partId,
                operation: this._getMethod(row),
                path: this._getPath(row, collectionType),
                payload: row.state == ROW_STATE.DELETED ? {} : this._dataInEdit
            });

        // }

        this._dataInEdit = new Object();
    }

    protected _getMethod(row: IRow): string {
        switch(row.state){
            case ROW_STATE.NEW: return 'create'; //Add
            case ROW_STATE.CHANGED: return 'update';  //Update
            case ROW_STATE.DELETED: return 'delete';  //Delete
            // default: return null;
            default: console.log(row); throw new Error("Cannot resolve ROW_STATE for row (it might still be QUERY).");
        }
    }

    protected _getPath(row: IRow, collectionType: CollectionType = this._collectionType): string {

        // let link: Link = CollectionType.findLinkByRel(row.data.links, LINK_TYPE.SELF);

        // let link: ILink = collectionType.getCollectionLink();
        let link: ILink = collectionType.getCollectionLinkForEdit();

        let href:string = link.href;

        if(row.state == ROW_STATE.DELETED || (row.state == ROW_STATE.CHANGED && !collectionType.isSingleMode()))
            href += '/'+row.data[collectionType.getKey().name];

        return href;
    }

    public getPartsForRow(row: IRow = this._collectionType.getEditRow()): Object {

        this._clear();

        this._processRow(row);

        return this._getParts();
    };

    public getPartsForRows(rows: IRow[]): Object {

        this._clear();

        rows.forEach((row) => {
            console.log(row);
            this._processRow(row);
        });

        return this._getParts();
    }


    public syncRowWithParts(resp: any): void {

        resp.parts.forEach((part: IBatchPart) => {

            // let row:Row = this._map.get(part.id);

            let rowInColl: IRowInCollection = this._map.get(part.id);

            if(part.operation == 'delete'){
                //TODO: pogledat da li radi
                rowInColl.row.data = null;
                rowInColl.row = null;
                return;
            }

            rowInColl.collectionType.attributes.forEach((attr) => {

                if (!(attr.type instanceof CollectionType))
                    rowInColl.row.data[attr.name] = part.payload[attr.name];

            });

            rowInColl.row.state = ROW_STATE.QUERY;
            // row.data = part.payload;
        });

    }

    protected _clear(): void {
        this._currentIndex = 0;
        this._subCollTypesAttributes = [];
        this._parts = [];
        this._map.clear();
    }

    protected _getParts(): Object { return { 'parts': this._parts }; }

}

export interface IBatchPart {
    id: string;
    path: string;
    operation: string;
    payload: Object;
    error?: string;
    parentPartId?: string;
}


export interface IRowInCollection {
    row: IRow;
    collectionType: CollectionType;
}