import { Injectable } from '@angular/core';
import { CollectionType, IRow } from '../shared/types/CollectionType';
import { User } from '../User';
import { environment } from '../../../environments/environment';
import { DataService } from './data.service';
import { Http } from '@angular/http';
import { StaticService } from './StaticService';

import * as CryptoJS from 'crypto-js';

import { Buffer } from 'buffer';
import { Company } from '../Company';

@Injectable()
export class UserService {

  // private user:User = new User();
  private company:Company = new Company();
  private dataService:DataService;

  private static readonly USER_KEY = "xyz";
  private static readonly KEY = "dwemfkremgo12e12234pom23kmf23";
      
  constructor(private http:Http, private staticService:StaticService) {

    this.dataService = new DataService(http, staticService);

    let u:string = (sessionStorage.getItem(UserService.USER_KEY));
    if(u != null){

        // Decrypt 
        // let bytes  = CryptoJS.AES.decrypt(u, UserService.KEY);
        // let plaintext = bytes.toString(CryptoJS.enc.Utf8);
        // this.user.setParams(JSON.parse(plaintext));   
        
        let bytes  = CryptoJS.AES.decrypt(u, UserService.KEY);
        let plaintext = bytes.toString(CryptoJS.enc.Utf8);
        this.company.setParams(JSON.parse(plaintext));   
                
    }
    
    // this.user.setParams(<User>{
    //     id: "1",
    //     name: "Romeo",
    //     surname: "Šajina",
    //     email: "romeo.sajina@gmail.com",
    //     password: "r"
    // });

    // this.company.setParams({
    //     id: 1,
    //     name: "Company 1",
    //     city: "Pula",
    //     address: "Pula 1",
    //     latitude: "123.000",
    //     longitude: "321.000",
    //     photo: "",
    //     phone: "098 123 3213",
    //     delivery: true,
    //     pickUp: true,
    //     minDeliveryPrice: "20.00",
    //     maxDestinationRange: "20.00",
    //     maxFreeDeliveryRange: "20.00",
    //     description: null
    // });

    
    //Bindanje funkcije data service-a na getBasicAuth()
    DataService.getBasicAuth = this.getBasicAuth.bind(this);
   }

  public getBasicAuth(): string{
    // console.log(this.user.email+":"+this.user.password);    
    // return btoa(this.user.email+":"+this.user.password);
    return this.company == null? btoa("anonymous:user") : btoa(this.company.email+":"+this.company.password);
  }

  public isAuthenticated(): boolean{
    // return this.user != null && this.user.id != null;
    return this.company != null && this.company.id != null;
  }

  setCompany(company:Company): void{
    if(this.company == null)
      this.company = new Company();
    
    this.company.setParams(company);

    //Klasa se instancira odmah pri startanju aplikacije (vec na login-u, pa se i header-i inicijaliziraju, treba ih reinicijalizirat)
    this.dataService.requestOptions.headers = this.dataService['initHeaders'](); 

    this._setSessionStorage(company);  
  }

  // setUser(user:User): void{
  //   if(this.user == null)
  //     this.user = new User();
    
  //   this.user.setParams(user);

  //   //Klasa se instancira odmah pri startanju aplikacije (vec na login-u, pa se i header-i inicijaliziraju, treba ih reinicijalizirat)
  //   this.dataService.requestOptions.headers = this.dataService['initHeaders'](); 

  //   this._setSessionStorage(user);    
  // }

  // getUser(): User{ return this.user; }
  getCompany(): Company{ return this.company; }

  logout(): void{
    // this.user = null;
    this.company = null;
    
    this._setSessionStorage(null);
  }

  private _setSessionStorage(company:Company): void{
    if(company == null){
      
      sessionStorage.removeItem(UserService.USER_KEY);

    }else{

      // Encrypt 
      let ciphertext = CryptoJS.AES.encrypt(JSON.stringify(company), UserService.KEY);

      sessionStorage.setItem(UserService.USER_KEY, ciphertext.toString());        
    }
      
  }


  public generateHash(s:string): string{
    var hash = CryptoJS.SHA256(s);
    return hash.toString(CryptoJS.enc.Base64);
  }

}
