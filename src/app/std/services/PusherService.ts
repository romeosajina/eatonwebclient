
// import Pusher from 'pusher-js/react-native';

// import * as Pusher from 'pusher-js'; //        "../node_modules/pusher-js/dist/web/pusher.min.js"

import { EventEmitter } from '@angular/core';
import { CollectionType } from '../shared/types/CollectionType';


export class OLDPusherService{

    private _collectionTypes:Map<string, CollectionType> = new Map();
/*
    private _pusher:Pusher;

    constructor(){
        
        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;
        
        this._pusher = new Pusher('c0a53cea58f28c26b294', {
            cluster: 'eu',
            encrypted: true
        });

        var channel = this._pusher.subscribe('my-channel');
        
        channel.bind('my-event', function(data) {
            console.log(data);
            alert(data.message);
        });
        
        console.log(this._pusher);
        
        this._registerChannels();
   
       }       
   
   
       public registerCollectionType(name:string, collectionType:CollectionType): void{
   
           let c = this._collectionTypes.get(name);
           
           if(c != null)
               throw new Error("CollectionType for name '"+name+"' already registered!");
   
           this._collectionTypes.set(name, collectionType);
       }
   
       
       private _registerChannels(): void{
   
           for(let key in CHANNEL_NAMES){
              let c:string = CHANNEL_NAMES[key];           
      
              var channel = this._pusher.subscribe(c);
           
               for (const eventKey in EVENTS) {
                   let e:string = EVENTS[eventKey];
                    
                   channel.bind(e, function(data) { this._processEvent(c, e, data); });
                   
               }
           }
       } 
   
       private _processEvent(channelName:string, eventName:string, data:PushedData): void{
   
           console.log(channelName + " " +eventName);        
           console.log(data);
           alert(data);
   
   
           let ct = this._collectionTypes.get(channelName);
       
           if(ct == null) return;
   
           switch(eventName){
               case EVENTS.ADDED: 
               
                   //TODO: continue
                   ct.findByKey();
   
               break;
               case EVENTS.CHANGED: 
               
               break;
               case EVENTS.DELETED:
   
               break;
               default: throw new Error("Unable to resolve event name: '"+eventName+"'"); 
           }
       }
  */ 
   }
   
   export const CHANNEL_NAMES = {
       COMMENTS: 'comments-channel',
       // COURSES: 'courses-channel',
       FILES: 'files-channel'
   }
   
   const EVENTS = {
       ADDED: 'added-event',
       CHANGED: 'changed-event',
       DELETED: 'deleted-event'
   }
   
   interface PushedData{
       data:any;
   }



/*
// import Pusher from 'pusher-js/react-native';

//import * as Pusher from 'pusher-js'; //        "../node_modules/pusher-js/dist/web/pusher.min.js"

import { EventEmitter } from '@angular/core';
import { CollectionType } from '../../../../shared/types/CollectionType';

// import { ab } from '../js/autobahn.js';

import * as ab from '../js/autobahn.js';

// import ab = require('../js/autobahn.js');

export class PusherService{

    // public onCommentChanged:EventEmitter<any> = new EventEmitter<any>();
    // public onCommentAdded:EventEmitter<any> = new EventEmitter<any>();

    // private _collectionTypes:CollectionType[] = [];

    private _collectionTypes:Map<string, CollectionType> = new Map();

    //private _pusher:Pusher;

    private _conn = null;

    constructor(){
    
        //console.log(ab);

        let ses = new ab.Session('ws://localhost:8081', this, this._onCloseConnection, {'skipSubprotocolCheck': true});

        
        // console.log(this._conn);
    }       

    private _onOpenConnection(conn): void{
        this._conn = conn;
        // console.log(this);          
        
        this._conn.subscribe('kittensCategory', function(topic, data) {
            console.log('New article published to category "' + topic + '" : ' + data.title);
        });
        
        this._registerChannels();        
        console.log("WebSocked connection opened");        
    }

    private _onCloseConnection(): void{
        console.warn('WebSocket connection closed');
    }


    public registerCollectionType(name:string, collectionType:CollectionType): void{

        let c = this._collectionTypes.get(name);
        
        if(c != null)
            throw new Error("CollectionType for name '"+name+"' already registered!");

        this._collectionTypes.set(name, collectionType);
    }

    
    private _registerChannels(): void{

        for(let key in CHANNEL_NAMES){
           let c:string = CHANNEL_NAMES[key];           
   
           var channel = this._conn.subscribe(c, this._processEvent);
        }
    }


    private _processEvent(channelName:string, data:PushedData): void{

        console.log(channelName);        
        console.log(data);
        alert(data);

        console.log(this);
                

        // let ct = this._collectionTypes.get(channelName);
    


     
    }

}

export const CHANNEL_NAMES = {
    COMMENTS: 'comments-channel',
    // COURSES: 'courses-channel',
    FILES: 'files-channel'
}

const EVENTS = {
    ADDED: 'added-event',
    CHANGED: 'changed-event',
    DELETED: 'deleted-event'
}

interface PushedData{
    data:any;
    eventType:EventType
}

enum EventType{
    ADDED,
    CHANGED,
    DELETED
}




*/


/*


// import Pusher from 'pusher-js/react-native';

import * as Pusher from 'pusher-js'; //        "../node_modules/pusher-js/dist/web/pusher.min.js"

import { EventEmitter } from '@angular/core';
import { CollectionType } from '../../../../shared/types/CollectionType';


export class PusherService{

    // public onCommentChanged:EventEmitter<any> = new EventEmitter<any>();
    // public onCommentAdded:EventEmitter<any> = new EventEmitter<any>();

    // private _collectionTypes:CollectionType[] = [];

    private _collectionTypes:Map<string, CollectionType> = new Map();

    private _pusher:Pusher;

    constructor(){
        
        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;
        
        this._pusher = new Pusher('c0a53cea58f28c26b294', {
            cluster: 'eu',
            encrypted: true
        });

        var channel = this._pusher.subscribe('my-channel');
        
        channel.bind('my-event', function(data) {
            console.log(data);
            alert(data.message);
        });
        
        console.log(this._pusher);
        
        this._registerChannels();
   
       }       
   
   
       public registerCollectionType(name:string, collectionType:CollectionType): void{
   
           let c = this._collectionTypes.get(name);
           
           if(c != null)
               throw new Error("CollectionType for name '"+name+"' already registered!");
   
           this._collectionTypes.set(name, collectionType);
       }
   
       
       private _registerChannels(): void{
   
           for(let key in CHANNEL_NAMES){
              let c:string = CHANNEL_NAMES[key];           
      
              var channel = this._pusher.subscribe(c);
           
               for (const eventKey in EVENTS) {
                   let e:string = EVENTS[eventKey];
                    
                   channel.bind(e, function(data) { this._processEvent(c, e, data); });
                   
               }
           }
       } 
   
       private _processEvent(channelName:string, eventName:string, data:PushedData): void{
   
           console.log(channelName + " " +eventName);        
           console.log(data);
           alert(data);
   
   
           let ct = this._collectionTypes.get(channelName);
       
           if(ct == null) return;
   
           switch(eventName){
               case EVENTS.ADDED: 
               
                   //TODO: continue
                   ct.findByKey();
   
               break;
               case EVENTS.CHANGED: 
               
               break;
               case EVENTS.DELETED:
   
               break;
               default: throw new Error("Unable to resolve event name: '"+eventName+"'"); 
           }
       }
   
   }
   
   export const CHANNEL_NAMES = {
       COMMENTS: 'comments-channel',
       // COURSES: 'courses-channel',
       FILES: 'files-channel'
   }
   
   const EVENTS = {
       ADDED: 'added-event',
       CHANGED: 'changed-event',
       DELETED: 'deleted-event'
   }
   
   interface PushedData{
       data:any;
   }

*/