import { Injectable } from '@angular/core';
import { CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { UserService } from './UserService';


@Injectable()
export class AuthGuard implements CanActivate {


  private readonly ADMIN_ROUTES:string[] = [
  ];

  private readonly ANONYMOUS_USER_ROUTES:string[] = [
    "/login",
    "/logout",
    "/home"
  ];


  constructor(private userService:UserService){

  }
  /*
  canActivate() :boolean{
    return AppComponent.userIsAdmin();
  }*/

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean{
    return this.ANONYMOUS_USER_ROUTES.find(r => r == state.url) != null || this.userService.isAuthenticated();
  };
  

}
