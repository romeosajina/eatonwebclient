import { Injectable, EventEmitter } from "@angular/core";



@Injectable()
export class StaticService {


    public onLoading:EventEmitter<boolean> = new EventEmitter<boolean>();

    public onError:EventEmitter<string> = new EventEmitter<string>();


    public loadingStarted(): void{
        this.onLoading.emit(true);
    }

    public loadingFinished(): void{
        this.onLoading.emit(false);
    }

    public error(s:any): void{
        this.onError.emit(s.toString());
    }

    
}
