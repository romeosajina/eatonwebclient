import { Injectable } from '@angular/core';

import * as Pusher from 'pusher-js';

import { environment } from 'src/environments/environment';
import { EventEmitter } from '@angular/core';
import { IOrder } from '../shared/common/AttributesBuilder';
import { UserService } from './UserService';


@Injectable({ providedIn: 'root' })
export class PusherService {
  
  static readonly ON_CHANGED_EVENT = "on-changed-event";
  static readonly ON_ADDED_EVENT   = "on-added-event";
  static readonly ON_DELETED_EVENT = "on-deleted-event";
  
  static readonly COMPANY_ORDERS_CHANNEL = "company-orders-channel";
  // static readonly COMPANY_ORDERS_CHANNEL = "company-orders-channel-";

  private _pusher:Pusher;
  // public onOrderChanged:EventEmitter<any> = new EventEmitter<any>();
  public onOrderAdded:EventEmitter<IOrder> = new EventEmitter<IOrder>();
  public onOrderDeleted:EventEmitter<IOrder> = new EventEmitter<IOrder>();

  constructor(private userService:UserService){
      
      // Pusher.logToConsole = !environment.production;
      Pusher.logToConsole = false;
      
      this._pusher = new Pusher('c0a53cea58f28c26b294', {
                                                          cluster: 'eu',
                                                          encrypted: true
                                                      });

      
      this._bindChannels();

      // var channel = this._pusher.subscribe('my-channel');

      // channel.bind('my-event', function(data) {
      //     console.log(data);
      //     alert(data.message);
      // });
      
      // console.log(this._pusher);
    }

    private _bindChannels(): void{

      // let channel = this._pusher.subscribe(PusherService.COMPANY_ORDERS_CHANNEL + this.userService.getCompany().id);
      let channel = this._pusher.subscribe(PusherService.COMPANY_ORDERS_CHANNEL);

      channel.bind(PusherService.ON_ADDED_EVENT, (order:IOrder) => {
        // console.log("NEW ORDER ADDED", order);
        this.onOrderAdded.emit(order);
      });

      channel.bind(PusherService.ON_DELETED_EVENT, (order:IOrder) => {
        // console.log("ORDER DELETED", order);
        this.onOrderDeleted.emit(order);
      });

    }
  
}
