import { Injectable, EventEmitter } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import { _throw } from 'rxjs/Observable/throw';

// import 'rxjs/add/observable/throw';

//import 'rxjs/Observable';//{Observable} from 


import { Attribute } from '../shared/common/Attribute';
//import { AttributeType } from '../shared/AttributeType';
import { ListOfValuesType } from '../shared/types/ListOfValuesType';
import { FileType } from '../shared/types/FileType';
import { BaseType } from '../shared/types/BaseType';
import { BasicType } from '../shared/types/BasicType';
import { CollectionType, IRow, ILink, LINK_TYPE, ROW_STATE } from '../shared/types/CollectionType';
import { FileCollectionType } from '../shared/types/FileCollectionType';


import { ErrorType, Messages, DataType } from '../shared/common/StaticValues'; //DataType, DisplayType,
// import { Key } from '../shared/validation/ValidationProperties';
import { /*ValidationUtils, StringUtils,*/ Utils } from '../shared/common/Utils';
import { BatchRequest } from './http/BatchRequest';
import { FileUpload, IFileFormData } from './http/FileUpload';
import { environment } from '../../../environments/environment';
import { StaticService } from './StaticService';
import { AdvancedBatchRequest } from './http/AdvancedBatchRequest';
import { AdvancedFileUpload } from './http/AdvancedFileUpload';

@Injectable()
export class DataService {

  commData: IComunicationData;

  // public collectionType: CollectionType;


  //public basicQueryString = "limit=100000";
  public basicQueryString = "";

  public onDataSaved:EventEmitter<void> = new EventEmitter<void>(true);
  public onDataSaveFailed:EventEmitter<void> = new EventEmitter<void>(true);
  public onFilesUploaded:EventEmitter<void> = new EventEmitter<void>(true);
  // public onError:EventEmitter<any> = new EventEmitter<any>(true);

  private initHeaders(): Headers {
    let hs: Headers = new Headers();
    hs.append('Content-Type', 'application/json');   
    hs.append('Accept', 'application/json');
    hs.append("Authorization", "Basic "+DataService.getBasicAuth());
    hs.append("App-User-Is-Company", "true");
    return hs;
  }

  private initBatchHeaders(): Headers {
    let hs: Headers = new Headers();
    hs.append('Content-Type', 'application/json');       
    hs.append('Accept', 'application/json');
    // hs.append('Origin', '*');
    // hs.append('Access-Control-Allow-Headers', '*');
    // hs.append('Access-Control-Allow-Origin', '*');
    hs.append("Authorization", "Basic "+DataService.getBasicAuth());
    hs.append("App-User-Is-Company", "true");
    return hs;
  }

  private initUploadHeaders(): Headers {
    let hs: Headers = new Headers({});
    hs.delete('Content-Type');
    hs.append("Authorization", "Basic "+DataService.getBasicAuth());
    hs.append("App-User-Is-Company", "true");
    return hs;
  }

  public static getBasicAuth:Function = function(){ return btoa("dummy:dummy"); }

  public readonly requestOptions = new RequestOptions({ headers: this.initHeaders() });
  public readonly batchRequestOptions = new RequestOptions({ headers: this.initBatchHeaders() });
  public readonly uploadRequestOptions = new RequestOptions({ headers: this.initUploadHeaders() });

  public static readonly FILE_UPLOAD_URL_PART = "file/upload";
  public static readonly FILE_DELETE_URL_PART = "file/delete";

  constructor(public http: Http, private staticService:StaticService) { }

  getDataset(collectionType: CollectionType/* = this.collectionType*/): void {

    let link: ILink = collectionType.getCollectionLink();

    this.get(link.href + this.getQueryString(link.name)).subscribe((data) => {
      if(data.items == null)
        collectionType.setCollection([data]);
        
      else      
        collectionType.setCollection(data.items);
    });

  }

  get(url: string): any { this.staticService.loadingStarted();
    return this.http.get(url, this.requestOptions).map((res) => {  return this._processResponse(res); },
      (error) => { return this.handleError(error); })
      .catch((error) => { return this.handleError(error) });
  }

  save(url: string, data: any): any { this.staticService.loadingStarted();
    return this.http.post(url, data, this.requestOptions).map((res) => { return this._processResponse(res); },
      (error) => { return this.handleError(error); })
      .catch((error) => { return this.handleError(error) });
  }

  saveBatch(url: string, data: any): any { this.staticService.loadingStarted();
    return this.http.post(url, data, this.batchRequestOptions).map((res) => { return this._processResponse(res); },
      (error) => { return this.handleError(error); })
      .catch((error) => { return this.handleError(error) });
  }


  update(url: string, data: any): any { this.staticService.loadingStarted();
    return this.http.patch(url, data, this.requestOptions).map((res) => { return this._processResponse(res); },
      (error) => { return this.handleError(error); })
      .catch((error) => { return this.handleError(error) });

  }

  delete(url: string): any { this.staticService.loadingStarted();
    return this.http.delete(url, this.requestOptions).map((res) => { return this._processResponse(res); },
      (error) => { return this.handleError(error); })
      .catch((error) => { return this.handleError(error) });
  }

  remove(collectionType: CollectionType/* = this.collectionType*/): void {
    // this.delete(collectionType.getCollectionLink().href + '/' + collectionType.getEditRow().data[collectionType.getKey().name]).subscribe((res) => {
    this.delete(collectionType.getOperationLink()).subscribe((res) => {
      collectionType.removeEditRowFromCollection();
    });
    
  }

  //https://www.syncfusion.com/forums/128693/data-binding-for-input-type-file
  upload(url: string, data: any): any {
    return this.http.post(url, data, this.uploadRequestOptions).map((res) => { return res/*.json()*/; },
                                                                          (error) => { return this.handleError(error); })
                                                                          .catch((error) => { return this.handleError(error) });
  }

  private handleError(error: any): Observable<any> {

    if(!environment.production){
      console.log(error);
      console.log("--------------- " + error._body + " ---------------");  
    }

    let errorMsg:string = error._body;

    if(error._body instanceof ProgressEvent){
      errorMsg = Messages.SERVER_ERROR;

    }else if(error._body.error != null){
      errorMsg = error._body.error;

    }else{

      try {
        
        var o = JSON.parse(error._body);
        if(o.message != null)
          errorMsg = o.message;
        else if(o.error != null)
          errorMsg = o.error;

      } catch (e) {
        if(!environment.production){
          console.log("Nepoznata greška sa servera!");
          console.log(e);
        }
      }

    }      

    // this.onError.emit(errorMsg);
  
    this.onDataSaveFailed.emit();

    this.staticService.loadingFinished();
    this.staticService.error(errorMsg);

    // return Observable.throw(error);
    return _throw(error);
  }

  private _processResponse(res): Observable<any>{

    this.staticService.loadingFinished();

    let o:Observable<any> = null;
    try{
        o = res.json();
  
        if(!environment.production)
          console.log(o);
  
      }catch(e){
        console.log(res);
      }

    return o;
  }


  /**
   * @description Recursively (when CollectionType) check if attribute needs list of values and fetch it 
   */
  getDataNeededToEdit(collectionType: CollectionType/* = this.collectionType*/): void {

    // if(ValidationUtils.emptyValue(this.dataToEdit)) 
    if (Utils.emptyValue(collectionType.getEditRow().data))
      throw new Error("Data to edit can't be null when fetching needed list of values.");

    collectionType.attributes.forEach((attr) => {

      if (attr.needsListOfValues()) {

        if (attr.type instanceof CollectionType)

          this.getDataNeededToEdit((<CollectionType>attr.type));

        else

          this.fetchLOV(attr, collectionType.getEditRow().data);

      }

    });

  }

  /**
   * @description Fetch's and saves list of values for attribute
   * @argument attr: attribute where to fetch list of values 
  */
  public fetchLOV(attr: Attribute, data: any, link:ILink = null): void {

    if(link == null){
      link = CollectionType.findLinkByRelAndName(data.links, LINK_TYPE.LOV, (<ListOfValuesType>attr.type).dataName);
    }

    if (link == null) throw new Error("Could not find link for list of values (" + attr.header + ")");

    //TODO: Ako je moguće dohvatit sve podatke svih atributa odjednom
    this.get(link.href + this.getQueryString(link.name)).subscribe((data) => {
      (<ListOfValuesType>attr.type).data = data.items;
    });

  }

  getQueryString(resName: string): string {
    if(this.commData == null) return "";

    let getOpt: IGetOptions = this.commData.getOptions.find((opt) => { return opt.resourceName == resName });

    let bQSEmpty: boolean = Utils.emptyValue(this.basicQueryString);

    if (getOpt == null)
      return bQSEmpty ? "" : "?" + this.basicQueryString;

    else
      return bQSEmpty ? "?" + getOpt.options : "?" + this.basicQueryString + "&" + getOpt.options;

  }


  saveDataAndFiles(collectionType: CollectionType/* = this.collectionType*/): void {

    // console.log(collectionType);
    
    collectionType.validateRow();

    if(!environment.production)
      console.log(collectionType);
    

     if (collectionType.needsFileUpload()){
        this.uploadFiles(collectionType);

     }else{

        if(collectionType.isInBatchMode())
            this.saveDataBatch(collectionType);

        else
          this.saveData(collectionType);

     }
      
      // console.log("_________________");
      
    //if( !this.uploadFilesIfNeeded() ) this.saveData();  

  }

  saveDataBatch(collectionType: CollectionType/* = this.collectionType*/): void {

    if (!collectionType.isValid()) return;

    // let br: BatchRequest = new BatchRequest(collectionType);
    let br: AdvancedBatchRequest = new AdvancedBatchRequest(collectionType, this.staticService);

    let parts = br.getPartsForRow();

    if(!environment.production){
      console.log(parts);
      console.log(JSON.stringify(parts));  
    }

    // if(1==1)return;

    // let state: ROW_STATE = collectionType.getEditRow().state;

    //'http://127.0.0.1:7101/HrRest-RESTWebService-context-root/rest/'
    // this.saveBatch(collectionType.getCollectionLink().href, parts).subscribe((data) => {
    this.saveBatch(this.commData.baseUrl+'batch', parts).subscribe((data) => {

      if(br.syncRowWithParts(data))
        this.onDataSaved.emit(); 
      else
        this.onDataSaveFailed.emit();

      // if (state == ROW_STATE.NEW)
      //   collectionType.addRowToCollection(collectionType.getEditRow());

      // else if (state == ROW_STATE.CHANGED)
      //   collectionType.replaceRowWithKeyInCollection(collectionType.getEditRow());

      // else {
      //   console.log(this);
      //   alert('Something went wrong when saving the data');
      //   throw new Error('Something went wrong when saving the data');
      // }

      // collectionType.setEditRowForCurrentAndClear();
      
      // this.onDataSaved.emit(); 
    });
  
  }
  /**/

  //https://stackoverflow.com/questions/32423348/angular-post-uploaded-file => Progress
  //https://www.npmjs.com/package/angular-progress-http
  uploadFiles(collectionType:CollectionType/* = this.collectionType*/): void {
   
    // let fu:FileUpload = new FileUpload(collectionType);
    let fu:AdvancedFileUpload = new AdvancedFileUpload(collectionType);
    
    let ffd:IFileFormData[] = fu.getFileFormData();
    
    if(!environment.production){
      console.log(ffd);
      // console.log(ffd[0]);  
    }
    
    // if(1==1)return; 
    
    ffd.forEach(f => {

      this.upload(this.commData.baseUrl + DataService.FILE_UPLOAD_URL_PART, f.formData).subscribe( (response) => {


          if(!environment.production) console.log(response._body);
    

          let res:any = JSON.parse(response._body);
            
          
          if(!environment.production) console.log(res);
    

          fu.syncWithResponse(res, f);
          

          if(fu.uploadIsFinished()){

            if(collectionType.isInBatchMode())
              this.saveDataBatch(collectionType);
            else
              this.saveData(collectionType);

          }

    
        });       

    });

  }







  /**Saving one row - not using batch mode */
  saveData(collectionType: CollectionType/* = this.collectionType*/): void {
    //TODO: omogućiti slanje samo potrebnih podataka....sve osim collectionType?
    if(!environment.production){
      console.log(collectionType);    
      console.log(JSON.stringify(collectionType.getDataForSend()));
    }


    if (!collectionType.isValid()) return;

    if (collectionType.getEditRow().state == ROW_STATE.NEW) {

      // this.save(collectionType.getCollectionLink().href, collectionType.getEditRow().data).subscribe((data) => {
      this.save(collectionType.getCollectionLink().href, collectionType.getDataForSend()).subscribe((data) => {

        collectionType.setDataForEditRow(data, ROW_STATE.QUERY);

        collectionType.addRowToCollection(collectionType.getEditRow());

        collectionType.setCurrentRow(collectionType.getEditRow());

        collectionType.clear();

        this.onDataSaved.emit();
      });

    } else if (collectionType.getEditRow().state == ROW_STATE.CHANGED) {

      // this.update(collectionType.getCollectionLink().href + '/' + collectionType.getEditRow().data[collectionType.getKey().name], collectionType.getEditRow().data).subscribe((data) => {
      // this.update(collectionType.getOperationLink(), collectionType.getEditRow().data).subscribe((data) => {
      this.update(collectionType.getOperationLink(), collectionType.getDataForSend()).subscribe((data) => {
        
        collectionType.setDataForEditRow(data, ROW_STATE.QUERY);

        collectionType.replaceRowWithKeyInCollection(collectionType.getEditRow());

        collectionType.setCurrentRow(collectionType.getEditRow());

        collectionType.clear();

        this.onDataSaved.emit();
      });

    } else {
      this.onDataSaveFailed.emit();
      console.log(this);
      alert('Something went wrong when saving the data');
      throw new Error('Something went wrong when saving the data');
    }

  }




}





export interface IComunicationData {
  // url:string;
  //  baseLinks:Link[] | null;
  baseUrl: string | null;
  getOptions: IGetOptions[];
}

export interface IGetOptions {
  resourceName: string | null;
  options: string | null;
}





 //TODO FIX METHOD
  /**
   * @description upload files, handle response and shows error message if needed, after all attributes are uploaded it saves data
   * @param attr attribute that needs to upload files 
   */

  // uploadAttrFiles(attr: Attribute, data: any): void {
    /*
   let ffd:FileFormData = (<FileType>attr.type).buildFormData();

   this.upload(this.commData.baseUrl+'upload', ffd.formData).subscribe( (response) => {

     console.log(response._body);

     let res:any = JSON.parse(response._body);
   
     console.log(res);

     attr.handleFileUploadResponse(res, ffd.fileNames, data);  

   //this.saveData(); ????????????      

   });
   */
  // }


  // uploadFiles(collectionType: CollectionType/* = this.collectionType*/): void {

  //   let fileUpload:FileUpload = new FileUpload(collectionType);

  //   collectionType.attributes.forEach((attr) => {

  //     if(attr.type instanceof FileCollectionType)
  //       console.log("FCT");
        
  //     else if(attr.type instanceof CollectionType)
  //       this.uploadFiles(attr.type)

  //     else
  //       this.uploadAttrFiles(attr, collectionType.getEditRow().data);

  //   });

  //   //this.saveData(); AFTER FILE UPLOAD
  //   console.log("UPLOAD FILES END________________________________________________");
  // }


  /*static findLinkByRelAndName(links:Link[], rel:string, name:string){
    return links.find((link:Link) => { return link.rel == rel && link.name == name; } );
  }*/






// export const DATA_STATUS = {
//   NEW: 'NEW',
//   EXISTING: 'EXISTING',
// }

// export const METHOD = {
//   GET: 'GET',
//   UPDATE: 'PATCH',
//   SAVE: 'POST',
//   DELETE: 'DELETE'
// }



// GET - will do a Read

// POST - will do a Create

// PATCH - will do an Update

// DELETE - will do a Delete




// onRequestViewChange:EventEmitter<any> = new EventEmitter<boolean>(true);//TRUE = EDIT, FALSE = TABLE

//* public dataset:any = [];
//* public data:any;
//public dataCopy:any;

// public dataToEdit:any;
//* public dataToEditStatus:string = DATA_STATUS.EXISTING;
//* public dataIndex:number;




// private numOfFileAttrsForUpload:number = 0;
// private numOfFileAttrsUploaded:number = 0;

// this.header.append('Content-Type', 'application/vnd.oracle.adf.resourceitem+json');
// this.header.append('Accept', 'application/json');
// this.header = new Headers([{'Content-Type': 'application/vnd.oracle.adf.resourceitem+json'},{'Accept': 'application/json'}]);
// this.requestOptions.headers = this.header;


// this.uploadHeader.delete('Content-Type');
// this.uploadRequestOptions.headers = this.uploadHeader;



  // clearAndClose(): void{
    // this.collectionType.clear();
    // this.onRequestViewChange.emit(false);
  // }

  // validateAttribute(attr:Attribute): void{
  //   this.collectionType.validate(attr);
  // }

  // dataValid(): boolean{
  //   return this.collectionType.isValid();
  // }



  /**
   * @description Upload files if needed and save the data after upload is complete, if there is no files to upload it doesn't save the data
   * @returns true if at least one attribute needs to upload files, false if there is no files to upload
   */
  // uploadFilesIfNeeded(): boolean{
  //   let uploadNeeded:boolean = false;

  //   //TODO: ako je moguće pronaći bolje rješenje od brojanja atributa
  //   this.numOfFileAttrsForUpload = this.collectionType.attributes.filter((attr) =>{ return attr.needsToUpload(); }).length;
  //   this.numOfFileAttrsUploaded = 0;

  //   this.collectionType.attributes.forEach((attr) => {

  //     if( attr.needsToUpload() ){
  //       this.uploadFiles(attr);
  //       uploadNeeded = true; 
  //     } 

  //   });

  //   return uploadNeeded;
  // }


  //TODO Move to collectionType
//   refreshMappings(attr:Attribute, newData:any): void{    

//     (<BasicType>attr.type).mapping.forEach((map) => {

//       if( ValidationUtils.emptyValue(newData) )
//         this.collectionType.getEditRow().data[map.to] = null;
// //        this.dataToEdit[map.to] = null;

//       else    
//         this.collectionType.getEditRow().data[map.to] = newData[map.from];
//        // this.dataToEdit[map.to] = newData[map.from];

//     });

//   }




/*
interface File{
  name:string;
  path:string;
  error:string;
  longError:string;
}
*/
/*
interface DataWithIndex{
  data:any;
  index:number;
}*/



/* UPLOAD
  reproduceBug() {
    const url = "url";

    let fileReader: FileReader = new FileReader();
    let body = new FormData();

    fileReader.addEventListener("load", $event => {
        body.append("file", fileReader.result);
        let options: RequestOptionsArgs = new RequestOptions();
        options.headers = new Headers();
        options.headers.append("Content-Type", "");

        return this.http.post(url, body, options)
            .toPromise();
    });

    // async
    fileReader.readAsBinaryString(this.file);
}

*/




/*
checkMandatory(attr:Attribute): boolean{
  if(attr.mandatory && ValidationUtils.emptyValue(this.dataToEdit[attr.name])){
    attr.setError(Messages.FIELD_MANDATORY, ErrorType.WARNING);
    return false;
  }
  return true;
}*/

/*
checkLength(attr:Attribute): boolean{
  if(!ValidationUtils.emptyValue(this.dataToEdit[attr.name]) && 
    (this.dataToEdit[attr.name].toString().length > attr.validationProperties.length.max ||
      this.dataToEdit[attr.name].toString().length < attr.validationProperties.length.min)){
      
      attr.setError(StringUtils.format(Messages.FIELD_LENGTH, 
        [attr.validationProperties.length.max.toString(), 
          attr.validationProperties.length.min.toString()]));
      return false;
  }

  return true;
}*/

/*fillKeys(keys:Key[], data:any): void{
  keys.forEach((key) => { key.value = data[key.name]; });
}*/


/*static emptyValue(val:any): boolean{
  return val == null || val == undefined || val == [] || val.toString() == '';
}

static stripValue(val:any): any{
  if(val == null) return null;
  val = val.toString().trim();
  val = val.toString().replace(/ +/g, ' ');

  return val;
}
*/



/*

checkUnique(attr:Attribute): boolean{
  
    if(!attr.validationProperties.unique) return;
    
    if(ValidationUtils.emptyValue(attr.validationProperties.uniqueKeys))
      throw new Error("Attribute cannot be declared unique without setting unique keys!");

    //ValidationUtils.fillKeys(attr.validationProperties.uniqueKeys, this.dataToEdit);
    attr.fillKeys(this.dataToEdit);
   
    let exsisting = this.findByKeys(this.dataset, attr.validationProperties.uniqueKeys);

    if(exsisting != null && this.dataToEditStatus == DATA_STATUS.NEW ||
       exsisting != null && exsisting.index != this.dataIndex){
      
      attr.setError(Messages.FIELD_UNIQUE);
      return false;
    }     
    return true;
  }


*/


/*dataValid(): boolean{
  return ValidationUtils.isDataValid(this.attributes);
 /* let valid = true;

  this.attributes.forEach((attr) => {
    
    if(attr.error.type != ErrorType.NONE &&
       attr.error.type != ErrorType.INFORMATION &&
       attr.error.type != ErrorType.SUCCESS){
         valid = false;
         return;
    }
    
  });

  return valid;/
}*/

/*

 validateAttribute(attr:Attribute): void{

    attr.validate(this.dataToEdit[attr.name], this.dataset, this.attributes);

    //attr.setError('', ErrorType.SUCCESS); //Remove errors before validating

    //this.dataToEdit[attr.name] = StringUtils.strip(this.dataToEdit[attr.name]);    

    //If everything ok => emit validateAttribute

    //let val = this.dataToEdit[attr.name];

    //TODO: validate by component (INPUT, FILE, LOV)
    /
    if(attr.checkMandatory(val) && attr.checkLength(val) && this.checkUnique(attr)){
      this.onValidateAttribute.emit(attr);
    }
    /  
      
  }
*/

/*  checkUnique(attr:Attribute): boolean{
  
    if(!attr.validation.unique) return;
    
    if(ValidationUtils.emptyValue(attr.validation.uniqueKeys))
      throw new Error("Attribute cannot be declared unique without setting unique keys!");

    //ValidationUtils.fillKeys(attr.validationProperties.uniqueKeys, this.dataToEdit);
    attr.fillKeys(this.dataToEdit);
   
    let exsisting = this.findByKeys(this.dataset, attr.validation.uniqueKeys);

    if(exsisting != null && this.dataToEditStatus == DATA_STATUS.NEW ||
       exsisting != null && exsisting.index != this.dataIndex){
      
      attr.setError(Messages.FIELD_UNIQUE);
      return false;
    }     
    return true;
  }
*/
/*
  findByKeys(dataset:any[], keys:Key[]): DataWithIndex{

    let ok:boolean = true;

    for (var d = 0; d < dataset.length; d++) {

      let data = dataset[d];
      ok = true;

      for (var k = 0; k < keys.length; k++) {
        
        let key = keys[k];

        let dataEmpty:boolean = ValidationUtils.emptyValue(data[key.name]);
        let keyEmpty:boolean = ValidationUtils.emptyValue(key.value);

        if(!dataEmpty && !keyEmpty && 
          data[key.name].toString().toLowerCase() != key.value.toString().toLowerCase() ||
          dataEmpty && !keyEmpty || !dataEmpty && keyEmpty){
          
          ok = false;
          break;
        }

      }

      if(ok) return {data:data, index:d};
    }

    return null;
  }
*/

  /*getKeyAttribute(): Attribute{
    let attr:Attribute = this.attributes.find((attr) => attr.key );
    
    if(attr == null) throw new Error("One attribute must be a key value.");
    return attr;
  }
  */

  /*
  static filesHaveError(data:any, fileNames:string[]): boolean{

   /   for (let i = 0; i < data.length; i++) {

        if( !DataService.emptyValue(data[i].error) ){
          console.log(data[i]);
          //TODO: Show message about error....in error of attibute?
          return true;
        }        
      
      }
   /
      for (let i = 0; i < fileNames.length; i++) {
      
        if( !ValidationUtils.emptyValue((<File>data[fileNames[i]]).error) ){

          console.log((<File>data[fileNames[i]]).error);

          return true;
          
        }

      }

    return false;
  }
  */








/* UPLOAD
if( !(<FileType>attr.type).checkForErrors(res, ffd.fileNames) ){ 

  if( !(<FileType>attr.type).multiple ){//Only one file

    let file:FileResponse = (<FileResponse>res[ffd.fileNames[0]]);

    this.dataToEdit[attr.name] = file.path + '/' + file.name;


  }else{ //List of files


  }

    console.log("_____________UPLOAD_OK__________");

    this.saveAndClose();

}else{


  if( !(<FileType>attr.type).multiple ){//Only one file
    
    attr.setError((<FileResponse>res[ffd.fileNames[0]]).error, ErrorType.ERROR);
    
    
  }else{ //List of files


  }

  console.log("_____________UPLOAD_ERROR__________");
}*/