import { Injectable } from '@angular/core';
import Web3 from 'web3';
import { IOrder, AttributesBuilder } from '../shared/common/AttributesBuilder';
import { MatSnackBar } from '@angular/material';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EthService {

  private web3:Web3 = null;
  private contract = null;

  private _orderInProcessExistFnc:Function = null;
  private _orderInProcess:IOrder = null;

  private _rewardableCountFnc:Function = null;

  constructor(private snackBar: MatSnackBar) { 
    this._initIfNeeded();
    // console.log(this);
    

    // this.web3.eth.enable();
    // setTimeout(() => {
      // this.finalizeOrder({id:1,companyId:1,date:"",deliveryTime:"",orderItems:[], rate:0,remark:"",status:"D",userId:1, paymentMethod:AttributesBuilder.PAYMENT_METHODS.ETH});
      
    // }, 1000);
  }


  private _init(): void{
    // this.web3 = new Web3(Web3.givenProvider);

    // console.log((<any>window).web3.currentProvider);    

    // this.web3 = new Web3((<any>window).ethereum);
    this.web3 = new Web3((<any>window).web3.currentProvider);
   
    this.web3.eth.defaultAccount = this.web3.eth.accounts[0];

    this.web3.currentProvider.enable();

    (<any>window).web3 = this.web3;
    (<any>window).es = this;
    setTimeout(() => { (<any>window).contract = this._getContract(); }, 1000);
    setTimeout('l=function(e,r){console.log(e,r)}', 0);
  }

  private _initIfNeeded(): void{
    if(this.web3 == null)
      this._init();
  }

  public isConnected(): boolean{
    if(this.web3.currentProvider.isConnected())
      return true;
    
    this.snackBar.open("Niste prijavljeni na Ethereum mrežu.", null, {duration: 15000 }); //15s 

    setTimeout(() => { this.web3.currentProvider.enable(); }, 0);

    return false;
  }


  public finalizeOrder(order:IOrder): void{
    
    this._orderInProcess = order;

    if(!(<any>this._orderInProcess).completed)
      this._getContract().completeOrder(order.id, this._completeOrderAsyncFnc.bind(this));

    // this.web3.eth.getBalance(this.web3.eth.defaultAccount, this._completeOrderAsyncFnc.bind(this))
  }


  private _completeOrderAsyncFnc(err, res): void{
    this._log(err, res, true);  
    (<any>this._orderInProcess).completed = true;

    this._orderInProcess = null;
  }


  public orderExist(order:IOrder, fnc:Function): void{
    this._orderInProcess = order;
    this._orderInProcessExistFnc = fnc;
    this._getContract().hasOrder(order.id, this.web3.eth.defaultAccount, this._orderExistAsyncFnc.bind(this));

  }
  
  
  private _orderExistAsyncFnc(err, res): void{
    this._log(err, res, true);

    if(!res){
      this.snackBar.open("Narudžba još nije dodana u blok.", null, {duration: 5000 }); //5s

    }else if(res){
      this._orderInProcess.isOnBlockchain = true;
      // this._orderInProcessExistFnc();  
      this._isRewarded();
    }

    // this._orderInProcessExistFnc = null;
  }

  private _isRewarded(): void{
    this._getContract().isOrderRewarded(this._orderInProcess.id, this._isRewardedAsyncFnc.bind(this));
  }

  private _isRewardedAsyncFnc(err, res): void{
    this._log(err, res, true);

    if(err){
      // alert(err);

    }else{
      
      this._orderInProcess.rewarded = res;
      this._orderInProcessExistFnc();
    }

    this._orderInProcess = null;
    this._orderInProcessExistFnc = null;
  }


  public getRewardableCount(fnc:Function): void{
    this._rewardableCountFnc = fnc;
    this._getContract().getRewardableCount(this._rewardableCountAsyncFnc.bind(this));
  }

  private _rewardableCountAsyncFnc(err, res): void{
    this._log(err, res, true);

    if(!err){

      if(this._rewardableCountFnc != null){
        this._rewardableCountFnc(res == null? 0 : res.toNumber());
        this._rewardableCountFnc = null;

      }

    }

  }

  public setRewardableCount(c:number): void{
    this._getContract().setRewardableCount(c, this._rewardableCountAsyncFnc.bind(this));
  }

  private _getContract(){

    if(this.contract == null)
      this.contract = this.web3.eth.contract(EthService.ABI).at(EthService.CONTRACT_ADDRESS);

    return this.contract;
  }


  private _log(err, res, alertError:boolean = false){
    
    if(!environment.production){
      console.log(err, res);
    }
   
    if(err && alertError){
      this.snackBar.open("Došlo je do greške u komunikaciji sa Blockchain-om. ("+err+")", null, {duration: 10000 }); //10s 
    }
  }

  private static CONTRACT_ADDRESS = "0x8a4d4404a3bc409e10a7dd7763287cc38857e0f0";
  private static ABI = [
    {
      "constant": false,
      "inputs": [
        {
          "name": "_id",
          "type": "uint32"
        },
        {
          "name": "_company",
          "type": "address"
        }
      ],
      "name": "cancelOrder",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [
        {
          "name": "_id",
          "type": "uint32"
        },
        {
          "name": "_company",
          "type": "address"
        }
      ],
      "name": "hasOrder",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "count",
          "type": "int8"
        }
      ],
      "name": "setRewardableCount",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [
        {
          "name": "_id",
          "type": "uint32"
        }
      ],
      "name": "isOrderRewarded",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_amount",
          "type": "uint256"
        }
      ],
      "name": "widraw",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [],
      "name": "getRewardableCount",
      "outputs": [
        {
          "name": "",
          "type": "int8"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [],
      "name": "die",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [],
      "name": "currentSupply",
      "outputs": [
        {
          "name": "",
          "type": "uint256"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_id",
          "type": "uint32"
        }
      ],
      "name": "completeOrder",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_id",
          "type": "uint32"
        },
        {
          "name": "_to",
          "type": "address"
        }
      ],
      "name": "placeOrder",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": true,
      "stateMutability": "payable",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [],
      "name": "getOwner",
      "outputs": [
        {
          "name": "",
          "type": "address"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [
        {
          "name": "_from",
          "type": "address"
        }
      ],
      "name": "getRewardableCount",
      "outputs": [
        {
          "name": "",
          "type": "int8"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [
        {
          "name": "_id",
          "type": "uint32"
        },
        {
          "name": "_company",
          "type": "address"
        }
      ],
      "name": "isOrderRewarded",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [
        {
          "name": "_id",
          "type": "uint32"
        }
      ],
      "name": "hasOrder",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    },
    {
      "payable": true,
      "stateMutability": "payable",
      "type": "fallback"
    }
  ];
}
