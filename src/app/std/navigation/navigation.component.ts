import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/UserService';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.sass']
})
export class NavigationComponent implements OnInit {

  constructor(private userService:UserService) { }

  private _isLogedIn = false;

  ngOnInit() {
  }

  isLogedIn(): boolean{

    if(this._isLogedIn != this.userService.isAuthenticated()){
      setTimeout(() => { this._isLogedIn = this.userService.isAuthenticated(); }, 0);
    }

    return this._isLogedIn;
  }

}
