import { Attribute } from './shared/common/Attribute';
import { InputType } from './shared/types/InputType';
import { DataType, DisplayType } from './shared/common/StaticValues';
import { FileType, FILE_ACCEPT_GROUPS } from './shared/types/FileType';
import { ListOfValuesType } from './shared/types/ListOfValuesType';
import { Mapping } from './shared/common/Mapping';
import { ListOfValuesDisplay } from './shared/display/ListOfValuesDisplay';
import { BaseDisplay } from './shared/display/BaseDisplay';

export class User{

    public id:string;
    public name:string;
    public surname:string;
    
    public email:string;
    
    public password:string;

    public address:string;
    public city:string;
    public latitude:number;
    public longitude:number;
    public showNumber:number;
    public phone:string;


    constructor(){
        // this.id = "1";
        // this.name = "Romeo";
        // this.surname = "Šajina";
        // this.email = "romeo.sajina@gmail.com";
        // this.photo = null;
        // this.role = "A";
        // this.password = "r";
     }

    // public isAdmin(): boolean{
    //     return this.role == "A";
    // }

    public setParams(u:User): void{
        this.id = u.id;
        this.name = u.name;
        this.surname = u.surname;
        this.email = u.email;
        this.password = u.password;
        this.address = u.address;
        this.city = u.city;
        this.longitude = u.longitude;
        this.latitude = u.latitude;
        this.showNumber = u.showNumber;
        this.phone = u.phone;
    }


    public static buildStandardAttributeList(): Attribute[]{
                  
        let Id:Attribute = new Attribute();
        Id.header = 'Id';
        Id.name = 'id';
        Id.visible = false;
        Id.updatable = false;
        Id.filter = '';
        Id.defaultValue = '-1';

        let Name:Attribute = new Attribute();
        Name.header = 'Ime';
        Name.name = 'name';
        Name.visible = true;
        Name.updatable = true;
        Name.filter = '';
        Name.defaultValue = '';
        Name.type = new InputType();

        let Surname:Attribute = new Attribute();
        Surname.header = 'Prezime';
        Surname.name = 'surname';
        Surname.visible = true;
        Surname.updatable = true;
        Surname.filter = '';
        Surname.defaultValue = '';
        Surname.type = new InputType();

        let Email:Attribute = new Attribute();
        Email.header = 'Email';
        Email.name = 'email';
        Email.visible = true;
        Email.updatable = true;
        Email.filter = '';
        Email.defaultValue = '';
        Email.type = new InputType(DataType.EMAIL);

        let Password:Attribute = new Attribute();
        Password.header = 'Lozinka';
        Password.name = 'password';
        Password.visible = true;
        Password.updatable = true;
        Password.filter = '';
        Password.defaultValue = '';
        Password.type = new InputType(DataType.PASSWORD);

        let Photo:Attribute = new Attribute();
        Photo.header = 'Slika';
        Photo.name = 'photo';
        Photo.visible = true;
        Photo.updatable = true;
        Photo.filter = '';
        Photo.defaultValue = '';
        Photo.type = new FileType("users", FILE_ACCEPT_GROUPS.IMAGES, "10MB");
        Photo.type.mandatory = false;
        Photo.displayType = new BaseDisplay(DisplayType.IMAGE);

        let Role:Attribute = new Attribute();
        Role.header = 'Uloga';
        Role.name = 'role';
        Role.visible = false;
        Role.updatable = false;
        Role.filter = '';
        Role.defaultValue = '';
       
        let RoleType = new ListOfValuesType();
        RoleType.staticData = true;
        RoleType.visibleAttribute = 'desc';
        RoleType.mapping = [new Mapping('value', 'role')];
        RoleType.data = [
            {value:'A', desc:'Admin'},
            {value:'S', desc:'Standard'}
        ];
        Role.type = RoleType;

        Role.displayType = new ListOfValuesDisplay(RoleType);
      

        return [Id, Name, Surname, Email,Password, Photo, User.buildDomainId(false), Role];
    }


    public static buildDomainId(editable:boolean = true): Attribute{
        
        let DomainId:Attribute = new Attribute();
        DomainId.header = 'Fakultet';
        DomainId.name = 'domainId';
        DomainId.visible = editable;
        DomainId.updatable = editable;
        DomainId.filter = '';
        DomainId.defaultValue = '1';
 
        let DomainIdType = new ListOfValuesType();
        DomainIdType.staticData = true;
        DomainIdType.visibleAttribute = 'name';
 
        DomainIdType.mapping = [new Mapping('id', 'domainId')];
        
        DomainId.type = DomainIdType;

        return DomainId;
    }


}