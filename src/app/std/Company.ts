import { User } from "./User";

export class Company extends User{

    photo:string;
    phone:string;
    delivery:boolean;
    pickUp:boolean;
    minDeliveryPrice:string;
    maxDestinationRange:string;
    maxFreeDeliveryRange:string;
    ethAddress:string;
    description:string;

    rewardableCount:number;

    //TODO: implement other stuff


    public setParams(u): void{
        super.setParams(u);
        this.photo = u.photo;
        this.delivery = u.delivery;
        this.pickUp = u.pickUp;
        this.minDeliveryPrice = u.minDeliveryPrice;
        this.maxDestinationRange = u.maxDestinationRange;
        this.maxFreeDeliveryRange = u.maxFreeDeliveryRange;
        this.ethAddress = u.ethAddress;
        this.description = u.description;
    }

}