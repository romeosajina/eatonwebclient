import { ErrorType } from './StaticValues';

export class ValidationError extends Error implements IValidationError {

    public type: string;
    constructor(type?: string, message?: string) {

        super(message);
        this.type = type;

        Object.setPrototypeOf(this, new.target.prototype); // restore prototype chain
    }


    public static getClearError(): IValidationError {
        return { message: '', type: ErrorType.NONE };
    }

    public static getClassForErrorType(error:IValidationError): string {

        switch (error.type) {

            case ErrorType.ERROR: return "danger";
            case ErrorType.WARNING: return "warning";
            case ErrorType.INFORMATION: return "information"; //TODO: Implement this class
            case ErrorType.SUCCESS: return "success";
            case ErrorType.NONE: return "";

        }

        return "";
    }


    public static getMKClassForErrorType(error:IValidationError): string{
        switch(error.type){
            case ErrorType.SUCCESS: return 'done';
            case ErrorType.WARNING: return 'warning';
            case ErrorType.ERROR: return 'error';
            case ErrorType.INFORMATION: return "information"; //TODO: Implement this class
            // case ErrorType.ERROR: return 'clear';
            default: return '';      
          }
    }

}

export interface IValidationError {
    message: string;
    type: string;
}

