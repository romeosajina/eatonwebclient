import { BaseType } from '../types/BaseType';
import { BaseDisplay } from '../display/BaseDisplay';
import { ListOfValuesType } from '../types/ListOfValuesType';
import { FileType } from '../types/FileType';
import { ErrorType, DataType, Messages } from './StaticValues';

import { Utils } from './Utils';

import { ValidationError, IValidationError } from '../common/ValidationError';



export class Attribute implements BaseAttribute {
  header: string;
  name: string;

  type: BaseType;
  visible: boolean;

  displayType: BaseDisplay;

  updatable: boolean;
  filter: string;
  defaultValue: any;
  error: IValidationError;



  constructor(name: string = '',
    header: string = '',
    type: BaseType = new BaseType(DataType.TEXT),
    visible: boolean = true,
    displayType: BaseDisplay = null,
    updatable: boolean = true,
    filter: string = '',
    defaultValue: any = '',
    error: IValidationError = null,
  ) {

    this.name = name;
    this.header = header == ''? name : header;
    this.type = type;
    this.visible = visible;
    this.displayType = displayType != null? displayType : new BaseDisplay();
    this.updatable = updatable;
    this.filter = filter;
    this.defaultValue = defaultValue;
    // this.error = error != null ? error : Attribute.getClearError();
    this.error = error != null ? error : ValidationError.getClearError();

    if(name.toLowerCase().indexOf('id') > -1 && defaultValue == '') this.defaultValue = -1

  }

  setError(message: string, type: string = ErrorType.ERROR): void {
    this.error.message = message;
    this.error.type = type;
  }

  getClassForErrorType(): string {
    return ValidationError.getClassForErrorType(this.error);
  }


  //TODO refactor this methods
  public needsListOfValues(): boolean {
    return this.isListOfValuesType() && this.type.needsListOfValues();
  }

  public isListOfValuesType(): boolean {
    return this.type instanceof ListOfValuesType; //TODO: Add other types of LOVS
  }


  public needsToUpload(): boolean {
    return this.isFileType() && (<FileType>this.type).needsToUpload();
  }

  public isFileType(): boolean {
    return this.type instanceof FileType;
  }


  clear(): void {
    this.setError('', ErrorType.NONE);
    this.type.clear();
  }


  public isValid(): boolean {
    return !ErrorType.isError(this.error.type) && this.type.isValid();
  }


  public processError(error: any) {
    console.log(this);

    console.log(error);

    if (error instanceof ValidationError)
      this.setError(error.message, error.type);

    else
      alert(error.message);

  }


}



export interface BaseAttribute {
  name: string;
  filter: string | null;
}
