import { Injectable, Provider } from '@angular/core';


@Injectable()
export class Icons {

    public static readonly SORT:string = 'fa fa-sort';
    public static readonly SORT_DESC:string = 'fa fa-sort-amount-desc';
    public static readonly SORT_ASC:string = 'fa fa-sort-amount-asc';
    public static readonly TRASH:string = 'fa fa-trash-o';
    public static readonly REMOVE:string = 'fa fa-times';
    public static readonly PENCIL:string = 'fa fa-pencil';
    public static readonly PLUS:string = 'fa fa-plus';
    public static readonly FILTER:string = 'fa fa-filter';
    public static readonly REFRESH:string = 'fa fa-refresh';
    public static readonly FILE_TEXT:string = 'fa fa-file-text';
    public static readonly STAR:string = 'fa fa-star';
    public static readonly LIKE:string = 'fa fa-thumbs-o-up';
    public static readonly DISLIKE:string = 'fa fa-thumbs-o-down';
    public static readonly FLAG:string = 'fa fa-flag';
    public static readonly REPLY:string = 'fa fa-reply'; 
    public static readonly CARET_DOWN:string = 'fa fa-caret-down'; 

    public static readonly SIGN_UP:string = 'fa fa-user-plus'; //user-plus
    public static readonly LOGIN:string = 'fa fa-sign-in';  

    

    public static readonly CHECK_CIRCLE:string = 'fa fa-check-circle';
    public static readonly X_CIRCLE:string = 'fa fa-times-circle';
    public static readonly INFO_CIRCLE:string = 'fa fa-info-circle';
    public static readonly WARNING_TRIANGLE:string = 'fa fa-exclamation-triangle';
    public static readonly QUESTION_CIRCLE:string = 'fa fa-question-circle';

    public static readonly QUESTIONS:string = 'fa fa-question-circle';
    public static readonly COMMENTS:string = 'fa fa-comments-o';
    public static readonly FILES:string = 'fa fa-file';
    public static readonly LINKS:string = 'fa fa-link';
    public static readonly PODCASTS:string = 'fa fa-podcast';


    public static readonly CHEVRON_LEFT:string = 'fa fa-chevron-left';
    public static readonly CHEVRON_RIGHT:string = 'fa fa-chevron-right';

    
}

@Injectable()
export class DataType {

    public static readonly TEXT = 'text';
    public static readonly TEXTAREA = 'textarea';
    public static readonly NUMBER = 'number';
    public static readonly URL = 'url';
    public static readonly PASSWORD = 'password';
    public static readonly EMAIL = 'email';
    public static readonly TEL = 'tel';
    public static readonly DATE = 'date';    
    public static readonly DATETIME = 'datetime-local';    
    public static readonly MONTH = 'month';    
    public static readonly WEEK = 'week';    
    public static readonly TIME = 'timr';    
    public static readonly COLOR = 'color';    
    
    public static readonly SELECT_ONE = 'select';
    public static readonly SELECT_MANY = 'select_many';
    public static readonly RICH_SELECT_ONE = 'rich-select';

    public static readonly CHECKBOX = 'checkbox';
    public static readonly RADIO = 'radio';
    public static readonly TOGGLE = 'toggle';

    public static readonly FILE = 'file'; 
   // public static readonly MULTIPLE_FILES = 'multiple-files';  
   
   public static readonly COLLECTION = 'collection'; 
   public static readonly EDITABLE_COLLECTION_SELECT = 'editable-collection-select'; 
   public static readonly EDITABLE_COLLECTION_INPUT = 'editable-collection-input'; 
   public static readonly FILE_COLLECTION = 'file-collection'; 
   
    
} 


@Injectable()
export class DisplayType {

   public static readonly TEXT = 'TEXT';
   public static readonly IMAGE = 'IMAGE';
   public static readonly BADGE = 'BADGE';
   public static readonly ICON = 'ICON';
   public static readonly LINK = 'LINK';

   public static readonly LIST_OF_VALUES = 'LOV';

   public static readonly LIST_OF_VALUES_BADGE = 'LOV_BADGE';

   public static readonly COLLECTION = 'COLLECTION'; 
    
   public static readonly TEMPLATE_REFERENCE = 'TEMPLATE_REFERENCE';
   
}   

@Injectable()
export class ErrorType {

   public static readonly ERROR = 'ERROR';
   public static readonly WARNING = 'WARNING';
   public static readonly INFORMATION = 'INFORMATION';
   public static readonly NONE = 'NONE';
   public static readonly SUCCESS = 'SUCCESS';


   public static isError(type:string): boolean{
   
    if(type != ErrorType.NONE &&
        type != ErrorType.INFORMATION &&
        type != ErrorType.SUCCESS)
        return true;

    return false;
    }
    
}   

@Injectable()
export class Messages {

   public static readonly FIELD_MANDATORY = 'Polje je obavezno.';//Field is required
   public static readonly COLLECTION_DATA_MANDATORY = 'Barem jedan podatak mora biti dodan.';
   public static readonly FIELD_UNIQUE = 'Polje mora biti jedinstveno.';//Field must be unique
   public static readonly FIELD_LENGTH = 'Dužina polja mora biti manja ili jednaka {0} i veća ili jednaka {1}.';//Field length must be less or equal to {0} and greater or equal to {1}
  
   public static readonly FILE_EXSTENSION = 'Ekstenzija datoteke nije dopuštena.';//File extension is not allowed
   public static readonly FILE_SIZE = 'Veličina datoteke prelazi granicu od {0}.';//File size exceeded the limit of {0}
   public static readonly MULTIPLE_FILE_UPLOAD_FAILED = 'Greška kod učitavanja datoteka, pogledajte poruku greške.';//Files upload failed, check error messages

   public static readonly ASK_DELETE_1 = 'Hmmm...Jeste li sigurni da želite ovo obrisati?';

   public static readonly SERVER_ERROR = 'Poslužitelj ne odgovara na zahtjev, moguće da je spušten.'; //'Server is not responding, may be shut down.'

   public static readonly ASK_DELETE = "Jeste li sigurni da želite izbrisati ovu stavku?";

   public static readonly SUCCESS = 'Uspjeh'; //Success
   public static readonly INFORMATION = 'Informacija'; //Information
   public static readonly CONFIRMATION = 'Potvrda'; //Confirmation
   public static readonly WARNING = 'Upozorenje'; //Warning
   public static readonly ERROR = 'Greška'; //Error


   public static readonly CANCEL = 'Odustani'; //Cancel
   public static readonly OK = 'OK';
//    public static readonly WHY_NOT = 'OK';
   
}   


export let CONSTANTS_PROVIDERS:Provider[] = [
    { provide: Icons, useClass: Icons },
    { provide: DataType, useClass: DataType },
    { provide: DisplayType, useClass: DisplayType },
    { provide: ErrorType, useClass: ErrorType },
    { provide: Messages, useClass: Messages }

  ];

  

//https://stackoverflow.com/questions/34404507/injection-versus-global-static-class-with-angular-2
//https://angular.io/guide/dependency-injection
