import { Attribute } from "./Attribute";
import { CollectionType } from "../types/CollectionType";
import { environment } from "../../../../environments/environment";
import { FileType } from "../types/FileType";
import { InputType } from "../types/InputType";
import { DataType } from "./StaticValues";


export class AttributesBuilder {


    public static ORDER_STATUS = {
        'O': 'fiber_new',
        'C': 'done',
        'P': 'loop',
        'F': 'done_all',
        'D': 'outlined_flag'
    }

    public static MENU_TYPE = {
        "M": "Meso",
        "V": "Vegan",
        "P": "Pizza",
        "B": "Burger",
        "G": "Grill"
    }

    public static PAYMENT_METHODS = {
        'ETH': 'E',
        'CASH': 'C'
    }

    public static getOrderStatusArray(): Array<any>{
        let ar = [];

        for(let key in AttributesBuilder.ORDER_STATUS)
            ar.push({key:key, value:AttributesBuilder.ORDER_STATUS[key]});            
        
        return ar;
      }

    public static buildOrdersCollection(): CollectionType{

        let collectionType = new CollectionType();
       
        collectionType.addSelfCollectionLink("orders", environment.API_URL+"orders");
       
        collectionType.attributes = AttributesBuilder.buildOrderAttributes();
    
        collectionType.init();

        return collectionType;
    }

    public static buildOrderAttributes(): Attribute[] {

        let id = new Attribute("id", "Broj narudžbe");
        // id.visible = false;

        let userId = new Attribute("userId");
        userId.visible = false;

        let companyId = new Attribute("companyId");
        companyId.visible = false;

        let date = new Attribute("date", "Datum narudžbe");

        let deliveryTime = new Attribute("deliveryTime", "Vrijeme dostave (cca)");

        let rate = new Attribute("rate");
        rate.visible = false;
        rate.type = new InputType(DataType.NUMBER)        
        

        let status = new Attribute("status", "Status");

        // INITIALIZED => Narudzba/rezervacija je tek kreirana - samo u mob/web aplikaciji
        // ORDERED     => Korisnik je potvrdia narudzbu, sad ceka da mu se jave iz company - commit na bazu/notifikacija company
        // CONFIRMED   => Company je potvrdila narudzbu
        // PROCESSING  => Narudzba se sada priprema
        // FINISHED    => Narudzba je obradena i spremna za dostavu - obavjesti korisnika da stize
        // DELIVERED   => Narudzba je dostavljena na adresu
        //'O' => 'ordered', 'C' => 'confirmed', 'P' => 'processing', 'F' => 'finished', 'D' => 'delivered'

        let remark = new Attribute("remark", "Napomena");
        remark.visible = false;
        remark.type.mandatory = false;

        //'E' => 'etherium', 'C' => 'cash'
        let paymentMethod = new Attribute("paymentMethod", "Metoda plaćanja");
        paymentMethod.visible = false;

        let orderItems = new Attribute("orderItems");

        // let total = new Attribute("total", "Ukupno (kn)");    

        orderItems.type = this.buildOrderItemsCollection();

        return [id, userId, companyId, date, deliveryTime, rate, status, remark, paymentMethod,/*total,*/ orderItems];
    }

    //TODO: NISU SVI ATRIBUTI UKLJUCENI U ARRAY
    public static buildOrderItemsCollection(): CollectionType {

        let ct: CollectionType = new CollectionType();

        ct.addSelfCollectionLink("orderItems", environment.API_URL + "orders/{id}/orderItems");

        let id = new Attribute("id");
        id.visible = false;

        let amount = new Attribute("amount", "Količina");
        amount.type = new InputType(DataType.NUMBER)        

        let discount = new Attribute("discount", "Popust");
        discount.visible = false;
        discount.type = new InputType(DataType.NUMBER)        

        let remark = new Attribute("remark", "Napomena");
        remark.visible = false;
        remark.type.mandatory = false;

        let menuItemId = new Attribute("menuItemId");
        menuItemId.visible = false;

        let menuItemOptionId = new Attribute("menuItemOptionId");
        menuItemOptionId.visible = false;

        let menuItemName = new Attribute("menuItemName");
        menuItemName.visible = false;
        
        let menuItemPhoto = new Attribute("menuItemPhoto");
        menuItemPhoto.visible = false;

        let menuItemPrice = new Attribute("menuItemPrice");
        menuItemPrice.visible = false;
        menuItemPrice.type = new InputType(DataType.NUMBER)        
        
        let menuItemDiscount = new Attribute("menuItemDiscount");
        menuItemDiscount.type = new InputType(DataType.NUMBER)        
        menuItemDiscount.visible = false;

        let menuItemDescription = new Attribute("menuItemDescription");
        menuItemDescription.visible = false;

        let menuItemOptionName = new Attribute("menuItemOptionName", "Naziv (opcija)");

        let menuItemOptionPrice = new Attribute("menuItemOptionPrice", "Cijena (kn)");
        menuItemOptionPrice.type = new InputType(DataType.NUMBER)        

        let total = new Attribute("total", "Ukupno (kn)");

        let orderItemSupplements = new Attribute("orderItemSupplements");
        orderItemSupplements.type = this.buildOrderItemSupplementsCollection();

        ct.attributes = [id, menuItemOptionName, menuItemOptionPrice, amount, discount, remark, menuItemId, menuItemOptionId, total, orderItemSupplements];

        return ct;
    }



    public static buildOrderItemSupplementsCollection(): CollectionType {

        let ct: CollectionType = new CollectionType();
        ct.addSelfCollectionLink("orderItemSupplements", environment.API_URL + "orders/{id}/orderItems/{id}/orderItemSupplements");

        let id = new Attribute("id");
        id.visible = false;

        let orderItemId = new Attribute("orderItemId");
        orderItemId.visible = false;

        let menuItemOptionSupplementId = new Attribute("menuItemOptionSupplementId");
        menuItemOptionSupplementId.visible = false;

        let menuItemOptionSupplementName = new Attribute("menuItemOptionSupplementName", "Naziv");

        let menuItemOptionSupplementPrice = new Attribute("menuItemOptionSupplementPrice", "Cijena (kn)");
        menuItemOptionSupplementPrice.type = new InputType(DataType.NUMBER)        


        ct.attributes = [id, orderItemId, menuItemOptionSupplementId, menuItemOptionSupplementName, menuItemOptionSupplementPrice];

        return ct;
    }


   public static buildCompaniesCollection(addMenusAttr:boolean = false): CollectionType {

        let ct: CollectionType = new CollectionType();
        ct.addSelfCollectionLink("companies", environment.API_URL + "companies");

        let id = new Attribute("id");
        // id.visible = false;

        let name = new Attribute("name", "Naziv");

        let city = new Attribute("city", "Grad");

        let address = new Attribute("address", "Adresa");

        let latitude = new Attribute("latitude", "");
        latitude.type = new InputType(DataType.NUMBER)                
        let longitude = new Attribute("longitude", "");
        longitude.type = new InputType(DataType.NUMBER)        

        let photo = new Attribute("photo", "Slika");
        photo.type = new FileType("images/companies");
        photo.type.mandatory = false;

        let phone = new Attribute("phone", "Broj tel.");
        phone.type.mandatory = false;

        let delivery = new Attribute("delivery", "Dostava");
        let pickUp = new Attribute("pickUp", "Pick up");
        
        let minDeliveryPrice = new Attribute("minDeliveryPrice", "Min. cijena dostave (kn)");
        minDeliveryPrice.type = new InputType(DataType.NUMBER)        
        
        let maxDestinationRange = new Attribute("maxDestinationRange", "Max. udaljenost dostave (km)");
        maxDestinationRange.type = new InputType(DataType.NUMBER)        
        let maxFreeDeliveryRange = new Attribute("maxFreeDeliveryRange", "Max. udaljenost besplatne dostave (km)");
        maxFreeDeliveryRange.type = new InputType(DataType.NUMBER)        

        let ethAddress = new Attribute("ethAddress", "Etherium adresa");
        ethAddress.type.mandatory = false;

        let description = new Attribute("description", "Opis");
        description.type.mandatory = false;

        ct.attributes = [id, name, city, address, latitude, longitude, photo, phone, delivery, pickUp, minDeliveryPrice, maxDestinationRange, maxFreeDeliveryRange, ethAddress, description];


        if(addMenusAttr){
            let menus = new Attribute("menus", "Meniji");
            menus.type = this.buildMenusCollection();
            
            ct.attributes.push(menus);
        }

        return ct;
    }

    public static buildMenusCollection(): CollectionType {

        let ct: CollectionType = new CollectionType();
        ct.addSelfCollectionLink("menus", environment.API_URL + "companies/{id}/menus");

        let id = new Attribute("id");
        id.visible = false;

        let companyId = new Attribute("companyId");
        companyId.visible = false;

        let type = new Attribute("type", "Tip");

        let photo = new Attribute("photo", "Slika");
        photo.type = new FileType("images/companies");
        photo.type.mandatory = false;

        let menuItems = new Attribute("menuItems", "Proizvodi");
        menuItems.type = this.buildMenuItemsCollection();
        menuItems.type.mandatory = false;

        ct.attributes = [id, companyId, type, photo, menuItems];

        return ct;
    }

    public static buildMenuItemsCollection(): CollectionType {

        let ct: CollectionType = new CollectionType();
        ct.addSelfCollectionLink("menuItems", environment.API_URL + "companies/{id}/menus/{id}/menuItems");

        let id = new Attribute("id");
        id.visible = false;

        let menuId = new Attribute("menuId");
        menuId.visible = false;

        let name = new Attribute("name", "Naziv");
        let price = new Attribute("price", "Cijena (kn)");
        price.type = new InputType(DataType.NUMBER)
        let discount = new Attribute("discount", "Popust (%)");
        discount.type = new InputType(DataType.NUMBER);
        discount.defaultValue = 0.0;
        
        let rate = new Attribute("rate", "Ocjena");
        rate.visible = false;
        rate.defaultValue = 0.0;

        let description = new Attribute("description", "Opis");

        let photo = new Attribute("photo", "Slika");
        photo.type = new FileType("images/companies");
        photo.type.mandatory = false;

        let menuItemOptions = new Attribute("menuItemOptions", "Opcije");
        menuItemOptions.type = this.buildMenuItemOptionsCollection();

        ct.attributes = [id, menuId, name, price, discount, description, rate, photo, menuItemOptions];

        return ct;
    }
    
    public static buildMenuItemOptionsCollection(): CollectionType {

        let ct: CollectionType = new CollectionType();
        ct.addSelfCollectionLink("menuItemOptions", environment.API_URL + "companies/{id}/menus/{id}/menuItems/{id}/menuItemOptions");

        let id = new Attribute("id");
        id.visible = false;

        let menuItemId = new Attribute("menuItemId");
        menuItemId.visible = false;

        let name = new Attribute("name", "Naziv");
        let price = new Attribute("price", "Cijena (kn)");
        price.type = new InputType(DataType.NUMBER)        
        
        let menuItemOptionSupplements = new Attribute("menuItemOptionSupplements", "Dodaci");
        menuItemOptionSupplements.type = this.buildMenuItemOptionSupplementsCollection();
        menuItemOptionSupplements.type.mandatory = false;

        ct.attributes = [id, menuItemId, name, price, menuItemOptionSupplements];

        return ct;
    }

    public static buildMenuItemOptionSupplementsCollection(): CollectionType {

        let ct: CollectionType = new CollectionType();
        ct.addSelfCollectionLink("menuItemOptionSupplements", environment.API_URL + "companies/{id}/menus/{id}/menuItems/{id}/menuItemOptions/{id}/menuItemOptionSupplements");

        let id = new Attribute("id");
        id.visible = false;

        let menuItemOptionId = new Attribute("menuItemOptionId");
        menuItemOptionId.visible = false;

        let name = new Attribute("name", "Naziv");
        let price = new Attribute("price", "Cijena (kn)");
        price.type = new InputType(DataType.NUMBER)        
        
        let photo = new Attribute("photo", "Slika");
        photo.type = new FileType("images/companies");
        photo.type.mandatory = false;

        ct.attributes = [id, menuItemOptionId, name, price, /*photo*/];

        return ct;
    }
    
}

export interface IOrder{
    id: number;
    userId: number;
    companyId: number;
    date: string;
    deliveryTime: string;
    rate: number;
    status: string;
    remark: string;
    paymentMethod: string;
    isOnBlockchain:boolean | false;
    rewarded:boolean | false;
    orderItems: Array<IOrderItem>;
}

export interface IOrderItem{
    amount: string;
    discount: string;
    id: number;
    menuItemDescription: string;
    menuItemDiscount: string;
    menuItemId: number;
    menuItemName: string;
    menuItemOptionId: number;
    menuItemOptionName: string;
    menuItemOptionPrice: string;
    menuItemPhoto: null
    menuItemPrice: string;
    orderItemSupplements: Array<any>
    remark: string;
  }
  