// import { Key } from  '../validation/ValidationProperties';
import { Attribute } from  '../common/Attribute';
import { ErrorType } from  '../common/StaticValues';
// import { CollectionType, IRow } from '../types/CollectionType';
 




// export class ValidationUtils{
  
    
//   public static emptyValue(val:any): boolean{
//     return val == null || val == undefined || val == [] || val.toString() == '';
//   }

//   public static isDataValid(attributes:Attribute[]): boolean{
//     let valid = true;

//     attributes.forEach((attr) => {
      
//       if( !attr.isValid() ){
//            valid = false;
//            return;
//       }
      
//     });

//     return valid;
//   }
  
// }


export class Utils{

    // public static getKeyAttribute(attributes:Attribute[]): Attribute{
         
    //     let attr:Attribute = attributes.find((attr) => attr.key );
        
    //     if(attr == null) throw new Error("One attribute must be a key value.");
    //     return attr;
    //   }

    public static emptyValue(val:any): boolean{
    return val == null || val == undefined || val == [] || val.toString() == '';
    }

    
    public static format(message:string, values:string[]): string{
        
        values.forEach((value, index) => {  
        message = message.replace('{'+index+'}', value);
        });

        return message;
    }


    public static strip(val:any): any{
        
        if(val == null) 
            return null;
        
        else if(val instanceof Array)
            return val;
        
        val = val.toString().trim();
        val = val.toString().replace(/ +/g, ' ');
    
        return val;
    }
    
 
    public static getFileName(url:string): string{
        //TODO: Check if separator is \ 
     
        if(!Utils.emptyValue(url))
           return url.substring(url.lastIndexOf('/') + 1);
        
        else
            return '';
    }


/*
    public static setFilters(collectionType:CollectionType, attrNames:string[], value:string): void{
        collectionType.attributes.filter(attr => attrNames.indexOf(attr.name) > -1).forEach(attr => attr.filter = value);
    }

    public static setChildFilters(collectionType:CollectionType, attrName:string, attrNames:string[], value:string): void{
        Utils.setFilters(<CollectionType>collectionType.attributes.find(attr => attr.name == attrName).type, attrNames, value);
    }


    public static filterCollectionType(collectionType:CollectionType): IRow[]{

        let ftr:IRow[] = [];
        let cln:IRow[] = collectionType.getCollection();
        
        for(let i=0; i<cln.length; i++){

            let valid = Utils.applysToContidion(collectionType, cln[i]);
            
            if(valid)
                ftr.push(cln[i]);

        }

        return ftr;
    }


    public static applysToContidion(collectionType:CollectionType, row:IRow): boolean{
        let valid = Utils.compareCoditionsToData(collectionType, row);
            
        if(valid)
            return true;
        
        let colTypes:Attribute[] = collectionType.attributes.filter(attr => attr.type instanceof CollectionType);

        //Provjeri da li u nekom CollectionType-u postoji redak sa odgovarajućom vrijedonsti
        for(let i=0; i<colTypes.length; i++){

            if(Utils.filterCollectionType(<CollectionType>colTypes[i].type).length > 0){
                valid = true;
                break;
            }

        }
        

        // collectionType.attributes.filter(attr => attr.type instanceof CollectionType).forEach(attr => {
            // valid = Utils.applysToContidion(<CollectionType>attr.type, row);
        // });

        return valid;
    }


    public static compareCoditionsToData(collectionType:CollectionType, row:IRow): boolean{
        let valid:boolean = true;    
    
    //    collectionType.attributes.forEach((attr) => {
            
    //         if( !(attr.type instanceof CollectionType) && 
    //             row.data[attr.name].toString().toLowerCase().indexOf(attr.filter.toLowerCase()) == -1){
    //           valid = false;
    //           return;
    //         } 
    
    //       });
       
        valid = false;

        // this.attributes.forEach((attr) => {
        collectionType.attributes.filter(attr => !(attr.type instanceof CollectionType)).forEach((attr) => {
            
            if(row.data[attr.name].toString().toLowerCase().indexOf(attr.filter.toLowerCase()) != -1){
                valid = true;
                return;
            } 

        });
        
        return valid;
    }
    */
    
}  


// export class StringUtils{



//     public static format(message:string, values:string[]): string{
        
//         values.forEach((value, index) => {  
//         message = message.replace('{'+index+'}', value);
//         });

//         return message;
//     }


//     public static strip(val:any): any{
        
//         if(val == null) return null;
        
//         val = val.toString().trim();
//         val = val.toString().replace(/ +/g, ' ');
    
//         return val;
//     }
    
 
//     public static getFileName(url:string): string{
//         //TODO: Check if separator is \ 
     
//         if(!ValidationUtils.emptyValue(url))
//            return url.substring(url.lastIndexOf('/') + 1);
        
//         else
//             return '';
//     }

// }