import { ValidationError } from '../common/ValidationError';
import { ErrorType, Messages } from '../common/StaticValues';
import { Utils } from '../common/Utils';

export class BaseType{
    type:string;

    mandatory:boolean;
    
    constructor(type:string = null,  mandatory:boolean = true){
      this.type = type;
      this.mandatory = mandatory;
    }
  

    public needsListOfValues(): boolean{
      return false;
    }


    public clear(): void{}

    public validate(data:any): void{
      this.checkMandatory(data);
    }


    public checkMandatory(data:any): void{
      data = Utils.strip(data);
      
      if(this.mandatory && Utils.emptyValue(data)){

        throw new ValidationError(ErrorType.WARNING, Messages.FIELD_MANDATORY);
      
      }
    
    }
    

    public isValid(): boolean{return true;}

  }
  
  // export interface TypeInterface{
  //   needsListOfValues(): boolean;
  //   clear(): void;
  //   validate(data:any): void;
  //   isValid(): boolean;
  // }


  /*
  
  export interface BaseType{
  type:string;
  needsListOfValues():boolean;
  clear():boolean;

}

  */