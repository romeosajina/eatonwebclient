import { CollectionType } from "./CollectionType";
import { DataType } from "../common/StaticValues";


export class EditableCollectionType extends CollectionType{

    public anyData:any[] = [];
    public anyList:any[] = [];

    constructor(type:string){
        super();
        this.type = type;
    }
}