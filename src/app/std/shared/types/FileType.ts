import { DataType } from '../common/StaticValues';
import { Mapping } from '../common/Mapping';
import { BaseType } from './BaseType';
import { Utils } from '../common/Utils';
// import { BaseAttribute } from '../common/Attribute';
import { ErrorType, Messages } from '../common/StaticValues';
import { ValidationError } from '../common/ValidationError';


export class FileType extends BaseType implements IFileType{

  public file: File;
  public destination: string;

  accept: string[];
  /** Must be expressed in GB, MB, KB (eg. 10MB)  */
  private maxSize: string;
  private _maxSize: number;

  // public uploadProgress:number = 0;

  constructor(destination: string = "files",
              accept: string[] = FILE_ACCEPT_GROUPS.ALL,
              maxSize: string = "10MB", ) {

    super(DataType.FILE);

    this.destination = destination;
    this.accept = accept;
    this.setMaxSize(maxSize);
    this.file = null;
  }

  public setMaxSize(maxSize: string): void {

    this.maxSize = maxSize;

    let sizeIn: string = maxSize.substr(maxSize.length - 2, maxSize.length - 1);

    this._maxSize = parseInt(maxSize.substr(0, maxSize.length - 2));

    switch (sizeIn) {
      case "GB": this._maxSize *= 1024;
      case "MB": this._maxSize *= 1024;
      case "KB": this._maxSize *= 1024; break;
      default: throw new Error("Unable to parse max file size.");
    }

  }

 
  public clear(): void { this.file = null; }

  public needsToUpload(): boolean {
    return this.file != null;
  }

  public isValid(): boolean { return true; }


  public checkSize(size: number): void {

    if (this._maxSize < size)
      throw new ValidationError(ErrorType.ERROR, Utils.format(Messages.FILE_SIZE, [this.maxSize]));

  }

  public checkExtension(name: string): void {

    let ext: string = name.split(".").pop();

    if (this.accept == FILE_ACCEPT_GROUPS.ALL)
      return;

    let valid: boolean = false;

    this.accept.forEach((ex) => {

      if (ex == ext) {
        valid = true;
        return;
      }

    });

    if (!valid)
      throw new ValidationError(ErrorType.ERROR, Messages.FILE_EXSTENSION)
  }

  public validate(data: any): void {
    super.validate(data);

    if (this.file != null) {

      this.checkExtension(this.file.name);
      this.checkSize(this.file.size);

    }

  }


}

export interface IFileType {
  destination: string;
  accept: string[];
  setMaxSize(maxSize: string): void;
  checkExtension(fileName: string);
  checkSize(size: number);
  // buildFormData(): FileFormData;
  // checkForErrors(files: FileResponse, fileNames: string[]): void;
}


/*
export interface FileFormData {
  formData: FormData;
  fileNames: string[];
}


export interface FileResponse {
  name: string;
  path: string;
  error: string;
  longError: string;
}*/



export const FILE_ACCEPT_GROUPS = {
  IMAGES: ["ase",
    "art",
    "bmp",
    "blp",
    "cd5",
    "cit",
    "cpt",
    "cr2",
    "cut",
    "dds",
    "dib",
    "djvu",
    "egt",
    "exif",
    "gif",
    "gpl",
    "grf",
    "icns",
    "ico",
    "iff",
    "jng",
    "jpeg",
    "jpg",
    "jfif",
    "jp2",
    "jps",
    "lbm",
    "max",
    "miff",
    "mng",
    "msp",
    "nitf",
    "ota",
    "pbm",
    "pc1",
    "pc2",
    "pc3",
    "pcf",
    "pcx",
    "pdn",
    "pgm",
    "PI1",
    "PI2",
    "PI3",
    "pict",
    "pct",
    "pnm",
    "pns",
    "ppm",
    "psb",
    "psd",
    "pdd",
    "psp",
    "px",
    "pxm",
    "pxr",
    "qfx",
    "raw",
    "rle",
    "sct",
    "sgi",
    "rgb",
    "int",
    "bw",
    "tga",
    "tiff",
    "tif",
    "vtf",
    "xbm",
    "xcf",
    "xpm",
    "3dv",
    "amf",
    "ai",
    "awg",
    "cgm",
    "cdr",
    "cmx",
    "dxf",
    "e2d",
    "egt",
    "eps",
    "fs",
    "gbr",
    "odg",
    "svg",
    "stl",
    "vrml",
    "x3d",
    "sxd",
    "v2d",
    "vnd",
    "wmf",
    "emf",
    "art",
    "xar",
    "png",
    "webp",
    "jxr",
    "hdp",
    "wdp",
    "cur",
    "ecw",
    "iff",
    "lbm",
    "liff",
    "nrrd",
    "pam",
    "pcx",
    "pgf",
    "sgi",
    "rgb",
    "rgba",
    "bw",
    "int",
    "inta",
    "sid",
    "ras",
    "sun",
    "tga"
  ],
  ARCHIVES: ["7z",
    "a",
    "apk",
    "ar",
    "cab",
    "cpio",
    "deb",
    "dmg",
    "egg",
    "epub",
    "gz",
    "iso",
    "jar",
    "mar",
    "pea",
    "rar",
    "s7z",
    "shar",
    "tar",
    "tbz2",
    "tgz",
    "tlz",
    "war",
    "whl",
    "xpi",
    "zip",
    "zipx",
    "zst"
  ],
  ALL: ["*"]
}
// https://github.com/arthurvr/image-extensions/blob/master/image-extensions.json
//https://github.com/sindresorhus/archive-extensions/blob/master/archive-extensions.json





/*

 public buildFormData(): FileFormData {

    let fn: string[] = [];
    const fd = new FormData();

    fn.push("file1");

    fd.append("file1", this.file);

    fd.append('destination', this.destination);

    fd.append('fileNames', JSON.stringify(fn));

    return { formData: fd, fileNames: fn };
  }


  public checkForErrors(files: FileResponse, fileNames: string[]): void {

    let error: string = files[fileNames[0]].error;

    if (!Utils.emptyValue(error))

      throw new ValidationError(ErrorType.ERROR, error);

  }

*/








/*
export interface CollectionAttribute{
  attribute:BaseAttribute;
  rel:FileAttributeRelation; 
}
*/
// export enum FileAttributeRelation{
//   KEY,
//   NAME,
//   PATH
// }


/*

I. Jedno polje, jedna datoteka => path se sprema u to polje
II. Više polja, jedna datoteka => path se sprema u jedno polje, ime u drugo polje
III. Više datoteka => path se sprema u jedno polje za svaki redak kolekcije
IV. Više datoteka => path se sprema u jedno polje, ime u drugo polje za svaki redak kolekcije

*/





 /*
  
    public buildFormData(): FileFormData{
        
        let fn:string[] = [];
        const fd = new FormData();
    
        for (let i = 0; i < this.files.length; i++) {
          
          fn.push("file"+[i]);
    
          fd.append("file"+[i], this.files[i]);
          
        }
    
        fd.append('destination', this.destination);
  
        fd.append('fileNames', JSON.stringify(fn));

      return {formData:fd, fileNames:fn};
    }
  
    public checkForErrors(files:FileResponse, fileNames:string[]): boolean{

      let valid:boolean = true;
    
      for (let i = 0; i < fileNames.length; i++) {
      
        let error:string = files[fileNames[i]].error;

        if( !Utils.emptyValue(error) ){

          this.errors[i] = {message:error, type:ErrorType.ERROR};

          valid = false;

          console.log(error);          
        }
      }
      return valid;
    }

  */





    /*
    private collectionHasErrors(): boolean{
      let valid = true;

      this.errors.forEach(error => {
      
       if(ErrorType.isError(error.type)){

         valid = false;
         return;

        }

      });

      return valid;
    }
  */