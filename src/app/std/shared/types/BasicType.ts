import { DataType } from '../common/StaticValues';
import { Mapping } from '../common/Mapping';
import { BaseType } from './BaseType';

export class BasicType extends BaseType{

  mapping: Mapping[];

  //public DataType:DataType = new DataType();  

  constructor(type: string = null, mapping: Mapping[] = []) {

    super();
    this.type = type != null ? type : DataType.TEXT;
    this.mapping = mapping;
  }

  public clear(): void { }

}
