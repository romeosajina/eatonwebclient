import { CollectionType, IKey, ILink, IRow } from './CollectionType';
import { Attribute } from '../common/Attribute';
import { Mapping } from '../common/Mapping';
import { IFileType, FILE_ACCEPT_GROUPS } from './FileType';
import { Utils } from '../common/Utils';
import { ErrorType, Messages, DataType } from '../common/StaticValues';
import { IValidationError, ValidationError } from '../common/ValidationError';

export class FileCollectionType extends CollectionType implements IFileType {

    // files: File[];
    destination: string;

    accept: string[];

    /** Must be expressed in GB, MB, KB (eg. 10MB)  */
    private maxSize: string;
    private _maxSize: number;

    private _mapping: Mapping[];

    // private _errors: IValidationError[] = [];

    private _files: Array<IFileRow> = new Array<IFileRow>();//Reference of row and it's file
    
    constructor(attribute: Attribute = null,
        key: IKey = null,
        uniqueKeys: IKey[][] = [],
        mapping: Mapping[] = [],
        defaultLinks: ILink[] = [],
        links: ILink[] = [],
        destination:string = 'files') {

        super([attribute], key, uniqueKeys, defaultLinks, links);
        // super(attributes, key, uniqueKeys, defaultLinks, links);
        this.type = DataType.FILE_COLLECTION;

        this._mapping = mapping;
        
        this.destination = destination;
    }

    public setMaxSize(maxSize: string): void {

        this.maxSize = maxSize;

        let sizeIn: string = maxSize.substr(maxSize.length - 2, maxSize.length - 1);

        this._maxSize = parseInt(maxSize.substr(0, maxSize.length - 2));

        switch (sizeIn) {
            case "GB": this._maxSize *= 1024;
            case "MB": this._maxSize *= 1024;
            case "KB": this._maxSize *= 1024; break;
            default: throw new Error("Unable to parse max file size.");
        }

    }

    public isValid(): boolean {
        return super.isValid();
        // let valid = true;

        // this.attributes.forEach((attr) => {

        //     if (!attr.isValid()) {
        //         valid = false;
        //         return;
        //     }
        // });
        // return valid;
        // return valid ? this._errors.length == 0 : valid;
    }

    public validate(data: any/*, file: File = null*/): void {
        // super.validate(data); //Only mandatory

        /*if (file != null) {

            this.checkExtension(file.name);
            this.checkSize(file.size);

        }*/

    }

    public validateFile(file: File): void {
        this.checkExtension(file.name);
        this.checkSize(file.size);
    }

    public getUrlAttribute(): Attribute {
        return this.attributes[0];
    }

    public getFiles(): Array<IFileRow> {
        return this._files;
    }

    public needsFileUpload(): boolean {
        return this._files.length > 0 || super.needsFileUpload();
    }

    public addNewFileToCollection(file: File, collection: IRow[]): IRow {

        try {
            this.validateFile(file);

            let row: IRow = this.create();

            row.data[this.getUrlAttribute().name] = file.name;

            CollectionType.addRowToCollection(null/*Always add to begining*/, row, collection);

            this._files.push({ file: file, row: row });

            return row;

        } catch (error) {
            this.processError(error);
        }

        return null;
    }

    public clear(collectionType: CollectionType = this): void {
        super.clear(collectionType);

        this._files = new Array<IFileRow>();
    }

    public checkSize(size: number): void {

        if (this._maxSize < size)
            throw new ValidationError(ErrorType.ERROR, Utils.format(Messages.FILE_SIZE, [this.maxSize]));

    }

    public checkExtension(name: string): void {

        let ext: string = name.split(".").pop();

        if (this.accept == FILE_ACCEPT_GROUPS.ALL)
            return;

        let valid: boolean = false;

        this.accept.forEach((ex) => {

            if (ex == ext) {
                valid = true;
                return;
            }

        });

        if (!valid)
            throw new ValidationError(ErrorType.ERROR, Messages.FILE_EXSTENSION)
    }

    
}

export interface IFileRow {
    row: IRow,
    file: File
}

export const enum FILE_MAPPING {
    NAME,
    URL
}


//export interface IFileRow extends Row{file:File | null; }


/*
 //TODO dodati novi atribut Map<ime_datoteke, zamjensko_ime> ???
    public buildFormData(): FileFormData {

        let fn: string[] = [];
        const fd = new FormData();

        for (let i = 0; i < this.files.length; i++) {

            fn.push("file" + [i]);

            fd.append("file" + [i], this.files[i]);

        }

        fd.append('destination', this.destination);

        fd.append('fileNames', JSON.stringify(fn));

        return { formData: fd, fileNames: fn };
    }

    public checkForErrors(files: FileResponse, fileNames: string[]): void {

        this._errors = [];

        let valid: boolean = true;

        for (let i = 0; i < fileNames.length; i++) {

            let error: string = files[fileNames[i]].error;

            if (!Utils.emptyValue(error)) {

                //  this.errors[i] = {message:error, type:ErrorType.ERROR};
                this._errors.push({ message: error, type: ErrorType.ERROR });

                valid = false;

                console.log(error);
            }
        }

        if (!this.isValid())
            throw new ValidationError(ErrorType.ERROR, Messages.MULTIPLE_FILE_UPLOAD_FAILED);
        //   return valid;
    }

*/