import { EventEmitter } from '@angular/core';
import { BaseType } from './BaseType';
import { BasicType } from '../types/BasicType';
import { Attribute } from '../common/Attribute';
import { DataType } from '../common/StaticValues';
import { ErrorType, Messages } from '../common/StaticValues';
import { Utils } from '../common/Utils';

// import { ValidationError } from '../validation/BaseValidation';
// import { IValidationError } from '../validation/BaseValidation';


import { ValidationError, IValidationError } from '../common/ValidationError';
import { FileInput } from 'ngx-material-file-input';

/*
import { DataType } from '../common/StaticValues';
import { Mapping } from '../common/Mapping';
import { BaseAttribute } from '../common/Attribute';
import { IValidationError } from '../validation/BaseValidation';
*/

export class CollectionType extends BaseType {

  attributes: Attribute[];
  defaultLinks: ILink[];
  links: ILink[];

  private _collection: IRow[] = [];
  // private _editCollection: IRow[] = [];
  private _deletedRows: IRow[] = [];
  private _currentRow: IRow = null;

  private _editRow: IRow = null;

  private _parentCollectionType: CollectionType = null;

  private _key: IKey; //name of key column
  private _uniqueKeys: IKey[][]; //names of unique keys columns

  /**Dictates if collection work with only one or with multiple records */
  private _singleMode:boolean = false;

  /**Dictates if collection is posted in batch mode to server */
  private _inBatchMode:boolean = false;

  public error: IValidationError;

  public rowAddingPolicy:ROW_ADDING_POLICY = ROW_ADDING_POLICY.NORMAL;

  public onCollectionChanged: EventEmitter<void> = new EventEmitter<void>(false);
  public onSelectionChanged: EventEmitter<void> = new EventEmitter<void>(false);
  public onValidateRow: EventEmitter<IRow> = new EventEmitter<IRow>(false); //Async = false zbog provjere koja mora zahvatit i validaciju. SENDS ROW FOR ARGUMENT
  public onValidateAttribute: EventEmitter<Attribute> = new EventEmitter<Attribute>(true);

  public static readonly LINK_KEY_NAME: string = '{id}';

  constructor(attributes: Attribute[] = [],
    key: IKey = null,
    uniqueKeys: IKey[][] = [],
    defaultLinks: ILink[] = [],
    links: ILink[] = []) {

    super(DataType.COLLECTION);
    this.attributes = attributes;
    this.defaultLinks = defaultLinks;
    this.links = links;
    this._key = key;
    this._uniqueKeys = uniqueKeys;
    this.error = ValidationError.getClearError();

  }

  

  public addSelfCollectionLink(name:string, url:string): void{
    let link:ILink = this.links.find(l => l.rel == LINK_TYPE.SELF);
    
    if(link != null)
      link.href = url;

    else
      this.links.push({
        rel : LINK_TYPE.SELF,
        href : url,
        name : name,
        kind : LINK_TYPE.COLLECTION
      });
  }

  public getSelfCollectionLink(): ILink{
    return CollectionType.findLinkByRel(this.links, LINK_TYPE.SELF);
  }


  public init(): void{

    if(this._key == null)
      this.setKey({name:"id", value:null});

    this.attributes.forEach((attr) => {

      if(attr.type instanceof CollectionType){

          attr.type.setParentCollectionType(this);

          attr.type.init();
      }

    });

    // if(this._parentCollectionType == null)
    //   console.log(this);
  }

  public findCollectionType(name:string): CollectionType{
    return <CollectionType>this.attributes.find(attr => attr.name == name).type;
  }

  public findAttr(name:string): Attribute{
    return this.attributes.find(attr => attr.name == name);
  }

  public needsListOfValues(): boolean {
    return this.attributes.filter((attr) => { return attr.needsListOfValues(); }).length > 0;
  }


  /** Recursively (when CollectionType) clears attributes  */
  public clear(collectionType: CollectionType = this): void {
    // console.log("clear ____ called");

    collectionType.attributes.forEach((attr) => {

      if (attr.type instanceof CollectionType)
        (<CollectionType>attr.type).clear();

      else
        collectionType.clearAttributes();

    });

    //Moguci bug jer se clear poziva svugdje
    this._deletedRows = [];
    // this.setEditRow(null);
    this.clearError();
  }

  /**
   * @override
   */
  public checkMandatory(data:any): void{

    if(this.mandatory && this._collection.length == 0){

      throw new ValidationError(ErrorType.WARNING, Messages.COLLECTION_DATA_MANDATORY);
    
    }
  
  }
  

  public isValid(): boolean {

    let valid = true;

    this.attributes.forEach((attr) => {

      if (!attr.isValid()) {
        valid = false;
        return;
      }
    });
    
    return valid && !ErrorType.isError(this.error.type);
  }


  public setCollection(dataset: any[]): void {
    this._currentRow = null;
    this._editRow = null;
    this._collection = [];
    this.addToCollection(dataset);
  }

  public addToCollectionFromBeginning(dataset: any[]): void { this._addToCollection(dataset, true); }
  public addToCollection(dataset: any[]): void { this._addToCollection(dataset, false); }

  private _addToCollection(dataset: any[], addToBeginning:boolean): void {

    dataset.forEach((data) => {

      this.attributes.forEach((attr) => {

        if (attr.type instanceof CollectionType){          
          data[attr.name] = this._formSubCollection(attr.type, data[attr.name], attr.name);
        }

      });
      
      if(addToBeginning)
        this._collection.unshift({ data: data, state: ROW_STATE.QUERY });
      else
        this._collection.push({ data: data, state: ROW_STATE.QUERY });

    });

    this._syncParentCollection();

    this.onCollectionChanged.emit();
  }

  private _syncParentCollection(): void {

    if (this._parentCollectionType == null) return;

    let attr: Attribute = this._parentCollectionType.attributes.find((attr) => { return attr.type == this });

    if (attr != null)
      this._parentCollectionType._currentRow.data[attr.name] = this._collection;

  }

  public setRowsForCollection(rows: IRow[]): void {
    
    if( !(rows instanceof Array) && (rows != null))
      throw new Error("Invalid argument for 'rows' in setRowsForCollection() where expected Array but got: " + rows)

    this._currentRow = null;
    this._editRow = null;
    this._collection = rows;
    this.onCollectionChanged.emit();
  }

  private _formSubCollection(collectionType:CollectionType, data: any[], attrName:string = null): IRow[] {

    if(data == null){
      console.log("***********WARNING*********** : No data found during subcollection forming for attribute name: "+attrName);
      return null;
      // return [];      
    }

    let newArray: Array<IRow> = [];

    data.forEach((d) => { 
            
      collectionType.attributes.forEach(attr => {

        if(attr.type instanceof CollectionType)
          d[attr.name] = attr.type._formSubCollection(attr.type, d[attr.name], attr.name);

      });

      newArray.push({ state: ROW_STATE.QUERY, data: d }) 
    
    });

    return newArray;
  }

  /**
   * Creates and return Row but doesn't add it to collection
   */
  public create(): IRow {
    let newRow: IRow = { data: new Object(), state: ROW_STATE.NEW };
    // let newRow: IRow = CollectionType.copy({ data: Object.assign({}, new Object()), state: ROW_STATE.NEW });

    this.attributes.forEach((attr) => { 
      if(attr.type instanceof CollectionType)
        newRow.data[attr.name] = new Array();
      else
        newRow.data[attr.name] = attr.defaultValue; 
    });
    
    //Ovo je trebalo samo za ADF-REST 
    // newRow.data['links'] = this.defaultLinks;

    return newRow;
  }

  /**
   * Creates and set's new row for edit, but doesn't add it to collection
   */
  public createRowAndSetItForEdit(): void { this.setRowForEdit(this.create()); }

  
  private _addRowToCollection(row:IRow, sync:boolean): void{
    switch(this.rowAddingPolicy){
      
      case ROW_ADDING_POLICY.START: this._collection.unshift(row); break; 
      case ROW_ADDING_POLICY.NORMAL: 

        let lastIndex: number = this._collection.indexOf(this._currentRow);

        if (lastIndex < 0)
          this._collection.splice(0, 0, row);
    
        else
          this._collection.splice(lastIndex, 0, row);  
      
      break;
      case ROW_ADDING_POLICY.END: this._collection.push(row); break;
      default: throw new Error("Row adding policy undefined");
    }
    
    if(sync)
      this._syncParentCollection();

    this.onCollectionChanged.emit();
  }

  public addRowSilentlyToCollection(row: IRow): void {    
    this._addRowToCollection(row, false);
  }

  public addRowToCollection(row: IRow): void {
    this._addRowToCollection(row, true);
    /*
    switch(this.rowAddingPolicy){
      
      case ROW_ADDING_POLICY.START: this._collection.unshift(row); break; 
      case ROW_ADDING_POLICY.NORMAL: 

        let lastIndex: number = this._collection.indexOf(this._currentRow);

        if (lastIndex < 0)
          this._collection.splice(0, 0, row);
    
        else
          this._collection.splice(lastIndex, 0, row);  
      
      break;
      case ROW_ADDING_POLICY.END: this._collection.push(row); break;
      default: throw new Error("Row adding policy undefined");
    }
    
    this._syncParentCollection();
    this.onCollectionChanged.emit();
    */
  }


  public static addRowToCollection(currRow: IRow, newRow: IRow, collection: IRow[]): void {
    let lastIndex: number = collection.indexOf(currRow);

    if (lastIndex < 0)
      collection.splice(0, 0, newRow);

    else
      collection.splice(lastIndex, 0, newRow);

  }

  public replaceCurrentRowInCollection(row: IRow): void {
    let curIndx = this._collection.indexOf(this._currentRow);

    if (curIndx < 0)
      throw new Error("Cannot call replaceCurrentRowInCollection when current row doesn't exsist in collection")

    this._collection[curIndx] = row;

    this._syncParentCollection();
    this.onCollectionChanged.emit();
  }

  //TODO: ovo ne dela najbolje (collectiontype šteka)
  public replaceRowWithKeyInCollection(row: IRow): void {
    let index: number = this._collection.findIndex((r: IRow) => { return r.data[this.getKey().name] == row.data[this.getKey().name] });

    if (index < 0)
      throw new Error("Unable to find row in collection for replace");

    this._collection[index] = row;
    // let rowToRpl:IRow = this._collection[index];   
    
    // this.attributes.forEach(attr => {

    //   rowToRpl.data[attr.name] = row.data[attr.name];
   
    // });
    

    this._syncParentCollection();
    this.onCollectionChanged.emit();
  }

  /** Removes row from collection and returns next row that can be selected (uses CollectionType.removeRowFromCollection())*/
  /*public removeRowFromCollection(row: IRow) : Row{

    let nextRow:Row = null;

    if (row == this.getCurrentRow())
      nextRow = this.removeCurrentRowFromCollection();//Not the best idea...set's currentRow
      // throw new Error("You must use removeCurrentRowFromCollection method when removing currentRow");

    //this._collection.splice(this._collection.indexOf(row), 1);
    else
       nextRow = CollectionType.removeRowFromCollection(row, this._collection);

    this._syncParentCollection();//?????
    this.onCollectionChanged.emit();

    return nextRow;
  }*/


  public removeCurrentRowFromCollection(): IRow {
    /*
    if(this.getCurrentRow() == null) return;

    let curIndx: number = this._collection.indexOf(this.getCurrentRow());

    if (curIndx > 1)
      this.setCurrentRow(this._collection[curIndx - 1]); //Set to previous row

    else if (curIndx > 0 && this._collection.length > curIndx + 1)
      this.setCurrentRow(this._collection[curIndx + 1]); //Set to next row

    else
      this.setCurrentRow(null);

    this._collection.splice(curIndx, 1);
    */
    this.setCurrentRow(CollectionType.removeRowFromCollection(this._currentRow, this._collection));
    this._syncParentCollection();
    this.onCollectionChanged.emit();

    return this._currentRow;
  }

  /**Removes row from collection and add it to deletedRows (uses CollectionType.removeRowFromCollection())*/
  public deleteRowFromCollection(row: IRow/*, collection: IRow[] = this.getCollection()*/): IRow {//TODO zaš se prosljeđuje collection kao param?????

    if (row == null) return null;

    if (row.state != ROW_STATE.NEW) {//New rows are not saved to DB yet

      row.state = ROW_STATE.DELETED;
      this._deletedRows.push(row);

    }

    let nextRow: IRow = CollectionType.removeRowFromCollection(row, this.getCollection());

    this.onCollectionChanged.emit();

    return nextRow;
  }


  public static removeRowFromCollection(row: IRow, collection: IRow[]): IRow {
    if (row == null) return null;

    let curIndx: number = collection.indexOf(row);
    let nextRow: IRow = null;

    if (curIndx > 0)
      nextRow = collection[curIndx - 1]; //Set to previous row

    else if (curIndx >= 0 && (collection.length > curIndx + 1))
      nextRow = collection[curIndx + 1]; //Set to next row

    collection.splice(curIndx, 1);

    return nextRow;
  }

  public removeEditRowFromCollection() {

    this.fillKey(this._editRow.data[this._key.name]);

    let row: IRow = this.findByKey();

    if (row == null) throw new Error("Unable to find row by key when removing from collection");

    if (this._currentRow != null && row.data[this._key.name] == this._currentRow.data[this._key.name])
      this.removeCurrentRowFromCollection();

    else
      this._collection.splice(this._collection.indexOf(row), 1);

    this.clearKey();

    this._syncParentCollection();
    this.onCollectionChanged.emit();
  }

  public fillKey(value: any): void { this._key.value = value; }
  public clearKey(): void { this._key.value = null; }

  public setUniqueKeys(keys: IKey[][]): void { this._uniqueKeys = keys; }

  public getKey(): IKey { return this._key; }
  public setKey(key: IKey): void { this._key = key; }

  public setCurrentRow(row: IRow) {

    if (row == this._currentRow) return;

    this._currentRow = row;
    this._setCollectionTypeAttributesForRow(row);
    this.onSelectionChanged.emit();
    /* this.selectionChanedFunction();*/
  }

  public setFirstRowAsCurrent(): void{
    this.setCurrentRow(this.getCollection().length > 0 ? this.getCollection()[0] : null);
  }

  public getCurrentRow(): IRow { return this._currentRow; }

  public setEditRowForCurrentAndClear(): void {
    this.setCurrentRow(this.getEditRow());
    this.clear();
  }

  public setCurrentRowForEdit(): void {
    this.setRowForEdit(CollectionType.copy(this._currentRow));
  }

  //TODO: set underlaying collectionTypes.....TESTIRAT
  public setEditRow(row: IRow): void {
    this._editRow = row;    
    this._setCTypeAttrsForEditRow(row);
  }

  //TODO: set underlaying collectionTypes.....TESTIRAT
  public setRowForEdit(row: IRow): void {
    this._editRow = row;
    if (this._editRow.state == ROW_STATE.QUERY) this._editRow.state = ROW_STATE.CHANGED;
    
    this._setCTypeAttrsForEditRow(row);

    // this._editRow.state = row.state == ROW_STATE.QUERY ? ROW_STATE.CHANGED : row.state; // Status is CHANGED immediate or dont change if NEW      


    // this._setCollectionTypeAttributesForRow(row);
    // this._copyAndSetCollectionTypeAttributesForRow(row);
    /*
        this.attributes.filter((attr) => { return attr.type instanceof CollectionType }).forEach((ct) => {

          (<CollectionType>ct.type)._collection = CollectionType.copyRows((<CollectionType>ct.type)._collection);

        });
    */
  }

  public getEditRow(): IRow { return this._editRow; }
  
  public setDataForEditRow(data: any, state: ROW_STATE): void {
    // this._editRow.data = data; this._editRow.state = state; 
    this._editRow.state = state; 
    
    this.attributes.forEach(attr => {
      //TODO: forsi ce se tu kada i trebat odmah zamjenit collectionType?
      if(!(attr.type instanceof CollectionType)){
    
        // console.log("Setting attribute: "+attr.name+" with value: "+data[attr.name]);
        
         this._editRow.data[attr.name] = data[attr.name];      
      }
   
    });
    
  }

  public getCollection(): IRow[] { return this._collection; }
  // public getEditCollection(): IRow[] { return this._editCollection; }

  public getDeletedRows(): IRow[] { return this._deletedRows; }

  public findByKey(key:any = this._key.value): IRow {
    return this._collection.find((r: IRow) => { return r.data[this._key.name] == key/*this._key.value */});
  }

  /** returns null or array of Keys where key name is found */
  public findInUniqueKeys(name: string): IKey[] {
    return this._uniqueKeys.find((keys) => { return keys.find((key) => { return key.name == name; }) != null; });
  }

  /** fill's keys with data attributes, you may need to call <code>findInUniqueKeys</code> first */
  public fillKeys(keys: IKey[], data: any = this._editRow.data): void {
    keys.forEach((key) => { key.value = data[key.name]; });
  }


  public handleFileChanged(row:IFileRow, attr:Attribute, e:FileInput): void{
    
    // console.log(row, attr, e);
    
    if(e != null && e.files != null && e.files.length > 0){
      
      if(row.files == null)
        row.files = new Array();

      row.files.push({name:attr.name, value:e.files[0]});

    }

  }

  public validateAttribute(attr: Attribute, row: IRow = this.getEditRow()): void {

    attr.setError('', ErrorType.NONE);

    try {

      attr.type.validate(row.data[attr.name]);

      this.checkUnique(attr.name, row);

      attr.setError('', ErrorType.SUCCESS);

      this.onValidateAttribute.emit(attr);

    } catch (error) {
      attr.processError(error);
    }

  }

  public validateOnlyMandatory(row: IRow = this.getEditRow()): void {

    this.attributes.forEach((attr) => {

      try {

        attr.setError('', ErrorType.NONE);

        attr.type.validate(row.data[attr.name]);

        attr.setError('', ErrorType.SUCCESS);

      } catch (error) {
        attr.processError(error);
      }
    });
  }

  public validateRow(row: IRow = this.getEditRow()) {
    this.onValidateRow.emit(row);
  }

  /** This method will not work on unique key witch is also key attribute */
  public checkUnique(name: string, row: IRow = this.getEditRow()): void {

    let uks: IKey[] = this.findInUniqueKeys(name);

    if (uks == null) return;//Unique key is not defined

    this.fillKeys(uks, row.data);

    let exsisting: IRow = this.findByKeys(uks);

    if (exsisting != null && row.state == ROW_STATE.NEW ||
      exsisting != null && exsisting.data[this._key.name].toString().toLowerCase() != row.data[this._key.name].toString().toLowerCase() && name != this._key.name) {

      throw new ValidationError(ErrorType.ERROR, Messages.FIELD_UNIQUE);
    }
  }


  public findByKeys(keys: IKey[]): IRow {

    let ok: boolean = true;

    for (var d = 0; d < this._collection.length; d++) {

      let data = this._collection[d].data;
      ok = true;

      for (var k = 0; k < keys.length; k++) {

        let key = keys[k];

        let dataEmpty: boolean = Utils.emptyValue(data[key.name]);
        let keyEmpty: boolean = Utils.emptyValue(key.value);

        if (!dataEmpty && !keyEmpty &&
          data[key.name].toString().toLowerCase() != key.value.toString().toLowerCase() ||
          dataEmpty && !keyEmpty || !dataEmpty && keyEmpty) {

          ok = false;
          break;
        }

      }

      if (ok) return this._collection[d];
    }

    return null;
  }


  public getDataForSend(data:any = this.getEditRow().data): any{
    let d:any = new Object();

    this.attributes.filter(a => !(a.type instanceof CollectionType)).forEach(attr => d[attr.name] = data[attr.name]);

    return d;
  }

  public clearAttributes(): void {
    this.attributes.forEach((attr) => { attr.clear(); });
  }


  public refreshMappings(attr: Attribute, newData: any): void {

    (<BasicType>attr.type).mapping.forEach((map) => {

      if (Utils.emptyValue(newData))
        this._editRow.data[map.to] = null;

      else
        this._editRow.data[map.to] = newData[map.from];

    });

  }


  public needsFileUpload(): boolean {

    // let uploadNeeded: boolean = false;

    // for(let i=0; i<this.attributes.length; i++){
    //   let attr:Attribute = this.attributes[i];

    //   if (attr.type instanceof CollectionType)
    //     uploadNeeded = attr.type.needsFileUpload();

    //   else
    //     uploadNeeded = attr.needsToUpload();

    //   if (uploadNeeded) break;

    // }
    // return uploadNeeded;

    //TODO: zasada provjerava samo root edit row.....prosiriti na CollectionType?
    return this._needsFileUpload([<IFileRow>this._editRow]);
  }

  private _needsFileUpload(collection:IFileRow[]): boolean{

    if(collection == null && collection.length == 0) return false;

    for (let i = 0; i < collection.length; i++) {
      
      const row = collection[i];
      
      if(row.files != null && row.files.length > 0) return true;

      
      const cTypes = this.attributes.filter(a => a.type instanceof CollectionType);
    
      
      for (let c = 0; c < cTypes.length; c++) {

        const ct = <CollectionType>cTypes[c].type;
        
        if(ct._needsFileUpload(row.data[cTypes[c].name])) return true;

      }
    
    }

    return false;
  }

  public getVisibleAttributes(onlyBase: boolean = true): Attribute[] {
    return this.attributes.filter(attr => attr.visible && (!onlyBase || onlyBase && !(attr.type instanceof CollectionType)));
  }

  public getVisibleAttributeNames(onlyBase: boolean = true): string[] {
    let nms:string[] = [];
    
    this.getVisibleAttributes(onlyBase).forEach(attr => nms.push(attr.name) );

    return nms;
  }

  private _setCollectionTypeAttributesForRow(row: IRow): void {

    this.attributes.forEach((attr) => {

      if (attr.type instanceof CollectionType)
        attr.type.setRowsForCollection(row == null ? [] : row.data[attr.name]);

    });

  }

  private _setCTypeAttrsForEditRow(row: IRow): void {

    this.attributes.forEach((attr) => {

      if (attr.type instanceof CollectionType)
        attr.type.setRowsForCollection(row == null ? [] : row.data[attr.name]);

    });

  }

  // private _copyAndSetCollectionTypeAttributesForRow(row: IRow): void {

  // this.attributes.filter(attr => attr.type instanceof CollectionType).forEach((attr) => {

  //(<CollectionType>attr.type)._editCollection = row == null ? [] : CollectionType.copyRows(row.data[attr.name]);

  // if (attr.type instanceof CollectionType) 

  // let a = ($.extend(true, [], row.data[attr.name]));
  // console.log(a);        

  // attr.type.setCollection([]);

  // (<CollectionType>attr.type)._collection = row == null ? [] : CollectionType.copyRows(row.data[attr.name]);
  // (<CollectionType>attr.type).setRowsForCollection( row == null ? [] : CollectionType.copyRows(row.data[attr.name]));
  // attr.type.setRowsForCollection( row == null ? [] : row.data[attr.name].slice());
  // attr.type.setRowsForCollection( row == null ? [] : JSON.parse(JSON.stringify(row.data[attr.name])));
  // (<CollectionType>attr.type).setRowsForCollection( row == null ? [] : row.data[attr.name].slice(0));

  //     });

  // }

  public setParentCollectionType(collectionType: CollectionType): void {
    if (this._parentCollectionType == null){
        this._parentCollectionType = collectionType;

    }else{
        console.log(this);
        console.log(collectionType);
        throw new Error("Parent collection type cannot be set more than once!");
     }

  }

  public getParentCollectionType(): CollectionType{
    return this._parentCollectionType;
  }

  public setSingleMode(mode:boolean): void{
    this._singleMode = mode;
  }

  public isSingleMode(): boolean{
    return this._singleMode;
  }


  static findLinkByRel(links: ILink[], rel: string): ILink {
    return links.find((link: ILink) => { return link.rel == rel; });
  }

  static findLinkByRelAndName(links: ILink[], rel: string, name: string): ILink {
    return links.find((link: ILink) => { return link.rel == rel && link.name == name; });
  }

  public getUnmodifiedCollectionLink(): ILink{
    return Object.assign({}, CollectionType.findLinkByRel(this.links, LINK_TYPE.SELF));
  }

  private _getCollectionLink(currentRow:boolean, link:ILink = null): ILink{
    // let link: ILink = CollectionType.findLinkByRel(this.links, LINK_TYPE.SELF);

    if(link == null)
      link = CollectionType.findLinkByRel(this.links, LINK_TYPE.SELF);

    if (link == null)
      throw new Error("Collection link cannot be found!");

    if (link.href.indexOf(CollectionType.LINK_KEY_NAME) >= 0) {

      if (this._parentCollectionType == null)
        throw new Error("Parent collection cannot be null when collection link depends on parent!");

      let row:IRow = currentRow?this._parentCollectionType.getCurrentRow() : this._parentCollectionType.getEditRow();

      if (row == null)
        throw new Error("Parent row must be set when requesting child data! "+(currentRow?"(current-row)":"(edit-row)"));

      link = Object.assign({}, link);//Dont change original

      let pos = link.href.lastIndexOf(CollectionType.LINK_KEY_NAME);
      
      link.href = link.href.substring(0,pos) + row.data[this._parentCollectionType.getKey().name] + link.href.substring(pos+CollectionType.LINK_KEY_NAME.length)
      // link.href = link.href.replace(CollectionType.LINK_KEY_NAME, row.data[this._parentCollectionType.getKey().name]);

      if (link.href.indexOf(CollectionType.LINK_KEY_NAME) >= 0)
        link = this._parentCollectionType._getCollectionLink(currentRow, link);

    }
    // console.log(link);

    return link;
  }

  public getCollectionLinkForEdit(): ILink {
    return this._getCollectionLink(false);
  }
  public getCollectionLink(): ILink {
    return this._getCollectionLink(true);
    /* 
    let link: ILink = CollectionType.findLinkByRel(this.links, LINK_TYPE.SELF);

    if (link == null)
      throw new Error("Collection link cannot be found!");

    if (link.href.indexOf(CollectionType.LINK_KEY_NAME) >= 0) {

      if (this._parentCollectionType == null)
        throw new Error("Parent collection cannot be null when collection link depends on parent!");

      if (this._parentCollectionType.getCurrentRow() == null)
        throw new Error("Parent row must be set when requesting child data!");

      link = Object.assign({}, link);//Dont change original

      link.href = link.href.replace(CollectionType.LINK_KEY_NAME,
        this._parentCollectionType
          .getCurrentRow()
          .data[this._parentCollectionType.getKey().name]);

    }
    // console.log(link);

    return link;
    */
  }

  public getOperationLink(row:IRow = this._editRow): string{
    let href:string = this.getCollectionLink().href;

    if(this.isSingleMode())
      return href;
    else
      return href + '/' + row.data[this._key.name];
    //collectionType.getCollectionLink().href + '/' + collectionType.getEditRow().data[collectionType.getKey().name]
  }
  

  //Ni okej jer ce se npr kopirat svi komentari i pitanja.....jako puno podataka
  public static copy(row: IRow): IRow {
    return JSON.parse(JSON.stringify(row));
    //$.extend(true, [], row.data[attr.name])
    //return { data: Object.assign({}, row.data), state: row.state };

    // return JSON.parse(JSON.stringify(row));
    // return { data: jQuery.extend(true, {}, row.data), state: row.state }; 

    // let newRow:Row = {data: new Object(), state: row.state};

    // this.attributes.forEach(attr => {

    //   if(attr.type instanceof CollectionType)
    //     newRow.data[attr.name] = attr.type.copy(row.data[attr.name]);

    //   else
    //     newRow.data[attr.name] = row.data[attr.name];

    // });

  }

  public static copyRows(rows: IRow[]): IRow[] {
    // let newRows:IRow[] = [];

    // rows.forEach((r) => { newRows.push(CollectionType.copy(r)); });
    // return newRows;
    return JSON.parse(JSON.stringify(rows));
  }

  public setFilters(attrNames:string[], value:string): void{
      this.attributes.filter(attr => attrNames.indexOf(attr.name) > -1).forEach(attr => attr.filter = value);
  }

  public setChildFilters(attrName:string, attrNames:string[], value:string): void{
      (<CollectionType>this.attributes.find(attr => attr.name == attrName).type).setFilters(attrNames, value);
  }


  public static filter(collection:CollectionType): IRow[]{
  
    // console.log(collection);    
    
    let rows:IRow[] = [];

    let colRows:IRow[] = CollectionType.copyRows(collection.getCollection());

    for(let i=0; i<colRows.length; i++){

      let row:IRow = colRows[i];

      let valid:boolean = true;

      for(let j=0; j<collection.attributes.length; j++){

        let attr:Attribute = collection.attributes[j];

        if(attr.type instanceof CollectionType && row.data[attr.name] != null && row.data[attr.name].length > 0){
          
          collection.setCurrentRow(row);//Potrebno je da bi se postavi child-collection
          row.data[attr.name] = CollectionType.filter(attr.type);
        
        }else if(attr.filter != '' && row.data[attr.name].toString().toLowerCase().indexOf(attr.filter.toLowerCase()) == -1){
          valid = false;
          break;
        }         

      }

      if(valid)
        rows.push(row);

    }

    collection.setCurrentRow(null);

    return rows;
  }

  public filtersEmpty(): boolean{
    return this.attributes.find(attr => attr.filter != '') == null;
  }

  public getClassForErrorType(): string {
    return ValidationError.getClassForErrorType(this.error);
  }

  public setInBatchMode(value:boolean): void{
    this._inBatchMode = value;
  }
  
  public isInBatchMode(): boolean{
    return this._inBatchMode;
  }

  public processError(error: any) {
    console.log(this);

    console.log(error);

    if (error instanceof ValidationError)
      this.setError(error.message, error.type);

    else
      alert(error.message);

  }

  public setError(message: string, type: string = ErrorType.ERROR): void {
    this.error.message = message;
    this.error.type = type;
  }

  public clearError(): void {
    this.error = ValidationError.getClearError();
  }

}


export interface ILink {
  href: string;
  kind: string;
  name: string;
  rel: string;
}

export const LINK_TYPE = {
  LOV: 'lov',
  COLLECTION: 'collection',
  SELF: 'self'
}

export const enum ROW_STATE {
  NEW,
  CHANGED,
  QUERY,
  DELETED
}

export const enum ROW_ADDING_POLICY {
  START,
  NORMAL,
  END
}


export interface IRow {
  data: any;
  state: ROW_STATE;
}

export interface IFileRow extends IRow{
  files?:IFile[]
}

export interface IFile{
  name: string;
  value: any;
  error?: string;
}

export interface IKey {
  name: string;
  value: any;
}

// interface DataWithIndex{
//   data:any;
//   index:number;
// }
