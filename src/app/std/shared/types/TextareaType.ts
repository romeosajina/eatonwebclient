import { DataType } from '../common/StaticValues';
import { Mapping } from '../common/Mapping';
import { InputType, ILength } from './InputType';
import { Utils } from '../common/Utils';
import { ValidationError } from '../common/ValidationError';
import { ErrorType, Messages } from '../common/StaticValues';

export class TextareaType extends InputType{

    rowNum:number;

    constructor( mandatory:boolean = true, length:ILength = null, rowNum:number = 4) {
        super(DataType.TEXTAREA, mandatory, length);
        this.rowNum = rowNum;
    }

}