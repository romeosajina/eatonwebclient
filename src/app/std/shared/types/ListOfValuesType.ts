import { DataType } from '../common/StaticValues';
import { Mapping } from '../common/Mapping';
import { BaseType } from './BaseType';

export class ListOfValuesType extends BaseType{

  dataName:string;
  visibleAttribute:string;
  mapping:Mapping[];
  data:any[];
  staticData:boolean;
  
  constructor(type:string = null, dataName:string = '', 
              visibleAttribute:string = '', 
              mapping:Mapping[] = [], 
              data:any[] = [],
              staticData:boolean = false){
    super();
    this.type = type != null ? type : DataType.RICH_SELECT_ONE;
    this.dataName = dataName;
    this.visibleAttribute = visibleAttribute;
    this.mapping = mapping;
    this.data = data;
    this.staticData = staticData;
  }

  public needsListOfValues(): boolean{

    return this.staticData == false //Ako je lista dinamička 
           ||
           this.staticData == true && 
           this.data.length == 0;//Ako je lista statična, a nije dohvaćena
  }


  public clear(): void{
    if(!this.staticData) this.data = [];
  }

}
