import { DataType } from '../common/StaticValues';
import { Mapping } from '../common/Mapping';
import { BaseType } from './BaseType';
import { Utils } from '../common/Utils';
import { ValidationError } from '../common/ValidationError';
import { ErrorType, Messages } from '../common/StaticValues';


export class InputType extends BaseType {

    length:ILength; 

    constructor(type: string = null,  mandatory:boolean = true, length:ILength = null) {

        super(type != null ? type : DataType.TEXT, mandatory);

        this.length = length != null ? length : {min:1, max:100};
    }


    public clear(): void { }

    public validate(data:any): void{
        data = Utils.strip(data);
        
        super.validate(data);

        this.checkLength(data);
    }


    public checkLength(data:any): void{
        
        if(!Utils.emptyValue(data) && 
          (data.toString().length > this.length.max ||
            data.toString().length < this.length.min)){
          
            let msg:string = Utils.format(Messages.FIELD_LENGTH, 
                                          [
                                            this.length.max.toString(),  
                                            this.length.min.toString()
                                          ]);
  
            throw new ValidationError(ErrorType.ERROR, msg);
    
        }
    
    }

}

export interface ILength{
    min:number;
    max:number;
  }