import { DisplayType } from '../common/StaticValues';
import { BaseDisplay } from './BaseDisplay';
import { ListOfValuesType } from '../types/ListOfValuesType';

export class ListOfValuesDisplay extends BaseDisplay{

    lovType:ListOfValuesType;

    constructor(lovType:ListOfValuesType, type:string = DisplayType.LIST_OF_VALUES){
  
      super(type);
      this.lovType = lovType;

    } 
  
  
    public getValue(attrValue:any): string{
      if(attrValue == null) 
        return null;

      let val:number = this.lovType.data.find(i => i.value == attrValue);
     
      if(val == null)
        return null;
      
      return val[this.lovType.visibleAttribute];

    }

}
   