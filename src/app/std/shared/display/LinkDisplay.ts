import { DisplayType } from '../common/StaticValues';
import { BaseDisplay } from './BaseDisplay';

export class LinkDisplay extends BaseDisplay{

    linkAttr:string;
    linkFun:Function;
    target:string;

    constructor(linkAttr:string = null, linkFun:Function = null, type:string = DisplayType.LINK){
  
      super(type);
      this.linkAttr = linkAttr;
      this.linkFun = linkFun;
      this.target = null;
    } 
  
  
  
    getLink(data:any): string{
      
      let link:string = data[this.linkAttr];

      if(this.linkAttr == null && this.linkFun != null){

        link = this.linkFun(data);

      }

      return link;
    }

}
   