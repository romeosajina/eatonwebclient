import { DisplayType } from '../common/StaticValues';


export class BaseDisplay{
    type:string;
    
    constructor(type:string = DisplayType.TEXT){

        this.type = type;
   
    }
  

}
  