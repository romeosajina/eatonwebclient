import { TemplateRef } from '@angular/core';
import { DisplayType } from '../common/StaticValues';
import { BaseDisplay } from './BaseDisplay';

export class TemplateDisplay extends BaseDisplay{

    tmplRef: TemplateRef<any>;

    constructor(tmplRef: TemplateRef<any>, type:string = DisplayType.TEMPLATE_REFERENCE){
      super(type);
      this.tmplRef = tmplRef;
    } 
  

}
   