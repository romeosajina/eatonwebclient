# Naslov 1
## Naslov 2
### Naslov 3
#### Naslov 4

Običan text

**bold**

*italic*

~~striketrough~~


<http://www.unipu.hr>

[text](http://www.unipu.hr)


Slika
![](012.png)

> blockquote
> 
> ovo je citiranje

test|test2
--|--
test3| test4
test5| test6


+------+------+
| test | tet2 |
+======+======+
| tes4 | tet5 |
+------+------+
| tes6 | tet7 |
+------+------+



- list
- list 
    - list
1. list
2. list
    1. list


# Naslov

    > neki kod more bit
    $mk dir test
    $ls -al
    $ll
    $vim test.md


Romeo je upisao naredbu `def foo` i došao zadnji na natjecanju

```python

def foo();
    pass

```


```bash

cd
pwd 
htop

```




**six sigma primjeri**
