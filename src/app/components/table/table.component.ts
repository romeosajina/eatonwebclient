import { Component, OnInit, ViewChild, Input, Output, TemplateRef, ContentChild, EventEmitter } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatTable } from '@angular/material';
import { CollectionType, IRow, ROW_STATE, IFileRow } from '../../std/shared/types/CollectionType';
import { ErrorType } from 'src/app/std/shared/common/StaticValues';
import { FileInput } from 'ngx-material-file-input';
import { Attribute } from 'src/app/std/shared/common/Attribute';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.sass']
})
export class TableComponent implements OnInit {
  // https://blog.angular-university.io/angular-material-data-table/
  
  // @Output() onRowClick = new EventEmitter<IRow>();
  // @Output() onRowDblClick = new EventEmitter<IRow>();
  @Output() onAction = new EventEmitter<ActionEvent>();
  
  @Input() collection:CollectionType;
  @Input() options: ITableOptions = TableComponent.buildStandardTableOptions();
  @Input() attributesTemplate:TemplateRef<any>;

  // displayedColumns: string[] = ["id", "userId", "companyId", "date", "deliveryTime", "status"];
  dataSource = new MatTableDataSource<IRow>([]);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatTable) matTable: MatTable<any>;
  // @ContentChild(TemplateRef) template: TemplateRef<any>;

  constructor() { }

  ngOnInit() {

    this.collection.onCollectionChanged.subscribe(() => this.refresh() );

    // console.log(this.matTable);

    // console.log(this.attributesTemplate);
    

  }

  refresh(): void{
    this.dataSource = new MatTableDataSource<IRow>(this.collection.getCollection());
    this.dataSource.paginator = this.paginator;
  }

  _getAttributes(){
    return this.collection == null? [] : this.collection.getVisibleAttributes();
  }

  _getDisplayedColumns(): string[]{
    return this.collection == null? [] : this.collection.getVisibleAttributeNames();
  }

  private _rowIsValid(row:IRow): boolean{
    
    if(row == null) return true;

    // this.collection.attributes.forEach(a => a.setError('', ErrorType.NONE));

    if(row.state != ROW_STATE.QUERY){
    
      this.collection.validateOnlyMandatory(row);

      return this.collection.isValid();
      
      // this.collection.validateRow(row);
    }

    return true;
  }

  onClick(row:IRow): void{

    if(row == this.collection.getCurrentRow()) return;

    if(!this._rowIsValid(this.collection.getCurrentRow())) return;

    this.collection.setCurrentRow(row);

    this.onAction.emit({action:ACTION_TYPE.ROW_CLICK, row:row});
    // this.onRowClick.emit(row);
  }
  onDblClick(row:IRow): void{
    this.onAction.emit({action:ACTION_TYPE.ROW_DOUBLE_CLICK, row:row});
    // this.onRowDblClick.emit(row);
  }


  add(): void{

    if(!this._rowIsValid(this.collection.getCurrentRow())) return;
    
    // this.collection.createRowAndSetItForEdit();
    // this.collection.addRowToCollection(this.collection.getEditRow());
    // this.collection.setEditRowForCurrentAndClear();
    // Ne treba radit sa edit jer je sve u edit modu

    //Ima mastera pa onda se redak ne moze dodati ako master nije postavljen
    if(this.collection.getParentCollectionType() != null && this.collection.getParentCollectionType().getCurrentRow() == null)
      return;
    

    if(this.options.doStandardAddAction){
      
      let row:IRow = this.collection.create();
      this.collection.addRowToCollection(row);
      // this.collection.getCollection().push(row);
      // this.collection.onCollectionChanged.emit();
      this.collection.setCurrentRow(row);
   
    }else{
      this.onAction.emit({action:ACTION_TYPE.ADD, row:null});
    }

  }

  edit(): void{

    if(!this._rowIsValid(this.collection.getCurrentRow()) || null == this.collection.getCurrentRow()) return;
    
    if(this.options.doStandardEditAction){
  
      if(this.collection.getCurrentRow().state == ROW_STATE.QUERY)
        this.collection.getCurrentRow().state = ROW_STATE.CHANGED;
  
    }else{
      this.onAction.emit({action:ACTION_TYPE.EDIT, row:this.collection.getCurrentRow()});

    }
  }

  delete(): void{

    if(null == this.collection.getCurrentRow()) return;
    // this._rowIsValid(this.collection.getCurrentRow()) || 

    if(this.options.doStandardDeleteAction){ 

      if(this.collection.getCurrentRow().state == ROW_STATE.NEW){
        let row = this.collection.deleteRowFromCollection(this.collection.getCurrentRow());
        this.collection.setCurrentRow(row); 

      }else{
        this.collection.getCurrentRow().state = ROW_STATE.DELETED; 

      }
      
      this.collection.clear();
      
    }else{
      this.onAction.emit({action:ACTION_TYPE.DELETE, row:this.collection.getCurrentRow()});

    }
  }

  isInput(row:IRow): boolean{ return (this.isNew(row) || this.isChanged(row)) && row == this.collection.getCurrentRow(); }
  isNew(row:IRow): boolean{ return row.state == ROW_STATE.NEW }
  isChanged(row:IRow): boolean{ return row.state == ROW_STATE.CHANGED }
  isDeleted(row:IRow): boolean{ return row.state == ROW_STATE.DELETED }

  public static buildStandardTableOptions(): ITableOptions{
    return{
      doStandardAddAction: true,
      doStandardEditAction: true,
      doStandardDeleteAction: true,
      addActionVisible: true,
      editActionVisible: true,
      deleteActionVisible: true  
    }
  }

}


export interface ActionEvent{
  action: ACTION_TYPE;
  row:IRow;

}

export interface ITableOptions{
  doStandardAddAction:boolean;
  doStandardEditAction:boolean;
  doStandardDeleteAction:boolean;
  addActionVisible:boolean;
  editActionVisible:boolean;
  deleteActionVisible:boolean;
}

export const enum ACTION_TYPE{
  ADD,
  EDIT,
  DELETE,
  ROW_CLICK,
  ROW_DOUBLE_CLICK,
}

/*
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
  {position: 11, name: 'Sodium', weight: 22.9897, symbol: 'Na'},
  {position: 12, name: 'Magnesium', weight: 24.305, symbol: 'Mg'},
  {position: 13, name: 'Aluminum', weight: 26.9815, symbol: 'Al'},
  {position: 14, name: 'Silicon', weight: 28.0855, symbol: 'Si'},
  {position: 15, name: 'Phosphorus', weight: 30.9738, symbol: 'P'},
  {position: 16, name: 'Sulfur', weight: 32.065, symbol: 'S'},
  {position: 17, name: 'Chlorine', weight: 35.453, symbol: 'Cl'},
  {position: 18, name: 'Argon', weight: 39.948, symbol: 'Ar'},
  {position: 19, name: 'Potassium', weight: 39.0983, symbol: 'K'},
  {position: 20, name: 'Calcium', weight: 40.078, symbol: 'Ca'},
];*/