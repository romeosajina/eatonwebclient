import { Component, OnInit, TemplateRef, Input, ViewChild } from '@angular/core';
import { DialogComponent, IDialogData } from 'src/app/components/dialog/dialog.component';
import { MatDialogRef, MatDialog } from '@angular/material';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.sass']
})
export class MessageComponent implements OnInit {

  @ViewChild('dialogContentRef') dialogContentRef:TemplateRef<any>;

  dialogRef:MatDialogRef<DialogComponent, IDialogData>;
  messageData:IMessageData;

  constructor(public dialog: MatDialog) { }
  
  ngOnInit() { }

  show(msgData:IMessageData): void{
    this.messageData = msgData;
    setTimeout(() => { this._show(msgData) }, 0);
  }

  private _show(msgData:IMessageData): void{
    this.dialogRef = DialogComponent.open(this.dialog, {
                                                        title: msgData.type, 
                                                        contentRef: this.dialogContentRef, 
                                                        ok: msgData.ok != null? msgData.ok : "U redu",
                                                        cancel: msgData.cancel != null? msgData.cancel : "Odustani",
                                                        cancelVisible: msgData.cancelVisible!=null? msgData.cancelVisible : false,
                                                        width: msgData.width!=null? msgData.width : "30%",
                                                        onOkHandler: this._ok.bind(this),
                                                        onCancelHandler: this._cancel.bind(this),
                                                      });
  }

  private _cancel(afterClosed:boolean): void{
    if(afterClosed) return;

    if(this.messageData.onCancelHandler != null)
      this.messageData.onCancelHandler(afterClosed);

    this.dialogRef.close();
  }

  private _ok(): void{
    
    if(this.messageData.onOkHandler != null)
      this.messageData.onOkHandler();

    this.dialogRef.close();
  }


}

export interface IMessageData{
  type: "warning" | "info" | "error";
  message: string;
  cancel?:string;
  ok?:string;
  width?:string;
  cancelVisible?:boolean;
  onCancelHandler?:Function;
  onOkHandler?:Function;
}