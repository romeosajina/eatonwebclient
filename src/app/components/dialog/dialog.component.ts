import { Component, OnInit, Inject, TemplateRef, ApplicationRef, ChangeDetectorRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.sass']
})
export class DialogComponent implements OnInit {

  // cancelCalled:boolean = false;
  _show:boolean = true;

  constructor(public dialogRef: MatDialogRef<DialogComponent>, @Inject(MAT_DIALOG_DATA) public config: IDialogData) {

    if(config.cancelVisible == null) 
      config.cancelVisible = true;

      dialogRef.afterClosed().subscribe( () => this.cancel(true) );
      // dialogRef.beforeClosed().subscribe( () => this.cancel() );
  }

  ngOnInit() {
  }

  public ok(): void{
    if(this.config.onOkHandler == null)
      this.dialogRef.close(true);
    else
      this.config.onOkHandler();
  }

  public cancel(afterClosed:boolean = false): void{
    // if(!this.cancelCalled)
    //   this.cancelCalled = true;
    // else
    //   return;
    
    if(this.config.onCancelHandler == null)
        this.dialogRef.close(false);
    else
      this.config.onCancelHandler(afterClosed);
  }

  public static open(dialog: MatDialog, dialogData: IDialogData): MatDialogRef<DialogComponent, IDialogData>{
    let dialogRef = dialog.open(DialogComponent, { data: dialogData, width: dialogData.width/*, height:dialogData.height*/, maxWidth:"95vw", });

    return dialogRef;
  }


  public static disableClose(dialogRef:MatDialogRef<DialogComponent, IDialogData>): void{
    dialogRef._containerInstance._config.data.btnEnabled = false;
    dialogRef.disableClose = true;
  }
  
  public static enableClose(dialogRef:MatDialogRef<DialogComponent, IDialogData>): void{
    dialogRef._containerInstance._config.data.btnEnabled = true;
    dialogRef.disableClose = false;
  }
}

export interface IDialogData {
  title: string;
  contentRef: TemplateRef<any>;
  cancel?:string;
  ok?:string;
  width?:string;
  cancelVisible?:boolean;
  onCancelHandler?:Function;
  onOkHandler?:Function;
  btnEnabled?:boolean;
  // height?:string;
}

// export interface IMatDialog{
//   dialogRef: MatDialogRef<DialogComponent, IDialogData>;
//   onCancelHandler?:Function;
//   onOkHandler?:Function;
// }