import { Component, TemplateRef, ViewChild, OnInit, ComponentFactoryResolver, ComponentFactory, ViewContainerRef } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { DataService } from 'src/app/std/services/data.service';
import { environment } from 'src/environments/environment';
import { IDialogData, DialogComponent } from 'src/app/components/dialog/dialog.component';
import { MatDialogRef, MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { BaseChartDirective } from 'ng2-charts';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  providers: [DataService],
  styleUrls: ['./dashboard.component.sass'],
})
export class DashboardComponent implements OnInit{

  @ViewChild('summary') summary:TemplateRef<any>;
  @ViewChild('lineChart') lineChart:TemplateRef<any>;
  @ViewChild('barChart') barChart:TemplateRef<any>;
  @ViewChild('pieChart') pieChart:TemplateRef<any>;
  @ViewChild('table') table:TemplateRef<any>;

  displayedColumns: string[] = ['year', 'month', 'total'];
  dataSource = null;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  private _cards:Array<any> = [];

  /** Based on the screen size, switch from standard to one column per row */
  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    
    map(({ matches }) => {      
      if (matches) {
        return this._cards;
      }

      return this._cards;
    })
  );



  @ViewChild('dialogContentRef') dialogContentRef:TemplateRef<any>;
  dialogRef:MatDialogRef<DialogComponent, IDialogData>;


  public labels:Array<any> = ["Siječanj","Veljača","Ožujak","Travanj","Svibanj","Lipanj","Srpanj","Kolovoz","Rujan","Listopad","Studeni","Prosinac"];
  private _mnths:Array<number> = [1,2,3,4,5,6,7,8,9,10,11,12];

  public datasets:Array<any> = null;
  public datasetsMonths:Array<any> = null;
  public labelsMonths:Array<string> = null;

  private _colors = ["#43a047","#ff3d00","#663399","#ff1744","#efff00","#00ffdc","#ff00e0","#797979","#000dad","#00ff4e","#e09200","#00dcff"];


  public supportedChartTypes = [
    {type:"line", icon:"show_chart"},
    {type:"bar", icon:"bar_chart"},
    {type:"pie", icon:"pie_chart"},
    {type:"doughnut", icon:"data_usage"},
  ];
  public options:any = {
    responsive: true,
    hoverMode: 'index',
    stacked: false,

    title: {
      display: true,
      text: ""
      // text: "Godišnjo-mjesečna distribucija prihoda (kn)"
    },

    legend: {
      display: true,
      position: "right"
    },
 
    responsiveAnimationDuration: 1000
  }; 


  public optionsMonths:any = {
    responsive: true,
    hoverMode: 'index',
    stacked: false,

    title: {
      display: true,
      text:""
    },

    legend: {
      display: true,
      position: "right"
    },
 
    responsiveAnimationDuration: 1000,
    tooltips: {
      mode: 'nearest'
    },
    scales: {
      xAxes: [{
        stacked: true,
      }],
      yAxes: [{
        stacked: true
      }]
    }
  }; 


  pending = null;
  done = null;
  users = null;
  total = null;
  lastYearOrders = null;
  lastYearUsers = null;
  lastYearTotal = null;
  
  constructor(private breakpointObserver: BreakpointObserver, private dataService:DataService, public dialog: MatDialog) {}

  ngOnInit(){    

    BaseChartDirective.defaultColors = [];
    this._colors.forEach(c => BaseChartDirective.defaultColors.push(DashboardComponent.hexToRgb(c)));

    this.dataService.get(environment.API_URL+"statistics/annually?includeSummary=true").subscribe(data => {

      this.pending = data.pending;
      this.done = data.done;
      this.users = data.users;
      this.lastYearOrders = data.lastYearOrders;
      this.lastYearUsers = data.lastYearUsers;

      let items = data.annually;

      this.total = 0.0;
      items.filter(i => new Date().getFullYear() == i.year).forEach(i => this.total += parseFloat(i.total));

      this.lastYearTotal = 0.0;
      items.filter(i => new Date().getFullYear()-1 == i.year).forEach(i => this.lastYearTotal += parseFloat(i.total));


      this.datasets = this._buildAnnuallyDatasets(items);
    
      this.dataSource =  new MatTableDataSource<any>(items);
      this.dataSource.paginator = this.paginator;
    });

    this._cards = [
      { title: 'Podaci u odnosu na prethodnu godinu', cols: 2, rows: 1, contentTemplate: this.summary },
      { title: 'Godišnjo-mjesečna distribucija prihoda (kn)', cols: 1, rows: 2, contentTemplate: this.lineChart },
      { title: 'Godišnjo-mjesečna distribucija prihoda (kn)', cols: 1, rows: 2, contentTemplate: this.barChart },
      { title: 'Godišnjo-mjesečna distribucija prihoda (kn)', cols: 1, rows: 2, contentTemplate: this.pieChart },
      { title: 'Godišnje/mjesečni prihodi', cols: 1, rows: 2, contentTemplate: this.table }  
    ];
    
  }




  private _buildAnnuallyDatasets(items:any): Array<any>{

    let datasets = [];

    let ds = DashboardComponent._groupIt(items);
    
    ds.forEach(y => {
      this._mnths.forEach(i => {if( y.value.find(m => m.month == i) == null) y.value.push({year: parseInt(y.key), month: i, total: 0.00})} );
      y.value.sort((a,b) => a.month - b.month);

      let tmp = [];
      y.value.forEach(v => tmp.push(parseFloat(v.total)));

      datasets.push({data:tmp, label:y.key, fill:false});
    });

    return datasets;
  }

  private _buildMonthlyProducts(items:any): Array<any>{

    let datasets = [];
    let labels = [];

    items.forEach(i => labels.push({id:i.item_id, name:i.item_name}));

    labels = labels.filter((v, i, a) => a.findIndex(w => w.id === v.id ) === i);   

    items.forEach(item => { 
      
      let dsData = [];
      for(let i=0;i<labels.length;i++)   dsData.push(0.0);

      let inx = labels.findIndex(l => l.id == item.item_id);

      dsData[inx] = parseFloat(item.total);

      datasets.push({data:dsData, stack:item.year, label:item.option_name, fill:false});

    });

    this.labelsMonths = [];
    labels.forEach(l => this.labelsMonths.push(l.name));

    return datasets;
  }

  
  public onClick(e:any):void {

    if(e.active.length == 0) return;

    let month = e.active[0]._index + 1;

    this.dataService.get(environment.API_URL+"statistics/monthlyProducts/"+month).subscribe(data => {
      this.datasetsMonths = this._buildMonthlyProducts(data.items);

      this.optionsMonths.title.text = "Mjesečna distribucija prihoda po proizvodu za "+month+". mjesec (kn)";
      
      this._show();
    });
    
  }



  private _show(): void{
    this.dialogRef = DialogComponent.open(this.dialog, {
                                                        title: this.optionsMonths.title.text, 
                                                        contentRef: this.dialogContentRef, 
                                                        ok: "U redu",
                                                        cancelVisible: false,
                                                        width:"99%"
                                                      });
  }
 
  
  expand(card): void{
    DialogComponent.open(this.dialog, {
                                        title: card.title,
                                        contentRef: card.contentTemplate,
                                        ok: "U redu",
                                        cancelVisible: false,
                                        width:"70%"
                                      });
  }
  remove(card): void{
    this._cards.splice(this._cards.indexOf(card), 1);
  }
  
  private static _groupIt(items:Array<any>, key:string="year"): Array<any>{

    const groupedCollection = items.reduce((previous, current)=> {
      if(!previous[current[key]]) 
          previous[current[key]] = [current];
      else 
          previous[current[key]].push(current);

          return previous;
    }, {});

    // this will return an array of objects, each object containing a group of objects
    let ds = Object.keys(groupedCollection).map(key => ({ key, value: groupedCollection[key] }));
    
    return ds;
  }


  static hexToRgb(hex) : any{
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? [
       parseInt(result[1], 16),
       parseInt(result[2], 16),
       parseInt(result[3], 16)
    ] : null;
  } 

}
