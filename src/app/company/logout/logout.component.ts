import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/std/services/UserService';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.sass']
})
export class LogoutComponent implements OnInit {
 
  constructor(public router: Router, private userService:UserService) { }

  ngOnInit() {
    this.userService.logout();
    this.router.navigate(['/login']);
  }

}
