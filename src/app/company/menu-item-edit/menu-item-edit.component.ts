import { Component, OnInit, Input, ViewChild, TemplateRef } from '@angular/core';
import { DialogComponent, IDialogData } from '../../components/dialog/dialog.component';
import { CollectionType, ROW_STATE, IRow, IFileRow } from '../../std/shared/types/CollectionType';
import { MatDialog, MatDialogRef } from '@angular/material';
import { DataService } from '../../std/services/data.service';
import { Attribute } from 'src/app/std/shared/common/Attribute';
import { FileInput } from 'ngx-material-file-input';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-menu-item-edit',
  templateUrl: './menu-item-edit.component.html',
  styleUrls: ['./menu-item-edit.component.sass']
})
export class MenuItemEditComponent implements OnInit {

  @Input() collectionType:CollectionType; //menuItems - Collection
  @ViewChild('dialogContentRef') dialogContentRef:TemplateRef<any>;

  dialogRef:MatDialogRef<DialogComponent, IDialogData>;

  form: FormGroup;
  
  constructor(public dialog: MatDialog, private dataService:DataService, private fb: FormBuilder) { }
  
  
  ngOnInit() { 
    // this.tmp();  

    this.form = this.fb.group({ photo: [] });
  }

  show(): void{
    setTimeout(() => { this._show() }, 0);
                                                      
    // this.dialogRef.afterOpen().subscribe(() => this.refresh() );

  }

  private _show(): void{
    this.dialogRef = DialogComponent.open(this.dialog, {
                                                        title: "Stavka menija", 
                                                        contentRef: this.dialogContentRef, 
                                                        width:"90%",
                                                        onOkHandler: this._save.bind(this),
                                                        onCancelHandler: this._cancel.bind(this)
                                                      });
                                                      
    this.dialogRef.afterClosed().subscribe(() => { this.form.reset(); });
  }

  // public refresh(): void{}

  private _cancel(afterClosed:boolean): void{    
    this.collectionType.clear();
  
    if(afterClosed)
      setTimeout(() => { 
        
        if(this.collectionType.getEditRow().state == ROW_STATE.NEW)//Bug koji je potrebno dodatno istražit
          this.collectionType.setCurrentRow(null);

        this.collectionType.setEditRow(null);         
      }, 0);
    else
      this.dialogRef.close();
  }

  private _save(): void{

    this.collectionType.validateOnlyMandatory(this.collectionType.getEditRow());
    if(!this.collectionType.isValid())
      return;

    DialogComponent.disableClose(this.dialogRef);
    
    this.collectionType.setInBatchMode(true);
    this.dataService.saveDataAndFiles(this.collectionType);  
    this.dataService.onDataSaved.subscribe(() => { this.dialogRef.close(); DialogComponent.enableClose(this.dialogRef); });
    this.dataService.onDataSaveFailed.subscribe(() => { DialogComponent.enableClose(this.dialogRef); });
  }



/*
  doneIt = false;
  tmp():void{
    
    this.collectionType.onCollectionChanged.subscribe(() => {

      if(!this.doneIt){
        this.doneIt = true;

        setTimeout(() => {
          // this.collectionType.setFirstRowAsCurrent();
          // this.collectionType.setCurrentRowForEdit();

          // this.collectionType.findCollectionType('menuItemOptions').getCollection().forEach(r => r.state = ROW_STATE.CHANGED)
          // this.collectionType.findCollectionType('menuItemOptions').setFirstRowAsCurrent();
          // this.collectionType.findCollectionType('menuItemOptions').findCollectionType('menuItemOptionSupplements').getCollection().forEach(r => r.state = ROW_STATE.CHANGED);

          // this.collectionType.createRowAndSetItForEdit();
          // this.collectionType.setEditRowForCurrentAndClear();
          // this.collectionType.findCollectionType('menuItemOptions').addRowToCollection(this.collectionType.findCollectionType('menuItemOptions').create());
          // this.collectionType.findCollectionType('menuItemOptions').setFirstRowAsCurrent();
          // this.collectionType.findCollectionType('menuItemOptions').findCollectionType('menuItemOptionSupplements').addRowToCollection(this.collectionType.findCollectionType('menuItemOptions').findCollectionType('menuItemOptionSupplements').create());
         
          // this.collectionType.getEditRow().data.name = "Test 2";
          
          // let br: AdvancedBatchRequest = new AdvancedBatchRequest(this.collectionType);

          // let parts = br.getPartsForRow();
      
          // console.log(parts);
          // console.log(JSON.stringify(parts, undefined, 2));  
      
          // this.dataService.saveDataBatch(this.collectionType);
        }, 0);
      }

    });

    this.collectionType.getParentCollectionType().onCollectionChanged.subscribe(() => { 
      setTimeout(() => {
        this.collectionType.getParentCollectionType().setCurrentRow(this.collectionType.getParentCollectionType().getCollection()[1]);
        // this.collectionType.getParentCollectionType().setFirstRowAsCurrent()
      }, 1000);
    })
  }
  */
}
/*

{
  "parts": [
    {
      "id": "part0",
      "operation": "create",
      "path": "http://localhost/EatOn/public/server/companies/1/menus/2/menuItems",
      "payload": {
        "id": -1,
        "menuId": "2",
        "name": "Meso test",
        "price": "10",
        "discount": "0",
        "description": "opis",
        "rate": 0,
        "photo": null
      }
    },
    {
      "id": "part1",
      "operation": "create",
      "path": "http://localhost/EatOn/public/server/companies/1/menus/2/menuItems/-1/menuItemOptions",
      "payload": {
        "id": -1,
        "menuItemId": -1,
        "name": "Random",
        "price": "10"
      }
    },
    {
      "id": "part2",
      "operation": "create",
      "path": "http://localhost/EatOn/public/server/companies/1/menus/2/menuItems/-1/menuItemOptions/-1/menuItemOptionSupplements",
      "payload": {
        "id": -1,
        "menuItemOptionId": -1,
        "name": "Random2",
        "price": "10.2",
        "photo": null
        }
    }
  ]
}

*/