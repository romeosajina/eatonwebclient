import { Component, OnInit, Input, ViewChild, TemplateRef } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DialogComponent } from '../../components/dialog/dialog.component';
import { CollectionType, IRow } from '../../std/shared/types/CollectionType';
import { MenuItemEditComponent } from '../menu-item-edit/menu-item-edit.component';
import { ActionEvent, ACTION_TYPE, ITableOptions, TableComponent } from '../../components/table/table.component';
import { MessageComponent } from 'src/app/components/message/message.component';
import { DataService } from 'src/app/std/services/data.service';
import { Messages } from 'src/app/std/shared/common/StaticValues';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-menu-items',
  templateUrl: './menu-items.component.html',
  styleUrls: ['./menu-items.component.sass']
})
export class MenuitemsComponent implements OnInit {

  @Input() collectionType:CollectionType;
  // @ViewChild('dialogContentRef') dialogContentRef:TemplateRef<any>;

  @ViewChild('menuItemEdit') menuItemEdit:MenuItemEditComponent;
  @ViewChild('message') message:MessageComponent;

  constructor(private dataService:DataService) { }

  ngOnInit() {
  }

  onAction(event:ActionEvent): void{

    switch(event.action){
      case ACTION_TYPE.ROW_DOUBLE_CLICK: this.onEdit(event.row); break;//TMP?'
      case ACTION_TYPE.ADD: this.add(); break;
      case ACTION_TYPE.EDIT: this.onEdit(event.row); break;
      case ACTION_TYPE.DELETE: this.delete(); break;
    }

  }

  add(): void{
    if(this.collectionType.getParentCollectionType().getCurrentRow() == null) return;
    
    let row:IRow = this.collectionType.create();
    this.collectionType.setCurrentRow(row);  
    this.collectionType.setCurrentRowForEdit();
    this.menuItemEdit.show(); 
  }

  onEdit(menuItem:IRow): void{
    if(menuItem == null) return;

    this.collectionType.setCurrentRowForEdit();
    
    this.menuItemEdit.show();      
    setTimeout(() => { this.collectionType.findCollectionType('menuItemOptions').onCollectionChanged.emit(); }, 0);      
  }

  delete(): void{
    this.message.show({
      message:Messages.ASK_DELETE,
      type:"warning",
      onOkHandler: this._delete.bind(this)
    });

  }
  private _delete():void{
    this.collectionType.setCurrentRowForEdit();
    this.dataService.remove(this.collectionType);
  }

  getOptions(): ITableOptions{
    let opt = TableComponent.buildStandardTableOptions();
    opt.doStandardAddAction = false;
    opt.doStandardEditAction = false;
    opt.doStandardDeleteAction = false;
    return opt;
  }

  src(img): string{
    return img==null || img==""? null : environment.API_URL + img;
  }
}
