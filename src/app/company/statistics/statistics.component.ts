import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { DataService } from 'src/app/std/services/data.service';
import { environment } from 'src/environments/environment';
import { IDialogData, DialogComponent } from 'src/app/components/dialog/dialog.component';
import { MatDialogRef, MatDialog } from '@angular/material';

//https://www.chartjs.org/samples/latest/

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  providers: [DataService],
  styleUrls: ['./statistics.component.sass']
})
export class StatisticsComponent implements OnInit {
  
  @ViewChild('dialogContentRef') dialogContentRef:TemplateRef<any>;
  dialogRef:MatDialogRef<DialogComponent, IDialogData>;


  public labels:Array<any> = ["Siječanj","Veljača","Ožujak","Travanj","Svibanj","Lipanj","Srpanj","Kolovoz","Rujan","Listopad","Studeni","Prosinac"];
  private _mnths:Array<number> = [1,2,3,4,5,6,7,8,9,10,11,12];

  public datasets:Array<any> = null;
  public datasetsMonths:Array<any> = null;
  public labelsMonths:Array<string> = null;

  public supportedChartTypes = [
    {type:"line", icon:"show_chart"},
    {type:"bar", icon:"bar_chart"},
    {type:"pie", icon:"pie_chart"},
    {type:"doughnut", icon:"data_usage"},
  ];

  public chartType:string = "line";//"doughnut";//"pie";//"bar";//"line";

  public isGridLayout = true;

  public options:any = {
    responsive: true,
    hoverMode: 'index',
    stacked: false,

    title: {
      display: true,
      text: "Godišnjo-mjesečna distribucija prihoda (kn)"
    },

    legend: {
      display: true,
      position: "right"
    },
 
    responsiveAnimationDuration: 1000
  }; 


  public optionsMonths:any = {
    responsive: true,
    hoverMode: 'index',
    stacked: false,

    title: {
      display: true,
      text:""
      // text: "Mjesečna distribucija prihoda po proizvodu (kn)"
    },

    legend: {
      display: true,
      position: "right"
    },
 
    responsiveAnimationDuration: 1000,
    tooltips: {
      mode: 'nearest'
      // intersect: false
    },
    scales: {
      xAxes: [{
        stacked: true,
      }],
      yAxes: [{
        stacked: true
      }]
    }
  }; 


  constructor(private dataService:DataService, public dialog: MatDialog) {}
  
  ngOnInit() { 

    this.dataService.get(environment.API_URL+"statistics/annually").subscribe(data => {
      this.datasets = this._buildAnnuallyDatasets(data.items);

    //   let t = JSON.parse(JSON.stringify(this.datasets[0]));
    //   t.label = "2017";
    //   t.data[1] = 230.00;
    //   t.data[8] = 230.00;
    //   t.data[5] = 230.00;
    //   this.datasets.unshift(t);
      // console.log(this.datasets);
    });
	
  }


  private _buildAnnuallyDatasets(items:any): Array<any>{

    let datasets = [];

    let ds = StatisticsComponent._groupIt(items);
    
    ds.forEach(y => {
      this._mnths.forEach(i => {if( y.value.find(m => m.month == i) == null) y.value.push({year: parseInt(y.key), month: i, total: 0.00})} );
      y.value.sort((a,b) => a.month - b.month);

      let tmp = [];
      y.value.forEach(v => tmp.push(parseFloat(v.total)));

      datasets.push({data:tmp, label:y.key, fill:false});
    });

    return datasets;
  }

  private _buildMonthlyProducts(items:any): Array<any>{

    let datasets = [];
    let labels = [];

    items.forEach(i => labels.push({id:i.item_id, name:i.item_name}));

    labels = labels.filter((v, i, a) => a.findIndex(w => w.id === v.id ) === i);   

    items.forEach(item => { 
      
      let dsData = [];
      for(let i=0;i<labels.length;i++)   dsData.push(0.0);

      let inx = labels.findIndex(l => l.id == item.item_id);

      dsData[inx] = parseFloat(item.total);

      datasets.push({data:dsData, stack:item.year, label:item.option_name, fill:false});

    });

    this.labelsMonths = [];
    labels.forEach(l => this.labelsMonths.push(l.name));

    return datasets;
  }

  
  public onClick(e:any):void {

    if(e.active.length == 0) return;

    let month = e.active[0]._index + 1;

    this.dataService.get(environment.API_URL+"statistics/monthlyProducts/"+month).subscribe(data => {
      this.datasetsMonths = this._buildMonthlyProducts(data.items);

      this.optionsMonths.title.text = "Mjesečna distribucija prihoda po proizvodu za "+month+". mjesec (kn)";
      
      this._show();
    });
    
  }



  private _show(): void{
    this.dialogRef = DialogComponent.open(this.dialog, {
                                                        title: this.optionsMonths.title.text, 
                                                        contentRef: this.dialogContentRef, 
                                                        ok: "U redu",
                                                        cancelVisible: false,
                                                        width:"99%"
                                                      });
  }
 
  
  private static _groupIt(items:Array<any>, key:string="year"): Array<any>{

    const groupedCollection = items.reduce((previous, current)=> {
      if(!previous[current[key]]) 
          previous[current[key]] = [current];
      else 
          previous[current[key]].push(current);

          return previous;
    }, {});

    // this will return an array of objects, each object containing a group of objects
    let ds = Object.keys(groupedCollection).map(key => ({ key, value: groupedCollection[key] }));
    
    return ds;
  }

  // public chartHovered(e:any):void {
  //   console.log(e);
  // }

}


// public datasetsMonths = {
  //   labels: ['Vegan nešto', 'Item id 3', 'Cetvorka'],
  //   datasets: [{
  //     label: 'Velika',
  //     stack: 2018,
  //     data: [
  //       74.94,0,0
  //     ]
  //   }, {
  //     label: 'Mala',
  //     stack: 2018,
  //     data: [
  //       50.00,0,0
  //     ]
  //   },{
  //     label: 'Srednja',
  //     stack: 2018,
  //     data: [
  //       70.00,0,0
  //     ]
  //   }, {
  //     label: 'Item id 3  option 1',
  //     stack: 2018,
  //     data: [
  //       0,70,0
  //     ]
  //   }, {
  //     label: 'Item id 3  option 2',
  //     stack: 2018,
  //     data: [
  //       0,30,0
  //     ]
  //   }, {
  //     label: 'cetvorka opt',
  //     stack: 2018,
  //     data: [
  //       0,0,100.00
  //     ]
  //   },{
  //     label: 'Velika',
  //     type: 'bar',
  //     stack: 2017,
  //     data: [
  //       120.94,0,0
  //     ]
  //   }, {
  //     label: 'Mala',
  //     stack: 2017,
  //     data: [
  //       510.00,0,0
  //     ]
  //   },{
  //     label: 'Srednja',
  //     stack: 2017,
  //     data: [
  //       732.00,0,0
  //     ]
  //   }]
  // };
 
// data = 
// {
//   "items":[
//     {"year":2018,"item_id":1,"item_name":"Vegan nešto","option_name":"Velika","total":"74.94"},
//     {"year":2018,"item_id":1,"item_name":"Vegan nešto","option_name":"Mala","total":"50.00"},
//     {"year":2018,"item_id":1,"item_name":"Vegan nešto","option_name":"Srednja","total":"70.00"},

//     {"year":2018,"item_id":3,"item_name":"Item id 3","option_name":"Item id 3 option 1","total":"70.00"},
//     {"year":2018,"item_id":3,"item_name":"Item id 3","option_name":"Item id 3 option 2","total":"30.00"},
    
//     {"year":2018,"item_id":4,"item_name":"Cetvorka","option_name":"cetvorka opt","total":"100.00"},
    
//     {"year":2017,"item_id":1,"item_name":"Vegan nešto","option_name":"Velika","total":"120.94"},
//     {"year":2017,"item_id":1,"item_name":"Vegan nešto","option_name":"Mala","total":"510.00"},
//     {"year":2017,"item_id":1,"item_name":"Vegan nešto","option_name":"Srednja","total":"732.00"}
//   ]
// }