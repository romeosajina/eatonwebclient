import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Utils } from 'src/app/std/shared/common/Utils';
import { DataService } from 'src/app/std/services/data.service';
import { environment } from 'src/environments/environment';
import { UserService } from 'src/app/std/services/UserService';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  providers: [DataService],
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  // showSpinner = false;
  email: string;
  password: string;
  repeatedPassword: string;

  loginErrorMsg:string = "";
  registerErrorMsg:string = "";

  _active:boolean = false;
  private _working:boolean = false;

  constructor(public router: Router, private dataService:DataService, private userService:UserService) { }

  ngOnInit() {
    
    if(this.userService.isAuthenticated())
      this.router.navigate(['/home']);

    this.email = "c1@c1.com";
    this.password = "c1";
    //c1@c1.com c1 0PYxyh3bqNs7z8ueBXzcmNA3nxvuAOdaVFFHon2t2YI=

    // if(window.navigator.geolocation){
      // window.navigator.geolocation.getCurrentPosition(this.setPosition.bind(this));
    // }
  }


  login():void{
    
    if(this._working) return;

    this._working = true;

    this.loginErrorMsg = "";
    
    //console.log(JSON.stringify({email:this.email, password:this.userService.generateHash(this.password)}));

    let tmpEmail = this.email;
    let tmpPwd = this.userService.generateHash(this.password);

    this.dataService.save(environment.API_URL + "auth2", {email:tmpEmail, password:tmpPwd}).subscribe(u => {

      if(u != {} && u.id != null){
        
        u.email = tmpEmail;
        u.password = tmpPwd;        

        this.userService.setCompany(u);

        this.router.navigate(['/pendingOrders']);

      }else{
        
        this.loginErrorMsg = "Neispravan unos: email/lozinka.";
      
      }
      
      this._working = false;
    });

  }

  toggle(): void{
    this._active = !this._active;    
  }

  register(): void{

    if(this._working) return;
    
    this._working = true;
    this.registerErrorMsg = "";

    let tmpEmail = this.email;
    let tmpPwd = this.userService.generateHash(this.password);

    let c = {
              id: -1,
              name: "Ime",
              city: "Pula",
              address: "-",
              latitude: "44.8669221",
              longitude: "13.8520822",
              photo: "",
              phone: "--- --- ---",
              delivery: true,
              pickUp: true,
              minDeliveryPrice: "20.00",
              maxDestinationRange: "20.00",
              maxFreeDeliveryRange: "20.00",
              description: null,
              email: tmpEmail,
              password: tmpPwd
            }

            
    this.dataService.save(environment.API_URL + "companies", c).subscribe(u => {

      if(u != {} && u.id != null && u.id+"" != "0"){ //0=unique-fail
        
        u.email = tmpEmail;
        u.password = tmpPwd;        

        this.userService.setCompany(u);

        this.router.navigate(['/editInfo']);

      }else{
        if(u.id+"" == "0")
          this.registerErrorMsg = "Email se već koristi";
        else
          this.registerErrorMsg = u;
      
      }
      
      this._working = false;
    });

  }

  loginEnabled(): boolean{
    return !this._working && !Utils.emptyValue(this.email) && !Utils.emptyValue(this.password);
  }

  registerEnabled(): boolean{
    return !this._working && !Utils.emptyValue(this.email) && !Utils.emptyValue(this.password) && !Utils.emptyValue(this.repeatedPassword) && this.password == this.repeatedPassword;
  }

  passwordReset(): void{ alert("TODO:") }
}
