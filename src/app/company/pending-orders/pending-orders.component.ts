import { Component, OnInit, ViewChild, TemplateRef, Input } from '@angular/core';
import { CollectionType, IRow, ROW_STATE, ILink } from '../../std/shared/types/CollectionType';
import { DataService, IComunicationData } from '../../std/services/data.service';
import { environment } from '../../../environments/environment';
import { UserService } from '../../std/services/UserService';
import { TableComponent, ActionEvent, ACTION_TYPE, ITableOptions } from '../../components/table/table.component';
import { Attribute } from '../../std/shared/common/Attribute';
import { MatDialog, MatDialogRef } from '@angular/material';
import { DialogComponent, IDialogData } from '../../components/dialog/dialog.component';
import { User } from '../../std/User';
import { UserInfoDialogComponent } from '../../shared/user-info-dialog/user-info-dialog.component';
import { AttributesBuilder, IOrder, IOrderItem } from '../../std/shared/common/AttributesBuilder';
import { PusherService } from 'src/app/std/services/pusher.service';
import { EthService } from 'src/app/std/services/eth.service';

@Component({
  selector: 'app-pending-orders',
  providers: [DataService, TableComponent],
  templateUrl: './pending-orders.component.html',
  styleUrls: ['./pending-orders.component.sass']
})

export class PendingOrdersComponent implements OnInit {

  // Da li je pregled gotovih ili tekućih
  @Input() pending:boolean = true;

  commData: IComunicationData = {
    baseUrl:environment.API_URL, 
    getOptions:[
      {
        resourceName: 'orders',
        options: 'expand=orderItems,orderItemSupplements'
      },
      {
        resourceName: 'orderItems',
        options: 'expand=orderItemSupplements'
      }
    ] 
  };

  public collectionType:CollectionType;
  public orderItemsCollectionType:CollectionType;
  public orderItemSupplementsCollectionType:CollectionType;

  @ViewChild('dialogContentRef') dialogContentRef:TemplateRef<any>;
  @ViewChild('userInfo') userInfo:UserInfoDialogComponent;

  deliveryTime:string;
  dialogRef:MatDialogRef<DialogComponent, IDialogData>;

  constructor(public dataService:DataService, 
              private userService:UserService, 
              public dialog: MatDialog,
              private pusherService:PusherService,
              private ethService:EthService) { 
  }

  ngOnInit() {

    this.commData.getOptions.forEach(o => o.options += "&onlyDoneOrders=" + !this.pending );
    

    this.dataService.commData = this.commData;


    this.collectionType = AttributesBuilder.buildOrdersCollection();

    this.orderItemsCollectionType = this.collectionType.findCollectionType("orderItems");
    this.orderItemSupplementsCollectionType = this.orderItemsCollectionType.findCollectionType("orderItemSupplements");

    this.dataService.getDataset(this.collectionType);


    this.pusherService.onOrderAdded.subscribe((order:IOrder) => {
      //Salje se samo glavni objekt pa se requery-a sve
      let link: ILink = this.collectionType.getSelfCollectionLink();

      this.dataService.get(link.href + "/" + order.id + this.dataService.getQueryString(link.name)).subscribe(o => {
        this.collectionType.addToCollectionFromBeginning([o]);
      });

    });

    this.pusherService.onOrderDeleted.subscribe((order:IOrder) => {

      let row = this.collectionType.findByKey(order.id);

      if(row != null){
        this.collectionType.setCurrentRow(row);
        this.collectionType.removeCurrentRowFromCollection();
        // CollectionType.removeRowFromCollection(row, this.collectionType.getCollection());
      }
      
    });


    
    this.dataService.onDataSaved.subscribe(() => { this._onSave(); });
    this.dataService.onDataSaveFailed.subscribe(() => { DialogComponent.enableClose(this.dialogRef); });
  }



  getOrderEditRow():IRow{//TODO: da li ce bit edit row ili obican row?
    return this.collectionType.getEditRow();
  }

  
  onAction(event:ActionEvent): void{
    
    switch(event.action){
      case ACTION_TYPE.ROW_DOUBLE_CLICK: this.onEdit(event.row); break;
      // case ACTION_TYPE.ADD: this.add(); break;
      case ACTION_TYPE.EDIT: this.onEdit(event.row); break;
    }
  }

  onEdit(row:IRow){

    if(!this.pending){
      this._onEdit();

    }else if(row.data.paymentMethod == AttributesBuilder.PAYMENT_METHODS.ETH && (<IOrder>row.data).isOnBlockchain == null){
      if(this.ethService.isConnected())
        this.ethService.orderExist(row.data, this._onEdit.bind(this))
    
    }else{
      this._onEdit();
    }
  }

  private _onEdit() {/*row:IRow*/

    this.collectionType.setCurrentRowForEdit();

    let order:IOrder = this.getOrderEditRow().data;
    // let order:IOrder = row.data;

    let dt = new Date(order.deliveryTime);
    this.deliveryTime = (dt.getHours() > 9? dt.getHours():"0"+dt.getHours()) + ":"+ (dt.getMinutes() > 9? dt.getMinutes() : "0"+dt.getMinutes());


    if(order.orderItems == null)
      this.dataService.getDataset(this.orderItemsCollectionType);

    let title:string = "Broj narudžbe: " + this.getOrderNumber(order);

    this.dialogRef = DialogComponent.open(this.dialog, {
                                                        title: title, 
                                                        contentRef: this.dialogContentRef, 
                                                        width:"95%",
                                                        onOkHandler: this._save.bind(this),
                                                        cancelVisible: this.pending,
                                                        onCancelHandler: this._cancel.bind(this)
                                                      });
    
    this.dialogRef.afterOpened().subscribe(()=>{
      this.orderItemsCollectionType.onCollectionChanged.emit();
    });
    
  }

  private _oldOrder:IOrder = null;
  private _save(): void{

    //Ok je samo close-window (ne save)
    if(!this.pending){
      this._cancel();
      return;
    } 

    this._oldOrder = this.collectionType.getCurrentRow().data;

    DialogComponent.disableClose(this.dialogRef);

    this.dataService.saveData(this.collectionType);

    /*
    this.dataService.onDataSaved.subscribe(() => { 
      this.dialogRef.close(); 

      //Ako je narudžba tek stigla i plaćeno je preko Eth-a, ali nije nagrada (ako je nagrada se ništa ne radi)
      if(oldOrder.status == "O" && this.getOrderEditRow().data.status != "O" && oldOrder.paymentMethod == AttributesBuilder.PAYMENT_METHODS.ETH && !oldOrder.rewarded)
        this.ethService.finalizeOrder(this.getOrderEditRow().data);

      //Ako je narudžba završena izbrisi je iz tekucih
      if(this.getOrderEditRow().data.status == "D"){
          this.collectionType.removeCurrentRowFromCollection();
      }

      DialogComponent.enableClose(this.dialogRef);
      
    });
    
      this.dataService.onDataSaveFailed.subscribe(() => { DialogComponent.enableClose(this.dialogRef); });
      */
  }

  private _onSave(): void{
    const oldOrder = this._oldOrder;

    this.dialogRef.close(); 

    //Ako je narudžba tek stigla i plaćeno je preko Eth-a, ali nije nagrada (ako je nagrada se ništa ne radi)
    if(oldOrder.status == "O" && this.getOrderEditRow().data.status != "O" && oldOrder.paymentMethod == AttributesBuilder.PAYMENT_METHODS.ETH && !oldOrder.rewarded)
      this.ethService.finalizeOrder(this.getOrderEditRow().data);

    //Ako je narudžba završena izbrisi je iz tekucih
    if(this.getOrderEditRow().data.status == "D"){
        this.collectionType.removeCurrentRowFromCollection();
    }

    DialogComponent.enableClose(this.dialogRef);

    this._oldOrder = null;
  }

  private _cancel(): void{
    this.collectionType.clear();
    this.dialogRef.close();
  }

  onDeliveryTimeChanged(): void{
    let dt = this.getOrderEditRow().data.deliveryTime;
    // "2018-11-11 13:53:21"
    let newDt = dt.split(" ")[0] + " " + this.deliveryTime.split(":")[0] + ":" + this.deliveryTime.split(":")[1] + ":00"; 
    this.getOrderEditRow().data.deliveryTime = newDt;

  }

  
  private _calculateTotal(row:IOrderItem): number{

    let discount = parseFloat(row.discount) / 100;
    let price = parseFloat(row.menuItemOptionPrice);
    let amount = parseInt(row.amount);

    let total = amount * price;


    row.orderItemSupplements.forEach((ois:IRow) => {
      total += parseFloat(ois.data.menuItemOptionSupplementPrice) * amount;
    });


    let realDiscount = discount * total;
    
    return (total - realDiscount);
  }

  

  calculateAllTotal(): number{
    let allTotal = 0.0;
    let cl = this.orderItemsCollectionType.getCollection();
   
    if(cl == null)
      return allTotal;

    cl.forEach(i => { allTotal += this._calculateTotal(i.data) });

    return allTotal;
  }

  getIconForStatus(status:string): string{ return AttributesBuilder.ORDER_STATUS[status]; }
  getOrderStatusArray(): Array<any>{ return AttributesBuilder.getOrderStatusArray(); }

  getOrderNumber(order:IOrder): string{
    return order.id.toString().padStart(8, "0");
  }

  showUser(): void{
    
    let o:IPendingOrder = this.collectionType.getCurrentRow().data;

    if(o.user == null){
     
      this.dataService.get(environment.API_URL+"orders/"+o.id+"/user/"+o.userId).subscribe(user => { 
        o.user = user; 
        this.getOrderEditRow().data.user = user;
        setTimeout(() => { this.userInfo.show();}, 0); });
    
    }else{
      this.userInfo.show();

    } 
 
  }


  getOptions(): ITableOptions{
    let opt = TableComponent.buildStandardTableOptions();
    opt.doStandardAddAction = false;
    opt.doStandardEditAction = false;
    opt.addActionVisible = false;
    opt.deleteActionVisible = false;
    opt.editActionVisible = this.pending;
    return opt;
  }
  getOptions2(): ITableOptions{
    let opt = this.getOptions();
    opt.editActionVisible = false;
    return opt;
  }


/*

  tmp(){
    this.collectionType.onCollectionChanged.subscribe(()=>{
      this.collectionType.setCurrentRow(this.collectionType.getCollection()[0]);  
      setTimeout(() => { this.orderItemsCollectionType.onCollectionChanged.emit(); }, 0);         
    });

    // this.simulate();
  }
  simulate():void{
    setTimeout(() => {
      this.collectionType.getCurrentRow().data.status = "C";
    }, 5000);

    setTimeout(() => {
      this.collectionType.getCurrentRow().data.status = "P";
    }, 10000);

    setTimeout(() => {
      this.collectionType.getCurrentRow().data.status = "D";
    }, 15000);

    setTimeout(() => {
      this.collectionType.getCurrentRow().data.status = "D";
      this.simulate();
    }, 20000);
  }
*/
}

export interface IPendingOrder extends IOrder{
  user:User//TMP za prikaz info-a ... nije u atributima jer je samo za internal svrhe
}





/*

  private _buildAttributes(): Attribute[]{
  
    let id = new Attribute("id", "Broj narudžbe");
    // id.visible = false;

    let userId = new Attribute("userId");
    userId.visible = false;

    let companyId = new Attribute("companyId");
    companyId.visible = false;
  
    let date = new Attribute("date", "Datum narudžbe");

    let deliveryTime = new Attribute("deliveryTime", "Vrijeme dostave (cca)");

    let rate = new Attribute("rate");
    rate.visible = false;
  
    let status = new Attribute("status", "Status");

    // INITIALIZED => Narudzba/rezervacija je tek kreirana - samo u mob/web aplikaciji
    // ORDERED     => Korisnik je potvrdia narudzbu, sad ceka da mu se jave iz company - commit na bazu/notifikacija company
    // CONFIRMED   => Company je potvrdila narudzbu
    // PROCESSING  => Narudzba se sada priprema
    // FINISHED    => Narudzba je obradena i spremna za dostavu - obavjesti korisnika da stize
    // DELIVERED   => Narudzba je dostavljena na adresu
    //'O' => 'ordered', 'C' => 'confirmed', 'P' => 'processing', 'F' => 'finished', 'D' => 'delivered'

    let remark = new Attribute("remark", "Napomena");
    remark.visible = false;

    let orderItems = new Attribute("orderItems");

    // let total = new Attribute("total", "Ukupno (kn)");    

    orderItems.type = this._buildOrderItemsCollection();

    return [id, userId, companyId, date, deliveryTime, rate, status, remark, orderItems];
  }

  private _buildOrderItemsCollection(): CollectionType{

    let ct:CollectionType = new CollectionType();

    ct.addSelfCollectionLink("orderItems", environment.API_URL+"orders/{id}/orderItems");

    let id = new Attribute("id");
    id.visible = false;

    let amount = new Attribute("amount", "Količina");
    
    let discount = new Attribute("discount", "Popust");
    discount.visible = false;
    
    let remark = new Attribute("remark", "Napomena");
    remark.visible = false;

    let menuItemId = new Attribute("menuItemId");
    menuItemId.visible = false;
   
    let menuItemOptionId = new Attribute("menuItemOptionId");
    menuItemOptionId.visible = false;

    let menuItemName = new Attribute("menuItemName");
    menuItemName.visible = false;
    let menuItemPhoto = new Attribute("menuItemPhoto");
    menuItemPhoto.visible = false;
    let menuItemPrice = new Attribute("menuItemPrice");
    menuItemPrice.visible = false;
    let menuItemDiscount = new Attribute("menuItemDiscount");
    menuItemDiscount.visible = false;
    let menuItemDescription = new Attribute("menuItemDescription");
    menuItemDescription.visible = false;

    let menuItemOptionName = new Attribute("menuItemOptionName", "Naziv (opcija)");
   
    let menuItemOptionPrice = new Attribute("menuItemOptionPrice", "Cijena (kn)");
        
    let total = new Attribute("total", "Ukupno (kn)");

    let orderItemSupplements = new Attribute("orderItemSupplements");    
    orderItemSupplements.type = this._buildOrderItemSupplementsCollection();


    ct.attributes = [id, menuItemOptionName, menuItemOptionPrice, amount, discount, remark, menuItemId, menuItemOptionId, total, orderItemSupplements];

    return ct;
  }

  

  private _buildOrderItemSupplementsCollection(): CollectionType{

    let ct:CollectionType = new CollectionType();
    ct.addSelfCollectionLink("orderItemSupplements", environment.API_URL+"orders/{id}/orderItems/{id}/orderItemSupplements");

    let id = new Attribute("id");
    id.visible = false;

    let orderItemId = new Attribute("orderItemId");
    orderItemId.visible = false;

    let menuItemOptionSupplementId = new Attribute("menuItemOptionSupplementId");
    menuItemOptionSupplementId.visible = false;

    let menuItemOptionSupplementName = new Attribute("menuItemOptionSupplementName", "Naziv");

    let menuItemOptionSupplementPrice = new Attribute("menuItemOptionSupplementPrice", "Cijena (kn)");



    ct.attributes = [id, orderItemId, menuItemOptionSupplementId, menuItemOptionSupplementName, menuItemOptionSupplementPrice];

    return ct;
  }

*/