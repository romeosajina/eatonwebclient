import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService, IComunicationData } from '../../std/services/data.service';
import { CollectionType, IRow, ROW_STATE } from '../../std/shared/types/CollectionType';
import { AttributesBuilder } from '../../std/shared/common/AttributesBuilder';
import { environment } from '../../../environments/environment';
import { UserService } from '../../std/services/UserService';
import { MenuitemsComponent } from '../menu-items/menu-items.component';
import { EditMenuComponent } from '../edit-menu/edit-menu.component';
import { MessageComponent } from 'src/app/components/message/message.component';
import { Messages } from 'src/app/std/shared/common/StaticValues';

@Component({
  selector: 'app-menus',
  providers: [DataService],
  templateUrl: './menus.component.html',
  styleUrls: ['./menus.component.sass']
})
export class MenusComponent implements OnInit {

  commData: IComunicationData = {
    baseUrl:environment.API_URL, 
    getOptions:[
      {
        resourceName: 'companies',
        options: 'expand=menus,menuItems,menuItemOptions,menuItemOptionSupplements'
      },
      {
        resourceName: 'menus',
        options: 'expand=menuItems,menuItemOptions,menuItemOptionSupplements'
      },
      {
        resourceName: 'menuItems',
        options: 'expand=menuItemOptions,menuItemOptionSupplements'
      },
      {
        resourceName: 'menuItemOptions',
        options: 'expand=menuItemOptionSupplements'
      }
    ] 
  };

  companiesCollectionType:CollectionType;
  collectionType:CollectionType;
  @ViewChild('editMenu') editMenu:EditMenuComponent;
  @ViewChild('message') message:MessageComponent;

  constructor(private dataService:DataService, private userService:UserService) { 
    
    this.companiesCollectionType = AttributesBuilder.buildCompaniesCollection(true);
    this.companiesCollectionType.setSingleMode(true);
    this.companiesCollectionType.addSelfCollectionLink("companies", environment.API_URL + "companies/" + this.userService.getCompany().id);
    this.companiesCollectionType.init();
    this.collectionType = this.companiesCollectionType.findCollectionType('menus');

    this.dataService.commData = this.commData;

    this.dataService.getDataset(this.companiesCollectionType);
    this.companiesCollectionType.onCollectionChanged.subscribe(() => { this.companiesCollectionType.setFirstRowAsCurrent(); })

  }

  ngOnInit() {

  }

  private _getType(type:string): string{
    return AttributesBuilder.MENU_TYPE[type];
  }

  onMenuSelected(menu:IRow): void{
      this.collectionType.setCurrentRow(menu);
  }

  add(): void{ 
    this.collectionType.createRowAndSetItForEdit();
    this.editMenu.show();
  }

  edit(): void{ 
    if(this.collectionType.getCurrentRow() == null) return;

    this.collectionType.setCurrentRowForEdit();
    this.editMenu.show();

  }

  delete(): void{ 
    if(this.collectionType.getCurrentRow() == null) return;

    this.message.show({
                        message:Messages.ASK_DELETE,
                        type:"warning",
                        onOkHandler: this._delete.bind(this)
                      });

  }
  private _delete():void{
    this.collectionType.setCurrentRowForEdit();
    this.dataService.remove(this.collectionType);
  }

  private _path(path:string): string{
    return path == null || path==""? null : environment.API_URL + path;
  }

}
