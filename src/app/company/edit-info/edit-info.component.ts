import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { UserService } from '../../std/services/UserService';
import { CollectionType, IFileRow } from '../../std/shared/types/CollectionType';
import { AttributesBuilder } from '../../std/shared/common/AttributesBuilder';
import { User } from '../../std/User';
import { Company } from '../../std/Company';
import { DataService, IComunicationData } from '../../std/services/data.service';
import { FileType } from '../../std/shared/types/FileType';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FileInput } from 'ngx-material-file-input';
import { MatDialog, MatDialogRef } from '@angular/material';
import { DialogComponent, IDialogData } from 'src/app/components/dialog/dialog.component';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { EthService } from 'src/app/std/services/eth.service';
// import { FileInputComponent } from '../../../../node_modules/ngx-material-file-input/lib/file-input/file-input.component';

@Component({
  selector: 'app-edit-info',
  templateUrl: './edit-info.component.html',
  providers: [DataService],
  styleUrls: ['./edit-info.component.sass']
})
export class EditInfoComponent implements OnInit {
  
  commData: IComunicationData = {
    baseUrl:environment.API_URL, 
    getOptions:[] 
  };

  @ViewChild('dialogContentRef') dialogContentRef:TemplateRef<any>;
  dialogRef:MatDialogRef<DialogComponent, IDialogData> = null;
  
  collectionType:CollectionType;
  company:Company;
  form: FormGroup;

  constructor(private userService:UserService, 
              private dataService:DataService, 
              private fb: FormBuilder,
              public dialog: MatDialog,
              public router: Router,
              private ethService:EthService) { }

  ngOnInit() {

    this.dataService.commData = this.commData;

    this.collectionType = AttributesBuilder.buildCompaniesCollection();
    this.collectionType.init();
    this.collectionType.setCollection([this.userService.getCompany()]);
    this.collectionType.setCurrentRow(this.collectionType.getCollection()[0]);
    this.collectionType.setCurrentRowForEdit();

    this.company = this.collectionType.getEditRow().data;
    // (<FileType>this.collectionType.findAttr("photo").type).destination = "photos/companies/N" + this.company.id;

    this.company.latitude = parseFloat(<any>this.company.latitude);
    this.company.longitude = parseFloat(<any>this.company.longitude);

    this.form = this.fb.group({ photo: [] });
    
    this.ethService.getRewardableCount(this._setRWC.bind(this));
  }

  private _setRWC(c:number): void{ this.company.rewardableCount = c; (<any>this.company).rewardableCountOld = c;}

  save(): void{    

    this.collectionType.validateOnlyMandatory();

    if(!this.collectionType.isValid()) return;

    this.dataService.saveDataAndFiles(this.collectionType);

    this.dataService.onDataSaved.subscribe(()=>{
      
      this.userService.getCompany().setParams(this.collectionType.getCurrentRow().data);

      //Ako se je promjenilo pošalji na blockchain
      if((<any>this.company).rewardableCountOld != this.company.rewardableCount)
        this.ethService.setRewardableCount(this.company.rewardableCount);

      this.router.navigate(['/home']);

    });

  }
 

  private coords = null;
  markerDragEnd(event:any): void{
    this.coords = event.coords;
  }

  updateCoords(): void{
    if(this.coords != null){

      this.company.latitude = this.coords.lat;
      this.company.longitude = this.coords.lng;

      this.coords = null;
    }

    this.dialogRef.close();
  }

  showMap(): void{
    this.dialogRef = DialogComponent.open(this.dialog, {title: "Lokacija vašeg objekta", 
                                                        contentRef: this.dialogContentRef, 
                                                        width:"900px", 
                                                        onOkHandler:this.updateCoords.bind(this)});
  }

}
