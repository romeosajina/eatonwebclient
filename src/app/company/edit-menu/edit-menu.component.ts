import { Component, OnInit, TemplateRef, Input, ViewChild, AfterViewInit } from '@angular/core';
import { CollectionType } from 'src/app/std/shared/types/CollectionType';
import { DialogComponent, IDialogData } from 'src/app/components/dialog/dialog.component';
import { MatDialogRef, MatDialog } from '@angular/material';
import { DataService } from 'src/app/std/services/data.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AttributesBuilder } from 'src/app/std/shared/common/AttributesBuilder';

@Component({
  selector: 'app-edit-menu',
  templateUrl: './edit-menu.component.html',
  styleUrls: ['./edit-menu.component.sass']
})
export class EditMenuComponent implements OnInit {

  @Input() collectionType:CollectionType; //menus - Collection
  @ViewChild('dialogContentRef') dialogContentRef:TemplateRef<any>;

  dialogRef:MatDialogRef<DialogComponent, IDialogData>;

  form: FormGroup;
  
  constructor(public dialog: MatDialog, private dataService:DataService, private fb: FormBuilder) { }
  
  ngOnInit() {
    this.form = this.fb.group({ photo: [] });

  }


  show(): void{
    setTimeout(() => { this._show() }, 0);
  }

  private _show(): void{
    this.dialogRef = DialogComponent.open(this.dialog, {
                                                        title: "Meni", 
                                                        contentRef: this.dialogContentRef, 
                                                        width:"50%",
                                                        onOkHandler: this._save.bind(this),
                                                        onCancelHandler: this._cancel.bind(this)
                                                      });

    this.dialogRef.afterClosed().subscribe(() => { this.form.reset(); });
  }

  private _cancel(afterClosed:boolean): void{    
    this.collectionType.clear();
  
    if(afterClosed)
      setTimeout(() => { 
        // this.collectionType.setEditRow(null); detalji se isprazne
      }, 0);
    else
      this.dialogRef.close();
  }

  private _save(): void{

    this.collectionType.validateOnlyMandatory(this.collectionType.getEditRow());
    if(!this.collectionType.isValid())
      return;

      
    DialogComponent.disableClose(this.dialogRef);

    this.dataService.saveDataAndFiles(this.collectionType);  
    this.dataService.onDataSaved.subscribe(() => { this.dialogRef.close(); DialogComponent.enableClose(this.dialogRef); });
    this.dataService.onDataSaveFailed.subscribe(() => { DialogComponent.enableClose(this.dialogRef); });
  }


  private _opts = Object.entries(AttributesBuilder.MENU_TYPE);
  private _getOpts(): Array<any>{
    // if(this._opts == null){
    //   this._opts = Object.entries(AttributesBuilder.MENU_TYPE);
    // }
    return this._opts;
  }
}
