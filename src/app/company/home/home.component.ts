import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  constructor(private router:Router) {}

  ngOnInit() { 
  }

  user(): void{
    window.open("https://play.google.com/store/apps/details?id=hr.nss.eaton", "_blank");
  }

  company(): void{
    this.router.navigate(['/login']);
  }

}
