/*Uključivanje lokale za app */
import { registerLocaleData } from '@angular/common';
import localeHr from '@angular/common/locales/hr';
registerLocaleData(localeHr);

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from '../core/app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../core/material.module';
import { HttpModule } from '@angular/http';
import { AgmCoreModule } from '@agm/core';

import { ChartsModule } from 'ng2-charts';

import { FlexLayoutModule } from '@angular/flex-layout';
import { NavigationComponent } from './std/navigation/navigation.component';
import { PendingOrdersComponent } from './company/pending-orders/pending-orders.component';
import { StaticService } from './std/services/StaticService';
import { PusherService } from './std/services/pusher.service';
import { AuthGuard } from './std/services/AuthGuard';
import { UserService } from './std/services/UserService';
import { CONSTANTS_PROVIDERS } from './std/shared/common/StaticValues';

import { TableComponent } from './components/table/table.component';
import { DialogComponent } from './components/dialog/dialog.component';
import { UserInfoDialogComponent } from './shared/user-info-dialog/user-info-dialog.component';
import { EditInfoComponent } from './company/edit-info/edit-info.component';
import { HomeComponent } from './company/home/home.component';
import { LogoutComponent } from './company/logout/logout.component';
import { LoginComponent } from './company/login/login.component';
import { MenusComponent } from './company/menus/menus.component';
import { MenuitemsComponent } from './company/menu-items/menu-items.component';
import { MenuItemEditComponent } from './company/menu-item-edit/menu-item-edit.component';
import { EditMenuComponent } from './company/edit-menu/edit-menu.component';
import { MessageComponent } from './components/message/message.component';
import { OrdersComponent } from './company/orders/orders.component';
import { StatisticsComponent } from './company/statistics/statistics.component';
import { DashboardComponent } from './company/dashboard/dashboard.component';
import { MatGridListModule, MatCardModule, MatMenuModule, MatIconModule, MatButtonModule } from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { EthService } from './std/services/eth.service';


@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    PendingOrdersComponent,
    TableComponent,
    DialogComponent,
    UserInfoDialogComponent,
    EditInfoComponent,
    HomeComponent,
    LogoutComponent,
    LoginComponent,
    MenusComponent,
    MenuitemsComponent,
    MenuItemEditComponent,
    EditMenuComponent,
    MessageComponent,
    OrdersComponent,
    StatisticsComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    HttpModule,
    ReactiveFormsModule, 
    ChartsModule,
    AgmCoreModule.forRoot({ apiKey: 'AIzaSyBQEZx3FRgo7rvhYygUfvaPaU87jjg7R1I' }),
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    LayoutModule //https://stackblitz.com/edit/angular-google-maps-demo?file=app%2Fapp.component.html
  ],
  entryComponents: [
    DialogComponent
  ],
  providers: [//Services
    CONSTANTS_PROVIDERS,
    UserService,
    AuthGuard,
    StaticService,
    PusherService,
    EthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

//https://auth0.com/blog/creating-beautiful-apps-with-angular-material/
//ng new EatOn
//npm install @angular/material @angular/cdk
//npm install hammerjs
//npm install @angular/flex-layout@^7.0.0-beta.19 rxjs-compact ....@depricated npm install @angular/flex-layout@v5.0.0-beta.14 rxjs-compat
//npm install @agm/core --save
//npm i ngx-material-file-input
//npm install ng2-charts --save
//npm install chart.js --save
//npm install --save pusher-js

/* Za web3 instalaciju
npm install -g node-gyp => treba za we3
npm config set python C:\Python27\python.exe --global
npm rebuild node-sass --force
npm install --save web3 => ipak treba nizu verziju zbog nekog crypt-a pa je onda npm install web3@0.20.5
*/

/*
****TODO:*****

- registracija - ok
- pregled i uređivanje info-a - polovicno
- dodavanje meni-a, meni-item-a, option-a i suplemenata - ok...file upload je TODO

- pregled tekucih tnx-a i mjenjanje njihovih statusa --> OK?
- pregled odrađenih narudžbi

- homeComponent => drildown - kad se klikne neka tocka dodat da se otvori 
  prikaz proizvoda i njihova dobit za taj mjesec



*/