import { Component, OnInit, Input, TemplateRef, ViewChild } from '@angular/core';
import { User } from '../../std/User';
import { DialogComponent } from '../../components/dialog/dialog.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-user-info-dialog',
  templateUrl: './user-info-dialog.component.html',
  styleUrls: ['./user-info-dialog.component.sass']
})
export class UserInfoDialogComponent implements OnInit {

  @Input() user:User;
  @Input() autoShow:boolean = false;
  @ViewChild('dialogContentRef') dialogContentRef:TemplateRef<any>;

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  _show():void{
    if(this.autoShow)
      this.show();
  }

  show(): void{
    const dialogRef = DialogComponent.open(this.dialog, {title: "Detalji korisnika", contentRef: this.dialogContentRef, width:"60%", cancelVisible: false});
  }

  private _parse(n): number{ return parseFloat(n); }

}
