import { Component } from '@angular/core';
import { StaticService } from './std/services/StaticService';
import { MatSnackBar } from '@angular/material';
import { environment } from 'src/environments/environment.prod';
import { UserService } from './std/services/UserService';
import { PusherService } from './std/services/pusher.service';
import { IOrder } from './std/shared/common/AttributesBuilder';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {

  public isLoading:boolean = false;

  //User service MORA biti injektan na razini app componente inace nece se bindat na dataService auth funkciju
  constructor(public staticService:StaticService, 
              private snackBar: MatSnackBar, 
              private userService:UserService, 
              private pusherService:PusherService){

    this.staticService.onLoading.subscribe(isLoading => { setTimeout(() => { this.isLoading = isLoading; }, 0); });

    this.staticService.onError.subscribe(errorMsg => {
      
      this.snackBar.open(errorMsg, null, {duration: 15000 }); //15s 

        if(!environment.production){
          console.log("Global error: "); 
          console.log(errorMsg); 
        }
        
      });
  
      this.pusherService.onOrderAdded.subscribe((order:IOrder) => {
        this._notifyOrderEvent("Dodana nova narudžba", order);        
        // let msg = "Dodana nova narudžba: (" + order.id.toString().padStart(8, "0") + ") " + formatDate(order.date, "dd.MM.yyy HH:mm", "hr-HR");
        // this.snackBar.open(msg, null, {duration: 10000 });

      })

      this.pusherService.onOrderDeleted.subscribe((order:IOrder) => {
        this._notifyOrderEvent("Korisnik je izbrisao narudžbu", order);        
      })
    // this.snackBar.open('Message archived', null, { duration: 3000 });
  
  }

  private _notifyOrderEvent(desc:string, order:IOrder): void{

    let msg = desc + ": (" + order.id.toString().padStart(8, "0") + ") " + formatDate(order.date, "dd.MM.yyy HH:mm", "hr-HR");

    this.snackBar.open(msg, null, {duration: 10000 });

  }


}
