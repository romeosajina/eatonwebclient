import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';

import { PendingOrdersComponent } from '../app/company/pending-orders/pending-orders.component';
import { AuthGuard } from '../app/std/services/AuthGuard';
import { EditInfoComponent } from '../app/company/edit-info/edit-info.component';
import { HomeComponent } from '../app/company/home/home.component';
import { LogoutComponent } from '../app/company/logout/logout.component';
import { LoginComponent } from '../app/company/login/login.component';
import { MenusComponent } from '../app/company/menus/menus.component';
import { OrdersComponent } from 'src/app/company/orders/orders.component';
import { StatisticsComponent } from 'src/app/company/statistics/statistics.component';
import { DashboardComponent } from 'src/app/company/dashboard/dashboard.component';

// const routes: Routes = [];

const appRoutes: Routes = [

  { path: 'login', component: LoginComponent, canActivate: [AuthGuard] },
  { path: 'logout', component: LogoutComponent, canActivate: [AuthGuard] },
  
  { path: 'pendingOrders', component: PendingOrdersComponent, canActivate: [AuthGuard] },
  { path: 'doneOrders', component: OrdersComponent, canActivate: [AuthGuard] },

  { path: 'editInfo', component: EditInfoComponent, canActivate: [AuthGuard] },

  { path: 'menus', component: MenusComponent, canActivate: [AuthGuard] },

  { path: 'statistics', component: StatisticsComponent, canActivate: [AuthGuard]},

  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},

  { path: 'home', component: HomeComponent, canActivate: [AuthGuard]},

  { path: '**', component: null, redirectTo: 'home' }
  // { path: '/', component: null, redirectTo: 'login' },
  // { path: '*', component: null, redirectTo: 'login' }

];


@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}]
})
export class AppRoutingModule { }
