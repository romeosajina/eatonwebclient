export const environment = {
  production: false,
  API_URL: 'http://www.romeosajina.ga/EatOn/public/server/',
  BASE_URL: 'http://www.romeosajina.ga/EatOn/public/client/'
};


/*

Build local
ng build --prod --output-path=C:/xampp/htdocs/EatOn/public/client --base-href /EatOn/public/client/ 

Build prod for adik.ga
ng build --prod --output-path=C:/xampp/htdocs/EatOn/public/client --base-href /EatOn/public/client/ --deploy-url http://adik.ga/EatOn/public/client/


Build prod for eaton.romeosajina.ga
ng build --prod --output-path=C:/xampp/htdocs/EatOn/public/client --base-href /public/client/ --deploy-url http://eaton.romeosajina.ga/public/client/


Build dist
ng build --prod --output-path=dist

*/